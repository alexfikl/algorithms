# Compilation

Compile with:

```
meson setup --buildtype=debug -Dtesting=true -Db_sanitize=undefined build
meson compile -C build
```

The `-Db_sanitize=undefined` option sets a desired sanitizer
(see the [meson docs](https://mesonbuild.com/Builtin-options.html#base-options)).
To build tests, add ``-Dtesting=true`` to the ``meson`` configure line and run

```
meson test -C build
```

# Algorithms

* [Scalable Bloom Filter](http://gsd.di.uminho.pt/members/cbm/ps/dbloom.pdf)
* [Sobel Filter](https://en.wikipedia.org/wiki/Sobel_operator)
* [Convex Hull Algorithms](https://en.wikipedia.org/wiki/Convex_hull_algorithms#Algorithms)
* [k-d Tree](https://en.wikipedia.org/wiki/K-d_tree)
* [van Emde Boas Tree](https://en.wikipedia.org/wiki/Van_Emde_Boas_tree)
* Some [string matching](https://www-igm.univ-mlv.fr/~lecroq/string/) algorithms
* [Voronoi Tesselation](https://en.wikipedia.org/wiki/Voronoi_diagram)

# License

If you happen to get a hold of this stuff, everything is under the MIT Licence.
