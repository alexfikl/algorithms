Algorithms
==========

Convex Hull
-----------

.. c:autodoc:: convex-hull/convex-hull.h

K-d Tree
--------

.. c:autodoc:: kd-tree/kdtree.h

Bloom Filter
------------

.. c:autodoc:: scalable-bloom-filter/bloom-filter.h

Scalable Bloom Filter
---------------------

.. c:autodoc:: scalable-bloom-filter/sbf.h
