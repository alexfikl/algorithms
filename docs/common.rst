Utilities
=========

Logging
-------

.. c:autodoc:: common/algorithms.h

Doubly-Linked List
------------------

.. c:autodoc:: common/linked-list.h

Hashmap
-------

.. c:autodoc:: common/hash-map.h

Priority Queue
--------------

.. c:autodoc:: common/priority-queue.h


Deterministic Finite Automaton (DFA)
------------------------------------

.. c:autodoc:: common/deterministic-finite-automaton.h

Geometry
--------

.. c:autodoc:: common/geometry.h

TikZ
----

.. c:autodoc:: common/tikz.h
