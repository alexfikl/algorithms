# https://www.sphinx-doc.org/en/master/usage/configuration.html
# https://www.sphinx-doc.org/en/master/usage/extensions/autodoc.html
# do not look for these imports (hard to install)
import importlib
import os
import sys

# {{{ project information

project = "Algorithms"
author = "Alexandru Fikl"
copyright = "2020 Alexandru Fikl"
version = "2020.09"
release = "2020.09"

# }}}

# {{{ general configuration

# needed extensions
extensions = [
    "sphinx.ext.autodoc",
    "sphinx.ext.intersphinx",
    "sphinx.ext.viewcode",
    "sphinx.ext.mathjax",
    "hawkmoth",
]

try:
    import sphinxcontrib.spelling

    extensions.append("sphinxcontrib.spelling")
except ImportError:
    pass

# extension for source files
source_suffix = {".rst": "restructuredtext"}
# name of the main (master) document
master_doc = "index"
# min sphinx version
needs_sphinx = "2.0"
# files to ignore
exclude_patterns = ["_build", "Thumbs.db", ".DS_Store"]
# highlighting
pygments_style = "sphinx"

# }}}

# {{{ internationalization

language = "en"

# sphinxcontrib.spelling options
spelling_lang = "en_US"
tokenizer_lang = "en_US"

# }}

# {{{ output

# html
html_theme = "sphinx_book_theme"
html_theme_options = {
    "show_toc_level": 2,
    "use_source_button": True,
    "use_repository_button": True,
    "navigation_with_keys": False,
    "repository_url": "https://gitlab.com/alexfikl/algorithms",
    "repository_branch": "main",
}

# }}}

# {{{ extension settings

autodoc_member_order = "bysource"
autodoc_default_options = {
    "show-inheritance": None,
}

# tell hawkmoth where to find files
from hawkmoth.util import compiler

hawkmoth_root = os.path.abspath("../src")

includes = [f"-I{directory}" for directory, _, _ in os.walk(hawkmoth_root)]
hawkmoth_clang = ["-I/usr/include", *compiler.get_include_args(), *includes, "-std=c18"]

# }}}

# {{{ links

intersphinx_mapping = {}

# }}}
