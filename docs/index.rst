Welcome to our documentation!
=============================

.. toctree::
    :maxdepth: 2

    algorithms
    common

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
