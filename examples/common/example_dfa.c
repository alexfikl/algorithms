// SPDX-FileCopyrightText: 2014-2025 Alexandru Fikl <alexfikl@gmail.com>
// SPDX-License-Identifier: MIT

#include <deterministic-finite-automaton.h>

int main(int argc, char **argv) {
    algorithms_init(argc, argv);

    const size_t states = 9;
    dfa_t *dfa = dfa_new(states);

    /* add transitions */
    dfa_set_transition(dfa, 0, 'G', 1);
    dfa_set_transition(dfa, 1, 'C', 2);
    dfa_set_transition(dfa, 2, 'A', 3);
    dfa_set_transition(dfa, 3, 'G', 4);
    dfa_set_transition(dfa, 4, 'A', 5);
    dfa_set_transition(dfa, 5, 'G', 6);
    dfa_set_transition(dfa, 6, 'A', 7);
    dfa_set_transition(dfa, 7, 'G', 8);
    dfa_set_transition(dfa, 1, 'G', 1);
    dfa_set_transition(dfa, 2, 'G', 1);
    dfa_set_transition(dfa, 4, 'G', 1);
    dfa_set_transition(dfa, 6, 'G', 1);
    dfa_set_transition(dfa, 8, 'G', 1);
    dfa_set_transition(dfa, 4, 'C', 2);
    dfa_set_transition(dfa, 6, 'C', 2);
    dfa_set_transition(dfa, 8, 'C', 2);
    dfa_set_initial(dfa, 0);
    dfa_set_terminal(dfa, 8);

    /* write */
    dfa_tikz(dfa, "dfa_example.tex");

    dfa_destroy(dfa);
    algorithms_finalize();

    return EXIT_SUCCESS;
}
