// SPDX-FileCopyrightText: 2014-2025 Alexandru Fikl <alexfikl@gmail.com>
// SPDX-License-Identifier: MIT

#include <hash-map.h>

#define BUCKET_COUNT 100
#define KEYS_PER_BUCKET 1000
#define KEY_COUNT (BUCKET_COUNT * KEYS_PER_BUCKET)

#define HASH_FUNCTION_COUNT 5
#define BUFFER_LEN 20

enum { HASH_MURMUR2, HASH_JENKINS, HASH_FNV, HASH_ADLER, HASH_DJB };

static uint8_t **hash_generate_keys(size_t size) {
    uint8_t **keys = NULL;
    FILE *urandom = NULL;

    urandom = fopen("/dev/urandom", "r");
    ALG_CHECK_ABORT(urandom != NULL, "could not open /dev/urandom");

    keys = (uint8_t **) alg_malloc(size * sizeof(uint8_t *));
    for (size_t i = 0; i < size; ++i) {
        keys[i] = (uint8_t *) alg_malloc(BUFFER_LEN * sizeof(uint8_t));
        fread(keys[i], sizeof(uint8_t), BUFFER_LEN, urandom);
    }

    fclose(urandom);

    return keys;
}

static void
hash_distribution(int *stats, uint8_t **keys, size_t size, hash_function_t hash) {
    uint32_t h = 0;

    for (size_t i = 0; i < size; ++i) {
        h = hash(keys[i], BUFFER_LEN, ALG_HASH_DEFAULT_SEED);
        stats[h % BUCKET_COUNT] += 1;
    }
}

int main(int argc, char **argv) {
    int stats[HASH_FUNCTION_COUNT][BUCKET_COUNT] = {{0}};
    uint8_t **keys = NULL;
    FILE *fd = NULL;

    if (argc < 2) {
        printf("usage: %s [OUTPUT]\n", argv[0]);
        exit(EXIT_FAILURE);
    }

    keys = hash_generate_keys(KEY_COUNT);

    hash_distribution(stats[HASH_MURMUR2], keys, KEY_COUNT, hash_murmur);
    hash_distribution(stats[HASH_JENKINS], keys, KEY_COUNT, hash_jenkins);
    hash_distribution(stats[HASH_FNV], keys, KEY_COUNT, hash_fnv_1a);
    hash_distribution(stats[HASH_ADLER], keys, KEY_COUNT, hash_adler);
    hash_distribution(stats[HASH_DJB], keys, KEY_COUNT, hash_djb);

    fd = fopen(argv[1], "w");
    ALG_CHECK_ABORT(fd != NULL, "cannot open file \"%s\"", argv[1]);

    fprintf(fd, "MURMUR\tJENKINS\tFNV\tADLER\tDJB\n");
    for (size_t i = 0; i < BUCKET_COUNT; ++i) {
        fprintf(
            fd,
            "%d\t%d\t%d\t%d\t%d\n",
            stats[HASH_MURMUR2][i],
            stats[HASH_JENKINS][i],
            stats[HASH_FNV][i],
            stats[HASH_ADLER][i],
            stats[HASH_DJB][i]
        );
    }

    fclose(fd);

    for (size_t i = 0; i < KEY_COUNT; ++i) {
        alg_free(keys[i]);
    }
    alg_free(keys);

    ALG_INFO("Statistics can be generated using R, for example like:");
    ALG_INFO("    hash <- read.table(\"%s\", header=T)", argv[1]);
    ALG_INFO("    summary(hash)");
    ALG_INFO("");
    ALG_INFO("Look for the smallest ranges for [MIN, MAX]");
    ALG_INFO("as well as a median equal to %d", KEYS_PER_BUCKET);
    return EXIT_SUCCESS;
}
