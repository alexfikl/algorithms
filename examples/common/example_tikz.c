// SPDX-FileCopyrightText: 2014-2025 Alexandru Fikl <alexfikl@gmail.com>
// SPDX-License-Identifier: MIT

#include <tikz.h>

int main(int argc, char **argv) {
    if (argc < 2) {
        printf("usage: %s [OUTPUT]", argv[0]);
        exit(1);
    }

    int point_id = 0;
    char label[BUFSIZ];

    algorithms_init(argc, argv);

    tikz_preamble(argv[1]);

    tikz_add_package("babel", "english");
    tikz_add_library("arrows");
    tikz_add_library("shapes");
    tikz_add_custom("\\newcommand{\\vect}[1]{\\mathbf{#1}}");

    tikz_document_begin();
    tikz_picture_begin(NULL);

    for (double y = 5.0; y >= -5.0; y -= 1.0) {
        for (double x = -5.0; x <= 5.0; x += 1.0) {
            snprintf(label, BUFSIZ, "$%d$", point_id);
            tikz_add_point_position(x, y, label);

            point_id += 1;
        }
    }

    tikz_add_line_position(-5.0, -5.0, 5.0, -5.0);
    tikz_add_line_position(5.0, -5.0, 5.0, 5.0);
    tikz_add_line_position(5.0, 5.0, -5.0, 5.0);
    tikz_add_line_position(-5.0, 5.0, -5.0, -5.0);

    tikz_add_rectangle_position(-5.1, -5.1, 5.1, 5.1);

    tikz_picture_end();
    tikz_document_end();

    algorithms_finalize();

    return EXIT_SUCCESS;
}
