// SPDX-FileCopyrightText: 2014-2025 Alexandru Fikl <alexfikl@gmail.com>
// SPDX-License-Identifier: MIT

#include <algorithms.h>
#include <convex-hull.h>

#include <time.h>

static const double xmin = -5;
static const double xmax = 5;
static const double ymin = -10;
static const double ymax = 10;

int main(int argc, char **argv) {
    size_t n = 1024;
    bool with_labels = (n > 100 ? false : true);
    double x = 0;
    double y = 0;
    struct point **points = NULL;
    linked_list_t *hull;

    if (argc < 2) {
        printf("usage %s [OUTPUT] [NPOINTS]\n", argv[0]);
        exit(1);
    }

    /* get number of points */
    if (argc > 2) {
        n = (size_t) atol(argv[2]);
    }

    ALG_CHECK_ABORT(xmin < xmax, "xmin >= xmax");
    ALG_CHECK_ABORT(ymin < ymax, "ymin >= ymax");

    algorithms_init(argc, argv);
    srand((uint32_t) time(NULL));

    /* generate random points */
    //     n = 6;
    points = (struct point **) alg_malloc(n * sizeof(struct point *));
    //     points[0] = point_new(0.0, 0.0);
    //     points[1] = point_new(1.0, 2.0);
    //     points[2] = point_new(3.0, 0.0);
    //     points[3] = point_new(2.0, -2.0);
    //     points[4] = point_new(2.0, 1.5);
    //     points[5] = point_new(1.0, 0.0);

    for (size_t i = 0; i < n; ++i) {
        x = xmin + (xmax - xmin) * (rand() / (double) RAND_MAX);
        y = ymin + (ymax - ymin) * (rand() / (double) RAND_MAX);

        points[i] = point_new(x, y);
    }

    /* get convex hull */
    hull = convex_hull_chan(points, n);
    convex_hull_tikz(
        (const struct point **) (void *) points, n, hull, argv[1], with_labels
    );

    linked_list_destroy(hull, false);
    for (size_t i = 0; i < n; ++i) {
        alg_free(points[i]);
    }
    alg_free(points);

    algorithms_finalize();

    return EXIT_SUCCESS;
}
