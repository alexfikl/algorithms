// SPDX-FileCopyrightText: 2014-2025 Alexandru Fikl <alexfikl@gmail.com>
// SPDX-License-Identifier: MIT

#include <kdtree.h>

static const uint8_t dim = 1;
static const size_t npoints = 8;

double random_coordinate(double a, double b) {
    return a + (b - a) * ((double) rand()) / ((double) RAND_MAX);
}

int main(int argc, char **argv) {
    kd_tree_t *tree = NULL;
    double *x = NULL;

    algorithms_init(argc, argv);

    ALG_INFO("Allocating tree.");
    tree = kd_tree_new(dim);

    for (size_t i = 0; i < npoints; ++i) {
        x = alg_malloc(dim * sizeof(double));

        for (uint8_t j = 0; j < dim; ++j) {
            x[j] = random_coordinate(-1.0, 1.0);
        }

        kd_tree_insert(tree, x, NULL);
    }
    kd_tree_tikz(tree, "kd_tree_random.tex");

    kd_tree_destroy(tree, NULL);
    algorithms_finalize();

    return EXIT_SUCCESS;
}
