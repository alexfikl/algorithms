// SPDX-FileCopyrightText: 2014-2025 Alexandru Fikl <alexfikl@gmail.com>
// SPDX-License-Identifier: MIT

#include <algorithms.h>
#include <bloom-filter.h>

int main(void) {
    struct bloom_filter_t *bf = bloom_filter_new(10, 0.001, NULL);
    const char *stringsin[] = {
        "i like pie", "pie is nice", "something else", "boobies", "directory", NULL
    };
    const char *stringsout[] = {
        "i like pie",
        "pie is nice",
        "directory",
        "haha you did not see me",
        "told you it was all wrong!",
        NULL
    };

    for (int i = 0; stringsin[i] != NULL; ++i) {
        ALG_INFO("Adding: \"%s\"", stringsin[i]);
        bloom_filter_add(bf, stringsin[i], strlen(stringsin[i]));
    }
    bloom_filter_dump(bf);

    for (int i = 0; stringsout[i] != NULL; ++i) {
        if (bloom_filter_contains(bf, stringsout[i], strlen(stringsout[i])) == 0) {
            ALG_INFO("NOT IN SET: \"%s\"", stringsout[i]);
        } else {
            ALG_INFO("IN SET    : \"%s\"", stringsout[i]);
        }
    }

    return EXIT_SUCCESS;
}
