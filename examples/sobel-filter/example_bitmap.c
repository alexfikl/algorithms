// SPDX-FileCopyrightText: 2014-2025 Alexandru Fikl <alexfikl@gmail.com>
// SPDX-License-Identifier: MIT

#include <algorithms.h>
#include <bitmap.h>

int main(int argc, char **argv) {
    static const bool grayscale = true;
    static const bool preserve_alpha = false;

    bitmap_t *bmp = NULL;

    algorithms_init(argc, argv);

    if (argc < 3) {
        printf("usage: %s [INPUT] [OUTPUT]", argv[0]);
        exit(1);
    }

    bmp = bitmap_read_png(argv[1], grayscale, preserve_alpha);
    ALG_CHECK_ABORT(bmp, "Could not initialize bmp from file");

    bitmap_write_png(bmp, argv[2]);
    bitmap_destroy(bmp);

    algorithms_finalize();

    return EXIT_SUCCESS;
}
