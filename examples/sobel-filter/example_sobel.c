// SPDX-FileCopyrightText: 2014-2025 Alexandru Fikl <alexfikl@gmail.com>
// SPDX-License-Identifier: MIT

#include <algorithms.h>
#include <bitmap.h>
#include <sobel.h>

int main(int argc, char **argv) {
    static const bool grayscale = true;
    static const bool preserve_alpha = false;

    bitmap_t *src = NULL;
    bitmap_t *dst = NULL;

    algorithms_init(argc, argv);

    if (argc < 3) {
        printf("usage: %s [INPUT] [OUTPUT]", argv[0]);
        exit(1);
    }

    src = bitmap_read_png(argv[1], grayscale, preserve_alpha);
    ALG_CHECK_ABORT(src, "Could not read bitmap from file");

    dst = sobel_filter(src);
    bitmap_write_png(dst, argv[2]);

    bitmap_destroy(src);
    bitmap_destroy(dst);

    algorithms_finalize();

    return EXIT_SUCCESS;
}
