// SPDX-FileCopyrightText: 2014-2025 Alexandru Fikl <alexfikl@gmail.com>
// SPDX-License-Identifier: MIT

#include <algorithms.h>
#include <veb.h>

static const int64_t universe_size = 16;

int64_t random_element(void) {
    return rand() % universe_size;
}

void check_delete(veb_t *veb, int64_t x) {
    ALG_INFO("Deleting %ld", x);
    veb_delete(veb, x);

    if (veb_contains(veb, x)) {
        ALG_INFO("Tree contains %ld", x);
    } else {
        ALG_INFO("Tree does not contain %ld", x);
    }
}

void check_insert(veb_t *veb, int64_t x) {
    ALG_INFO("Inserting %ld", x);
    veb_insert(veb, x);
    //     veb_dump(veb->summary);
}

int main(int argc, char **argv) {
    veb_t *veb = NULL;

    algorithms_init(argc, argv);

    ALG_INFO("Allocating tree.");
    veb = veb_new(universe_size);
    //     veb_preallocate(veb);

    if (veb_is_empty(veb)) {
        ALG_INFO("Tree is empty.");
    } else {
        ALG_INFO("Tree is not empty");
    }

    check_insert(veb, 6);
    check_insert(veb, 3);
    check_insert(veb, 9);
    check_insert(veb, 12);
    veb_dump(veb);

    if (veb_is_empty(veb)) {
        ALG_INFO("Tree is empty.");
    } else {
        ALG_INFO("Tree is not empty");
    }

    ALG_INFO("Tree min is %ld and max is %ld.", veb->min, veb->max);

    if (veb_contains(veb, 12)) {
        ALG_INFO("Tree contains 12.");
    } else {
        ALG_INFO("Tree does not contain 12.");
    }

    if (veb_contains(veb, 10)) {
        ALG_INFO("Tree contains 10.");
    } else {
        ALG_INFO("Tree does not contain 10.");
    }

    //     veb_dump(veb->summary);
    check_delete(veb, 3);
    check_delete(veb, 6);
    check_delete(veb, 9);
    check_delete(veb, 12);
    veb_dump(veb);

    //     ALG_INFO("Successor of 18 is %ld.", veb_next(veb, 18));
    //     ALG_INFO("Predecessor of 18 is %ld.", veb_prev(veb, 18));
    //     ALG_INFO("Predecessor of 25 is %ld.", veb_prev(veb, 25));
    //     ALG_INFO("Predecessor of 32 is %ld.", veb_prev(veb, 32));
    //     ALG_INFO("Predecessor of 44 is %ld.", veb_prev(veb, 44));

    //     ALG_INFO("Deleting all elements.");
    //     check_delete(veb, 68);    /* delete max */
    //     check_delete(veb, 44);    /* delete middle */
    //     check_delete(veb, 18);    /* delete min */
    //     check_delete(veb, 51);    /* delete max */
    //     veb_dump(veb);          /* empty */

    //     if (veb_is_empty(veb)) {
    //         ALG_INFO("Tree is empty.");
    //     } else {
    //         ALG_INFO("Tree is not empty");
    //     }

    veb_free(veb);
    algorithms_finalize();

    return EXIT_SUCCESS;
}
