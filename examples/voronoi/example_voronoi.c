// SPDX-FileCopyrightText: 2014-2025 Alexandru Fikl <alexfikl@gmail.com>
// SPDX-License-Identifier: MIT

#include <algorithms.h>
#include <voronoi.h>

#include <time.h>

int main(int argc, char **argv) {
    const size_t n = 9;
    struct point **points = NULL;
    voronoi_t *voronoi = NULL;

    algorithms_init(argc, argv);

    srand((uint32_t) time(NULL));

    points = (struct point **) alg_malloc(n * sizeof(struct point *));
    //     for (size_t i = 0; i < n; ++i) {
    //         points[i].x = -5.0 + 10.0 * ((double) rand() / RAND_MAX);
    //         points[i].y = -5.0 + 10.0 * ((double) rand() / RAND_MAX);
    //     }

    points[0] = point_new(-1.28607, -2.26559);
    points[1] = point_new(1.22007, -0.452911);
    points[2] = point_new(-2.98163, 0.670659);
    points[3] = point_new(2.48405, 3.48313);
    points[4] = point_new(-1.26055, 2.92655);
    points[5] = point_new(4.29193, -3.63338);
    points[6] = point_new(1.93947, 1.33819);
    points[7] = point_new(-1.86147, -0.419299);
    points[8] = point_new(2.81266, -3.78463);

    voronoi = voronoi_new(points, n);
    for (size_t i = 0; i < n; ++i) {
        printf(
            "face <x=%g, y=%g>",
            point_x(voronoi->faces[i].p),
            point_y(voronoi->faces[i].p)
        );
    }

    algorithms_finalize();

    return EXIT_SUCCESS;
}
