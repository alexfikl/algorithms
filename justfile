PYTHON := 'python -X dev'

_default:
    @just --list

# {{{ formatting

[doc("Reformat all source code")]
format: clangfmt mesonfmt

[private]
clang_format directory:
    find {{ directory }} -type f -name '*.c' -exec clang-format -i {} \;
    find {{ directory }} -type f -name '*.h' -exec clang-format -i {} \;

[doc("Run clang-format over all source files")]
clangfmt:
    @just clang_format src
    @just clang_format tests
    @just clang_format examples
    @echo -e "\e[1;32mclang-format clean!\e[0m"

[doc("Run meson format over all meson files")]
mesonfmt:
    meson format --inplace --recursive
    @echo -e "\e[1;32mmeson format clean!\e[0m"

[doc("Run just --fmt over the justfiles")]
justfmt:
    just --unstable --fmt
    @echo -e "\e[1;32mjust --fmt clean!\e[0m"

# }}}
# {{{ linting

[doc('Run all linting checks over the source code')]
lint: typos reuse

[doc('Run typos over the source code and documentation')]
typos:
    typos --sort
    @echo -e "\e[1;32mtypos clean!\e[0m"

[doc('Check REUSE license compliance')]
reuse:
    {{ PYTHON }} -m reuse lint
    @echo -e "\e[1;32mREUSE compliant!\e[0m"

[doc("Run clang-tidy over the source code")]
clangtidy: build
    ninja -C build clang-tidy

# }}}
# {{{ develop

[doc("Build project in debug mode")]
build:
    meson setup --buildtype=debug -Dtesting=true -Db_sanitize=undefined build
    meson compile -C build

[doc("Run tests")]
test: build
    meson test -C build

[doc("Regenerate ctags")]
ctags:
    ctags --recurse=yes \
        --tag-relative=yes \
        --exclude=.git \
        --exclude=docs

[doc("Remove various build artifacts")]
clean:
    rm -rf docs/build.sphinx

[doc("Remove various temporary files")]
purge: clean
    rm -rf build tags

# }}}
