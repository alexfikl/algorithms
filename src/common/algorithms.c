// SPDX-FileCopyrightText: 2014-2025 Alexandru Fikl <alexfikl@gmail.com>
// SPDX-License-Identifier: MIT

#include "algorithms.h"

#include <time.h>
#include <unistd.h>

static int algorithms_malloc_count = 0;

void algorithms_init(int argc, char **argv) {
    ALG_UNUSED(argc);
    ALG_UNUSED(argv);

    // srand(time(NULL));

    algorithms_malloc_count = 0;
}

void algorithms_finalize(void) {
    ALG_CHECK_ABORT(
        algorithms_malloc_count == 0,
        "Uneven malloc / free calls: remaining %d",
        algorithms_malloc_count
    );
}

void *alg_malloc(size_t size) {
    void *p = malloc(size);
    ALG_CHECK_MEMORY(p);

    algorithms_malloc_count += 1;
    return p;
}

void *alg_calloc(size_t n, size_t size) {
    void *p = calloc(n, size);
    ALG_CHECK_MEMORY(p);

    algorithms_malloc_count += 1;
    return p;
}

void alg_free(void *p) {
    algorithms_malloc_count -= 1;
    free(p);
}

char *alg_getcwd(void) {
    size_t size;
    char *cwd;

    size = pathconf(".", _PC_PATH_MAX);
    cwd = (char *) alg_malloc(size);
    return getcwd(cwd, size);
}
