// SPDX-FileCopyrightText: 2014-2025 Alexandru Fikl <alexfikl@gmail.com>
// SPDX-License-Identifier: MIT

#ifndef ALGORITHMS_H
#define ALGORITHMS_H

#include <errno.h>
#include <float.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <math.h>

/**
 * Malloc the memory and exit on failure.
 */
void *alg_malloc(size_t size);

/**
 * Calloc the memory and exit on failure.
 */
void *alg_calloc(size_t n, size_t size);

/**
 * Free.
 */
void alg_free(void *p);

/**
 * Initialize global variables.
 */
void algorithms_init(int argc, char **argv);

/**
 * Finalize algorithm run.
 */
void algorithms_finalize(void);

/**
 * Get current working directory.
 *
 * The returned string must be deallocated.
 */
char *alg_getcwd(void);

#define ALG_FILENAME (strrchr(__FILE__, '/') ? strrchr(__FILE__, '/') + 1 : __FILE__)

#define clean_errno() (errno == 0 ? "None" : strerror(errno))

/*
 * Define special variadic macros to make gcc happy when using c99.
 * The problem is that if we use these macros directly, __VA_ARGS__ can be
 * empty, but the c99 standard doesn't like that.
 */

#define ALG_LOGC99(M, ...) fprintf(stderr, M "%s\n", __VA_ARGS__)
#define ALG_DEBUGC99(M, ...)                                                           \
    fprintf(                                                                           \
        stderr,                                                                        \
        "[DEBUG] %s:%s:%d: " M "%s\n",                                                 \
        ALG_FILENAME,                                                                  \
        __func__,                                                                      \
        __LINE__,                                                                      \
        __VA_ARGS__                                                                    \
    )
#define ALG_ERRORC99(M, ...)                                                           \
    fprintf(                                                                           \
        stderr,                                                                        \
        "[ERROR] (%s:%s:%d: errno: %s) " M "%s\n",                                     \
        ALG_FILENAME,                                                                  \
        __func__,                                                                      \
        __LINE__,                                                                      \
        clean_errno(),                                                                 \
        __VA_ARGS__                                                                    \
    )
#define ALG_WARNINGC99(M, ...)                                                         \
    fprintf(                                                                           \
        stderr,                                                                        \
        "[WARN] (%s:%s:%d: errno: %s) " M "%s\n",                                      \
        ALG_FILENAME,                                                                  \
        __func__,                                                                      \
        __LINE__,                                                                      \
        clean_errno(),                                                                 \
        __VA_ARGS__                                                                    \
    )
#define ALG_INFOC99(M, ...)                                                            \
    fprintf(stderr, "[INFO] %s: " M "%s\n", __func__, __VA_ARGS__)
#define ALG_ABORTC99(M, ...)                                                           \
    do {                                                                               \
        ALG_ERRORC99(M, __VA_ARGS__);                                                  \
        exit(EXIT_FAILURE);                                                            \
    } while (0)

#define ALG_NOOP() ((void) (0))

/**
 * Default logging to ``stderr``.
 *
 * :param ...: argument list passed to ``printf``.
 */
#define ALG_LOG(...) ALG_LOGC99(__VA_ARGS__, "")
/**
 * Error logging to ``stderr``.
 *
 * :param ...: argument list passed to ``printf``.
 */
#define ALG_ERROR(...) ALG_ERRORC99(__VA_ARGS__, "")
/**
 * Warning logging to ``stderr``.
 *
 * :param ...: argument list passed to ``printf``.
 */
#define ALG_WARN(...) ALG_WARNINGC99(__VA_ARGS__, "")
/**
 * Information logging to ``stderr``.
 *
 * :param ...: argument list passed to ``printf``.
 * */
#define ALG_INFO(...) ALG_INFOC99(__VA_ARGS__, "")

#ifdef NDEBUG
#define ALG_DEBUG(...) ALG_NOOP()
#define ALG_ASSERT(A, ...) ALG_NOOP()
#else
/**
 * Debug logging to ``stderr``.
 *
 * :param ...: argument list passed to ``printf``.
 */
#define ALG_DEBUG(...) ALG_DEBUGC99(__VA_ARGS__, "")
/**
 * Assert condition is ``true``.
 *
 * :param ...: argument list passed to ``printf``.
 */
#define ALG_ASSERT(A) ALG_CHECK_ABORT(A, "Assert: \"" #A "\": ")
#endif

/**
 * Prints the error message and halts execution.
 *
 * :param ...: argument list passed to ``printf``.
 */
#define ALG_ABORT(...) ALG_ABORTC99(__VA_ARGS__, "")
/**
 * Abort execution if *A* is ``false``.
 *
 * :param A: condition to check.
 * :param ...: argument list passed to ``printf``.
 */
#define ALG_CHECK_ABORT(A, ...)                                                        \
    if (!(A)) {                                                                        \
        ALG_ABORTC99(__VA_ARGS__, "");                                                 \
    }
/**
 * Abort execution if memory not allocated.
 *
 * :param A: pointer to newly allocated memory.
 * :param ...: argument list passed to ``printf``.
 */
#define ALG_CHECK_MEMORY(A) ALG_CHECK_ABORT((A), "Out of memory.")

/**
 * Mark ``switch`` case as a
 * `fallthrough <https://clang.llvm.org/docs/AttributeReference.html#fallthrough>`_.
 */
#if defined(__clang__)
#define ALG_FALLTHROUGH() __attribute__((fallthrough));
#elif defined(__GNUC__)
#define ALG_FALLTHROUGH() __attribute__((fallthrough));
#else
#error Unsupported compiler
#endif

/**
 * Mark variable as unused.
 */
#define ALG_UNUSED(x) ((void) (x))
/**
 * Mark branch as unreachable
 */
#define ALG_UNREACHABLE()                                                              \
    do {                                                                               \
        ALG_CHECK_ABORT(false, "ALG_UNREACHABLE() was reached");                       \
        __builtin_unreachable();                                                       \
    } while (0)
/**
 * Turn input argument name into a string.
 *
 * :param x: identifier.
 * :returns: stringified identifier through ``#x``.
 */
#define ALG_STRINGIFY(x) #x
/**
 * Concatenate two argument names.
 *
 * :param x: identifier.
 * :param y: identifier.
 * :returns: string with the concatenated names of the identifiers.
 */
#define ALG_CONCAT(x, y) x##y

/**
 * Default tolerance.
 */
#ifndef ALG_THRESHOLD
#define ALG_THRESHOLD 1.0e-10
#endif

#define ALG_MIN(a, b) ((a) > (b) ? (b) : (a))
#define ALG_MAX(a, b) ((a) > (b) ? (a) : (b))

/**
 * Compare to zero using :c:macro:`ALG_THRESHOLD`.
 *
 * :returns: ``true`` if the number is close to zero.
 */
#define ALG_FUZZY_ZERO(a) (fabs(a) < ALG_THRESHOLD)

/**
 * Check if two numbers are equal using :c:macro:`ALG_THRESHOLD`.
 *
 * :returns: ``true`` if the two numbers are equal within tolerance.
 */
#define ALG_FUZZY_EQ(a, b)                                                             \
    ((ALG_FUZZY_ZERO(a) && ALG_FUZZY_ZERO(b)) ||                                       \
     (fabs((a) - (b)) <= (ALG_THRESHOLD * ALG_MIN(fabs(a), fabs(b)))))

/**
 * Check if a number is with an open interval using :c:macro:`ALG_THRESHOLD`.
 *
 * :returns: ``true`` if :math:`x \in (a, b)` within tolerance.
 */
#define ALG_FUZZY_CONTAINS(x, a, b)                                                    \
    ((((a) - ALG_THRESHOLD) < (x)) && ((x) < ((b) + ALG_THRESHOLD)))

#endif /* ALGORITHMS_H */
