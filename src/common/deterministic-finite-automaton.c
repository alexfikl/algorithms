// SPDX-FileCopyrightText: 2014-2025 Alexandru Fikl <alexfikl@gmail.com>
// SPDX-License-Identifier: MIT

#include "deterministic-finite-automaton.h"

#include <tikz.h>

#define STRING_MAX_SIZE 128

typedef struct dfa_transition_t dfa_transition_t;

/**
 * @brief Transition.
 */
struct dfa_transition_t {
    size_t origin; /**< Origin state of the transition. */
    char label;    /**< Element of the alphabet. */
    size_t target; /**< Target state of the transition. */
};

/**
 * @brief Compare the labels of two transitions.
 *
 * @param[in] a         Transition.
 * @param[in] b         Transition.
 * @return              1 if the two transitions have the same label.
 */
int dfa_transition_compare(const void *a, const void *b) {
    const dfa_transition_t *t1 = (dfa_transition_t *) a;
    const dfa_transition_t *t2 = (dfa_transition_t *) b;

    return t1->label == t2->label;
}

/**
 * @brief Create a new transition.
 *
 * @param[in] origin    Origin state of the transition.
 * @param[in] label     Element of the alphabet.
 * @param[in] target    Target state of the transition.
 * @return              An allocated transition.
 */
dfa_transition_t *dfa_transition_new(size_t origin, char label, size_t target) {
    dfa_transition_t *t = (dfa_transition_t *) alg_malloc(sizeof(dfa_transition_t));

    t->origin = origin;
    t->label = label;
    t->target = target;

    return t;
}

/**
 * @brief Write a path for a given transition.
 *
 * @param[in] a         Transition.
 * @param[in] args      Additional arguments (unused).
 */
static void dfa_transition_tikz(void *a, void *args) {
    ALG_UNUSED(args);

    dfa_transition_t *t = (dfa_transition_t *) a;
    char from[STRING_MAX_SIZE];
    char to[STRING_MAX_SIZE];
    char label[STRING_MAX_SIZE];
    char position[STRING_MAX_SIZE];

    snprintf(from, STRING_MAX_SIZE, "q%lu", t->origin);
    snprintf(to, STRING_MAX_SIZE, "q%lu", t->target);
    snprintf(label, STRING_MAX_SIZE, "%c", t->label);

    switch (t->target - t->origin) {
    case 0:
        if (rand() % 2 == 0) {
            snprintf(position, STRING_MAX_SIZE, "loop below");
        } else {
            snprintf(position, STRING_MAX_SIZE, "loop above");
        }
        break;
    case 1:
        position[0] = '\0';
        break;
    default:
        if (t->target % 2 == 0) {
            snprintf(position, STRING_MAX_SIZE, "bend left=50");
        } else {
            snprintf(position, STRING_MAX_SIZE, "bend right=50");
        }
        break;
    }

    tikz_add_state_path(from, to, position, label);
}

dfa_t *dfa_new(size_t v) {
    dfa_t *dfa = alg_malloc(sizeof(dfa_t *));

    dfa->nstates = v;
    dfa->initial_state = 0;
    dfa->terminal_state = 0;

    dfa->transitions = (linked_list_t **) alg_malloc(v * sizeof(linked_list_t *));
    for (size_t i = 0; i < v; ++i) {
        dfa->transitions[i] = linked_list_new();
    }

    return dfa;
}

void dfa_destroy(dfa_t *dfa) {
    if (dfa == NULL) {
        return;
    }

    for (size_t i = 0; i < dfa->nstates; ++i) {
        linked_list_destroy(dfa->transitions[i], true);
    }

    alg_free(dfa->transitions);
    alg_free(dfa);
}

size_t dfa_terminal_state(const dfa_t *dfa) {
    if (dfa == NULL) {
        return 0;
    }

    return dfa->terminal_state;
}

size_t dfa_initial_state(const dfa_t *dfa) {
    if (dfa == NULL) {
        return 0;
    }

    return dfa->initial_state;
}

size_t dfa_target_state(const dfa_t *dfa, size_t origin, char label) {
    if (dfa == NULL) {
        return origin;
    }

    if (origin >= dfa->nstates) {
        return dfa->nstates;
    }

    dfa_transition_t transition = {.origin = -1, .label = label, .target = -1};
    dfa_transition_t *tmp = (dfa_transition_t *) linked_list_find_data(
        dfa->transitions[origin], dfa_transition_compare, &transition
    );

    if (tmp == NULL) {
        return dfa->nstates;
    }

    return tmp->target;
}

void dfa_set_initial(dfa_t *dfa, size_t state) {
    if (dfa == NULL) {
        return;
    }

    if (state >= dfa->nstates) {
        return;
    }

    dfa->initial_state = state;
}

void dfa_set_terminal(dfa_t *dfa, size_t state) {
    if (dfa == NULL) {
        return;
    }

    if (state >= dfa->nstates) {
        return;
    }

    dfa->terminal_state = state;
}

void dfa_set_transition(dfa_t *dfa, size_t origin, char label, size_t target) {
    if (dfa == NULL) {
        return;
    }

    if (origin >= dfa->nstates || target >= dfa->nstates) {
        return;
    }

    dfa_transition_t *t = dfa_transition_new(origin, label, target);

    /* TODO: check if a transition with "label" already exists from origin */
    linked_list_append(dfa->transitions[origin], t);
}

void dfa_tikz(const dfa_t *dfa, const char *filename) {
    if (dfa == NULL) {
        return;
    }

    char label[STRING_MAX_SIZE];
    char position[STRING_MAX_SIZE];
    char text[STRING_MAX_SIZE];

    tikz_preamble(filename);
    tikz_add_library("arrows");
    tikz_add_library("automata");
    tikz_document_begin();
    tikz_picture_begin(
        "->, >=stealth, auto, node distance=2.5cm, semithick, shorten >=1pt"
    );

    /* write states */
    tikz_add_state("q0", NULL, "$q_0$", true);
    for (size_t i = 1; i < dfa->nstates; ++i) {
        snprintf(label, STRING_MAX_SIZE, "q%lu", i);
        snprintf(position, STRING_MAX_SIZE, "right of=q%lu", i - 1);
        snprintf(text, STRING_MAX_SIZE, "$q_%lu$", i);

        tikz_add_state(label, position, text, false);
    }

    /* write edges */
    for (size_t i = 0; i < dfa->nstates; ++i) {
        linked_list_foreach(dfa->transitions[i], dfa_transition_tikz, NULL);
    }

    tikz_picture_end();
    tikz_document_end();
}
