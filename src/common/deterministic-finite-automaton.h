// SPDX-FileCopyrightText: 2014-2025 Alexandru Fikl <alexfikl@gmail.com>
// SPDX-License-Identifier: MIT

#ifndef ALGORITHMS_DFA_H
#define ALGORITHMS_DFA_H

#include <algorithms.h>
#include <linked-list.h>

typedef struct dfa_t dfa_t;

/**
 * A simple deterministic finite automaton (DFA).
 *
 * A DFA is classically defined by:
 *
 *  * A finite set of states Q. The states are represented by integers
 *    internally and no additional labels are kept. A separate mapping could be
 *    constructed to fill that gap.
 *  * An alphabet Sigma. The alphabet is not explicitly stored by the DFA,
 *    but could be retried by analyzing all possible transitions.
 *  * A set of transitions. Each state will have a variable number of transitions
 *    which are stored as linked lists.
 *  * An initial and a terminal state.
 */
struct dfa_t {
    /** Number of states (or vertices) */
    size_t nstates;

    /** Initial state. (default: 0) */
    size_t initial_state;
    /** Final state. (default: 0) */
    size_t terminal_state;

    /** A list of transitions for each state. */
    linked_list_t **transitions;
};

/**
 * Create a new automaton.
 *
 * :param v: Number of states / vertices.
 * :returns: An allocated automaton.
 */
dfa_t *dfa_new(size_t v);

/**
 * Free an existing automaton.
 */
void dfa_destroy(dfa_t *dfa);

/**
 * Retrieve the id of the initial state.
 *
 * :param dfa: Automaton.
 * :returns: The initial state.
 */
size_t dfa_initial_state(const dfa_t *dfa);

/**
 * Retrieve the id of the terminal state.
 *
 * :param dfa: Automaton.
 * :returns: The terminal state.
 */
size_t dfa_terminal_state(const dfa_t *dfa);

/**
 * Retrieve the id of the target state.
 *
 * :param dfa: Automaton.
 * :param origin: The origin of the transition.
 * :param label: An element of the alphabet describing the transition.
 * :returns: The target state. If there is no target state, an
 *  invalid state is returned (larger than nstates).
 */
size_t dfa_target_state(const dfa_t *dfa, size_t origin, char label);

/**
 * Reset the id of the initial state.
 *
 * :param dfa: Automaton.
 * :param state: New initial state.
 */
void dfa_set_initial(dfa_t *dfa, size_t state);

/**
 * Reset the id of the terminal state.
 *
 * :param dfa: Automaton.
 * :param state: New terminal state.
 */
void dfa_set_terminal(dfa_t *dfa, size_t state);

/**
 * Add a new transition to the DFA.
 *
 * :param dfa: Automaton.
 * :param origin: Origin state of the transition.
 * :param label: Element of the alphabet.
 * :param target: Target state of the transition.
 */
void dfa_set_transition(dfa_t *dfa, size_t origin, char label, size_t target);

/**
 * Write the DFA to a LaTeX file using TIKZ.
 *
 * The states of the DFA are written left to right using the `automata`
 * library. The transitions pose some problem and may overlap, giving
 * suboptimal results.
 *
 * :param dfa: Automaton.
 * :param filename: Filename.
 */
void dfa_tikz(const dfa_t *dfa, const char *filename);

#endif
