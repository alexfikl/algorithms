// SPDX-FileCopyrightText: 2014-2025 Alexandru Fikl <alexfikl@gmail.com>
// SPDX-License-Identifier: MIT

#include "geometry.h"

const struct point geometry_origin = {.x = 0.0, .y = 0.0, .z = 0.0};

struct point *point_new(double x, double y) {
    struct point *p = alg_malloc(sizeof(struct point));
    p->x = x;
    p->y = y;
    p->z = 0.0;

    return p;
}

struct point *point_new_line(struct line *line, double x) {
    return point_new(x, line->m * x + line->b);
}

void point_free(struct point *p) {
    alg_free(p);
}

void point_coordinates(const struct point *p, double *x, double *y, double *z) {
    if (p == NULL) {
        return;
    }

    if (x != NULL) {
        *x = p->x;
    }

    if (y != NULL) {
        *y = p->y;
    }

    if (z != NULL) {
        *z = p->z;
    }
}

double point_x(const struct point *p) {
    if (p == NULL) {
        return 0.0;
    }

    return p->x;
}

void point_set_x(struct point *p, double x) {
    if (p == NULL) {
        return;
    }

    p->x = x;
}

double point_y(const struct point *p) {
    if (p == NULL) {
        return 0.0;
    }

    return p->y;
}

void point_set_y(struct point *p, double y) {
    if (p == NULL) {
        return;
    }

    p->y = y;
}

double point_z(const struct point *p) {
    if (p == NULL) {
        return 0.0;
    }

    return p->z;
}

void point_set_z(struct point *p, double z) {
    if (p == NULL) {
        return;
    }

    p->z = z;
}

void point_dump(const struct point *point) {
    ALG_INFO("point<x=%g, y=%g>", point->x, point->y);
}

bool point_equal(const struct point *p, const struct point *q) {
    return ALG_FUZZY_EQ(p->x, q->x) && ALG_FUZZY_EQ(p->y, q->y) &&
           ALG_FUZZY_EQ(p->z, q->z);
}

struct point *linked_list_point(const linked_list_node_t *node) {
    return (struct point *) node->user_data;
}

struct line *line_new(double m, double b) {
    struct line *line = alg_malloc(sizeof(struct line));
    line->m = m;
    line->b = b;

    return line;
}

struct line *line_new_ext(double x1, double y1, double x2, double y2) {
    ALG_CHECK_ABORT(ALG_FUZZY_EQ(x1, x2), "x coordinates are equal");

    struct line *line = alg_malloc(sizeof(struct line));
    line->m = (y2 - y1) / (x2 - x1);
    line->b = y1 - line->m * x1;

    return line;
}

void line_dump(struct line *line) {
    ALG_INFO("line<m=%g, b=%g>", line->m, line->b);
}

struct line *linked_list_line(const linked_list_node_t *node) {
    return (struct line *) node->user_data;
}

struct segment *segment_new(struct point *A, struct point *B) {
    struct segment *e = alg_malloc(sizeof(struct segment));

    if (ALG_FUZZY_EQ(A->x, B->x) && ALG_FUZZY_EQ(A->y, B->y)) {
        ALG_WARN("A and B are one point");
    }

    /* make sure e->A is the northern point and e->B is the southern point */
    if (A->y > B->y) {
        e->A = A;
        e->B = B;
    } else if (A->y < B->y) {
        e->A = B;
        e->B = A;
    } else if (A->x > B->x) {
        e->A = A;
        e->B = B;
    } else {
        e->A = B;
        e->B = A;
    }

    return e;
}

void segment_dump(const struct segment *edge) {
    ALG_INFO(
        "segment<A<x=%g, y=%g>, B<x=%g, y=%g>",
        edge->A->x,
        edge->A->y,
        edge->B->x,
        edge->B->y
    );
}

struct rectangle *rectangle_new(double x1, double y1, double x2, double y2) {
    struct rectangle *rect = alg_malloc(sizeof(struct rectangle));

    rect->xmin = (x1 < x2 ? x1 : x2);
    rect->ymin = (y1 < y2 ? y1 : y2);
    rect->xmax = (x1 > x2 ? x1 : x2);
    rect->ymax = (y1 > y2 ? y1 : y2);

    return rect;
}

struct rectangle *rectangle_new_ext(double x, double y, double w, double h) {
    return rectangle_new(x, y, x + w, y + h);
}

struct rectangle *rectangle_new_point(const struct point *p, const struct point *q) {
    return rectangle_new(p->x, p->y, q->x, q->y);
}

void rectangle_free(struct rectangle *rect) {
    alg_free(rect);
}

void rectangle_update(struct rectangle *box, const struct point *p) {
    if (box == NULL || p == NULL) {
        return;
    }

    if (p->x < box->xmin) {
        box->xmin = p->x;
    }
    if (p->y < box->ymin) {
        box->ymin = p->y;
    }
    if (p->x > box->xmax) {
        box->xmax = p->x;
    }
    if (p->y > box->ymax) {
        box->ymax = p->y;
    }
}

double rectangle_width(const struct rectangle *rect) {
    if (rect == NULL) {
        return 0.0;
    }

    return rect->xmax - rect->xmin;
}

double rectangle_height(const struct rectangle *rect) {
    if (rect == NULL) {
        return 0.0;
    }

    return rect->ymax - rect->ymin;
}

void rectangle_coordinates(
    const struct rectangle *rect, double *x1, double *y1, double *x2, double *y2
) {
    if (rect == NULL) {
        return;
    }

    if (x1 != NULL) {
        *x1 = rect->xmin;
    }
    if (y1 != NULL) {
        *y1 = rect->ymin;
    }

    if (x2 != NULL) {
        *x2 = rect->xmax;
    }
    if (y2 != NULL) {
        *y2 = rect->ymax;
    }
}

void rectangle_enlarge(struct rectangle *rect, double dx, double dy) {
    if (rect == NULL) {
        return;
    }

    rect->xmin -= dx;
    rect->ymin -= dy;
    rect->xmax += dx;
    rect->ymax += dy;
}

double point_distance(const struct point *A, const struct point *B) {
    return sqrt(pow(A->x - B->x, 2) + pow(A->y - B->y, 2) + pow(A->z - B->z, 2));
}

double segment_length(const struct segment *edge) {
    return point_distance(edge->A, edge->B);
}

int rectangle_contains(const struct rectangle *rect, const struct point *p) {
    if (ALG_FUZZY_EQ(rect->xmin, p->x) || ALG_FUZZY_EQ(rect->xmax, p->x) ||
        ALG_FUZZY_EQ(rect->ymin, p->y) || ALG_FUZZY_EQ(rect->ymax, p->y)) {
        return 0;
    }

    if (rect->xmin < p->x && p->x < rect->xmax && rect->ymin < p->y &&
        p->y < rect->ymax) {
        return 1;
    }

    return -1;
}

int geometry_point_comparison_wrapper(const void *p1, const void *p2) {
    return geometry_point_comparison(
        *(struct point *const *) p1, *(struct point *const *) p2
    );
}

int geometry_point_comparison(const struct point *p1, const struct point *p2) {
    /* check the biggest x */
    if (p1->x > p2->x) {
        return 1;
    }
    if (p1->x < p2->x) {
        return -1;
    }

    /* if the x coordinates are equal, check y */
    if (p1->y > p2->y) {
        return 1;
    }
    if (p1->y < p2->y) {
        return -1;
    }

    /* the two points are the same */
    return 0;
}

double geometry_triangle_area(
    const struct point *A, const struct point *B, const struct point *C
) {
    return 0.5 * ((B->x - A->x) * (C->y - A->y) - (B->y - A->y) * (C->x - A->x));
}

bool geometry_collinear(
    const struct point *A, const struct point *B, const struct point *C
) {
    return ALG_FUZZY_ZERO(geometry_triangle_area(A, B, C));
}

bool geometry_counterclockwise(
    const struct point *A, const struct point *B, const struct point *C
) {
    return geometry_triangle_area(A, B, C) > ALG_THRESHOLD;
}

bool geometry_clockwise(
    const struct point *A, const struct point *B, const struct point *C
) {
    return geometry_triangle_area(A, B, C) < -ALG_THRESHOLD;
}

double
geometry_angle(const struct point *A, const struct point *B, const struct point *C) {
    /* <AB, BC> = ||AB|| * ||BC|| * cos(theta) */
    return acos(
        ((B->x - A->x) * (B->x - C->x) + (B->y - A->y) * (B->y - C->y)) /
        (point_distance(A, B) * point_distance(B, C))
    );
}

int geometry_segment_intersection(
    const struct segment *p, const struct segment *q, struct point *r
) {
    if (p == NULL || q == NULL || r == NULL) {
        return -2;
    }

    struct point *pa = p->A;
    struct point *pb = p->B;
    struct point *qa = q->A;
    struct point *qb = q->B;
    double D = 0;
    double s = 0;
    double t = 0;

    D = pa->x * (qb->y - qa->y) + pb->x * (qa->y - qb->y) + qa->x * (pb->y - pa->y) +
        qb->x * (pa->y - pb->y);

    if (ALG_FUZZY_ZERO(D)) {
        return 0;
    }

    s = (pa->x * (qb->y - qa->y) + qa->x * (pa->y - qb->y) + qb->x * (qa->y - pa->y)) /
        D;

    t = -(pa->x * (qa->y - pb->y) + pb->x * (pa->y - qa->y) + qa->x * (pb->y - pa->y)) /
        D;

    if (!ALG_FUZZY_CONTAINS(s, 0.0, 1.0) || !ALG_FUZZY_CONTAINS(t, 0.0, 1.0)) {
        return -1;
    }

    r->x = pa->x + s * (pb->x - pa->x);
    r->y = pa->y + s * (pb->y - pa->y);

    return 1;
}

bool geometry_parabola_intersection(
    const struct point *p1, const struct point *p2, double xd, struct point *p
) {
    double h1 = 0;
    double h2 = 0;
    double a = 0;
    double b = 0;
    double c = 0;
    double y1 = 0;
    double y2 = 0;
    struct point p0 = *p1;

    /*
     * We have 4 different cases:
     *  * x_1 == x_2 means that the parabolas have the same shape, just
     * different y coordinates: the intersection is always the middle y point.
     *  * x_1 is on the directrix means the parabola given by p_1 is just a
     * a line: the intersection is at y_1.
     *  * x_2 is on the directrix: the intersection is at y_2.
     *  * otherwise we have to compute the intersection normally.
     */
    if (ALG_FUZZY_EQ(p1->x, p2->x)) {
        p->y = (p1->y + p2->y) / 2.0;
    } else if (ALG_FUZZY_EQ(p1->x, xd)) {
        p->y = p1->y;
        p0 = *p2;
    } else if (ALG_FUZZY_EQ(p2->x, xd)) {
        p->y = p2->y;
    } else {
        h1 = 2.0 * (p1->x - xd);
        h2 = 2.0 * (p2->x - xd);

        a = (1 / h2 - 1 / h1);
        b = -2.0 * (p2->y / h2 - p1->y / h1);
        c = (pow(p2->x, 2) + pow(p2->y, 2)) / h2 - (pow(p1->x, 2) + pow(p1->y, 2)) / h1;
        if ((b * b - 4.0 * a * c) < -ALG_THRESHOLD) {
            ALG_ERROR("parabolas do not intersect");
            return false;
        }

        y1 = (-b - sqrt(b * b - 4.0 * a * c)) / (2.0 * a);
        y2 = (-b + sqrt(b * b - 4.0 * a * c)) / (2.0 * a);
        if (p1->y < p2->y) {
            p->y = fmax(y1, y2);
        } else {
            p->y = fmin(y1, y2);
        }
    }

    /* compute x by plugging y into the equation of p1 or p2 */
    p->x = (pow(p0.x, 2) + pow(p0.y - p->y, 2) - pow(xd, 2)) / (2.0 * (p0.x - xd));

    return true;
}

int geometry_segment_clip(
    const struct segment *segment, const struct rectangle *rect, struct segment *clip
) {
    if (segment == NULL || rect == NULL || clip == NULL) {
        return -1;
    }

    struct point *A = segment->A;
    struct point *B = segment->B;
    double dx = B->x - A->x;
    double dy = B->y - A->y;
    double p = 0.0;
    double q = 0.0;
    double r = 0.0;
    double ta = 0.0;
    double tb = 1.0;

    clip->A->x = clip->B->x = 0.0;
    clip->A->y = clip->B->y = 0.0;

    for (int edge = 0; edge < 4; ++edge) {
        switch (edge) {
        case 0: /* left edge    */
            p = -dx;
            q = -(rect->xmin - A->x);
            break;
        case 1: /* right edge   */
            p = dx;
            q = (rect->xmax - A->x);
            break;
        case 2: /* bottom edge  */
            p = -dy;
            q = -(rect->ymin - A->y);
            break;
        case 3: /* top edge     */
            p = dy;
            q = (rect->ymax - A->y);
            break;
        default:
            ALG_UNREACHABLE();
            break;
        }

        /* line parallel to edge, outside of the rectangle */
        if (ALG_FUZZY_ZERO(p) && q < 0) {
            return 0;
        }

        r = q / p;

        /* line is completely outside the rectangle */
        if ((p < 0.0 && r > tb) || (p > 0.0 && r < ta)) {
            return 0;
        }

        if (p < 0) {
            ta = r;
        } else {
            tb = r;
        }
    }

    clip->A->x = A->x + ta * dx;
    clip->A->y = A->y + ta * dy;
    clip->B->x = B->x + tb * dx;
    clip->B->y = B->y + tb * dy;

    return 1;
}

bool geometry_circle(
    struct point *a,
    struct point *b,
    struct point *c,
    struct point *center,
    double *radius
) {
    if (geometry_collinear(a, b, c)) {
        return false;
    }

    if (!geometry_counterclockwise(a, b, c)) {
        return false;
    }

    double A = b->x - a->x;
    double B = b->y - a->y;
    double C = c->x - a->x;
    double D = c->y - a->y;
    double E = A * (a->x + b->x) + B * (a->y + b->y);
    double F = C * (a->x + c->x) + D * (a->y + c->y);
    double G = 2.0 * (A * (c->y - b->y) - B * (c->x - b->x));

    center->x = (D * E - B * F) / G;
    center->y = (A * F - C * E) / G;
    *radius = sqrt(pow(a->x - center->x, 2) + pow(a->y - center->y, 2));

    return true;
}
