// SPDX-FileCopyrightText: 2014-2025 Alexandru Fikl <alexfikl@gmail.com>
// SPDX-License-Identifier: MIT

#ifndef ALGORITHMS_GEOMETRY_H
#define ALGORITHMS_GEOMETRY_H

#include <algorithms.h>
#include <linked-list.h>

/**
 * Definition of a 3D point with coordinates (x, y, z).
 */
struct point {
    /** Coordinate. */
    double x;
    /** Coordinate. */
    double y;
    /** Coordinate. */
    double z;
};

/**
 * Definition of a line (with no bounds).
 */
struct line {
    /** Slope of the line. */
    double m;
    /** y-intercept of the line. */
    double b;
};

/**
 * Definition of a segment between two points.
 */
struct segment {
    /** Northern point, eastern if horizontal. */
    struct point *A;
    /** Southern point, western if horizontal. */
    struct point *B;
};

/**
 * Definition of a rectangle.
 */
struct rectangle {
    /** Bottom left coordinates of the rectangle. */
    double xmin;
    double ymin;
    /** Upper right coordinates of the rectangle. */
    double xmax;
    double ymax;
};

/**
 * Point denoting the origin.
 */
extern const struct point geometry_origin;

/**
 * Create a new point.
 */
struct point *point_new(double x, double y);

/**
 * Free a point.
 */
void point_free(struct point *p);

/**
 * Create a new point on a given line.
 *
 * :param line: Line on which the point should be.
 * :param x: :math:`x` coordinate of the point.
 */
struct point *point_new_line(struct line *line, double x);

/**
 * Retrieve the coordinates of a given point.
 */
void point_coordinates(const struct point *p, double *x, double *y, double *z);

/**
 * Get x coordinate of a given point.
 */
double point_x(const struct point *p);

/**
 * Set x coordinate of a given point.
 */
void point_set_x(struct point *p, double x);

/**
 * Get y coordinate of a given point.
 */
double point_y(const struct point *p);

/**
 * Set y coordinate of a given point.
 */
void point_set_y(struct point *p, double y);

/**
 * Get z coordinate of a given point.
 */
double point_z(const struct point *p);

/**
 * Set z coordinate of a given point.
 */
void point_set_z(struct point *p, double z);

/**
 * Write the point coordinates to stdout.
 *
 * :param point: Point to write.
 */
void point_dump(const struct point *point);

/**
 * Check if two points are equal.
 *
 * :returns: `true` if the points are equal using :c:func:`ALG_FUZZY_EQ`.
 */
bool point_equal(const struct point *p, const struct point *q);

/**
 * Get a point struct from inside a linked list node.
 *
 * :param node: Linked list node.
 * :returns: Point contained in the node. Note that we can't know
 *  that the data contained in the node is actually a point,
 *  so this simply casts the void pointer to a point.
 */
struct point *linked_list_point(const linked_list_node_t *node);

/**
 * Create a new line from a slope and intercept.
 *
 * :param m: Slope of the line.
 * :param b: Intercept of the line.
 * :returns: A new line.
 */
struct line *line_new(double m, double b);

/**
 * Create a new line from two points.
 *
 * :param x1: x coordinate of the first point.
 * :param y1: y coordinate of the first point.
 * :param x2: x coordinate of the second point.
 * :param y2: y coordinate of the second point.
 * :returns: A new line.
 */
struct line *line_new_ext(double x1, double y1, double x2, double y2);

/**
 * Write the line information to stdout.
 */
void line_dump(struct line *line);

/**
 * Get a struct line struct from inside a linked list node.
 *
 * :param node: Linked list node.
 * :returns: Line contained in the node. Note that we can't know
 *  that the data contained in the node is actually a line,
 *  so this simply casts the void pointer to a line.
 */
struct line *linked_list_line(const linked_list_node_t *node);

/**
 * Create a new segment.
 *
 * When stored in the segment, the ``A`` node will be the northern (eastern if the
 * segment is completely horizontal) node and ``B`` will be the southern (western
 * if the segment is completely horizontal) node.
 *
 * :param A: Endpoint of the edge.
 * :param B: Endpoint of the edge.
 */
struct segment *segment_new(struct point *A, struct point *B);

/**
 * Write the coordinates of the two edge points to stdout.
 */
void segment_dump(const struct segment *edge);

/**
 * Get a segment struct from inside a linked list node.
 *
 * :param node: Linked list node.
 * :returns: Segment contained in the node. Note that we can't know
 *  that the data contained in the node is actually a segment,
 *  so this simply casts the void pointer to an segment.
 */
struct segment *linked_list_segment(const linked_list_node_t *node);

/**
 * Create a new rectangle.
 *
 * :param x1: x coordinate of the lower left corner.
 * :param y1: y coordinate of the lower left corner.
 * :param x2: x coordinate of the upper right corner.
 * :param y2: y coordinate of the upper right corner.
 * :returns: New rectangle.
 */
struct rectangle *rectangle_new(double x1, double y1, double x2, double y2);

/**
 * Create a new rectangle.
 *
 * :param x: x coordinate of the lower left corner.
 * :param y: y coordinate of the lower left corner.
 * :param w: Width of the rectangle.
 * :param h: Height of the rectangle.
 * :returns: New rectangle.
 */
struct rectangle *rectangle_new_ext(double x, double y, double w, double h);

/**
 * Create a new rectangle.
 *
 * :param p: Lower left corner coordinates.
 * :param q: Upper right corner coordinates.
 */
struct rectangle *rectangle_new_point(const struct point *p, const struct point *q);

/**
 * Free a rectangle.
 */
void rectangle_free(struct rectangle *rect);

/**
 * Get rectangle coordinates.
 *
 * :param rect: Rectangle.
 * :param x1: x coordinate of the lower left corner.
 * :param y1: y coordinate of the lower left corner.
 * :param x2: x coordinate of the upper right corner.
 * :param y2: y coordinate of the upper right corner.
 */
void rectangle_coordinates(
    const struct rectangle *rect, double *x1, double *y1, double *x2, double *y2
);

/**
 * Compute the rectangle width.
 */
double rectangle_width(const struct rectangle *rect);

/**
 * Compute the rectangle height.
 */
double rectangle_height(const struct rectangle *rect);

/**
 * Uniformly enlarge the rectangle in both dimensions.
 *
 * :param rect: Rectangle.
 * :param dx: Amount to grow in the x dimension.
 * :param dy: Amount to grow in the y dimension.
 */
void rectangle_enlarge(struct rectangle *rect, double dx, double dy);

/**
 * Update the rectangle to include given point.
 *
 * In this case, we use the rectangle as a bounding box and we grow it to
 * include the given point.
 *
 * :param box: Bounding box.
 * :param p: New point.
 */
void rectangle_update(struct rectangle *box, const struct point *p);

/**
 * Check if a point is in a rectangle.
 *
 * Note that if the point is on the boundary of the rectangle, the function
 * returns false.
 *
 * :param rect: Given rectangle.
 * :param p: Point.
 * :returns: 0 if the point is on the boundary, 1 if if it inside
 *  the rectangle and -1 if it is outside the rectangle.
 */
int rectangle_contains(const struct rectangle *rect, const struct point *p);

/**
 * Compute the Euclidean distance between two points.
 */
double point_distance(const struct point *A, const struct point *B);

/**
 * Compute the length of the edge.
 */
double segment_length(const struct segment *edge);

/**
 * Compare two points for lexicographical ordering.
 *
 * Lexicographical ordering is:
 *
 *  .. math::
 *
 *      (x_1, y_1) < (x_2, y_2) \Leftrightarrow
 *          x_1 < x_2 \text{ or }
 *          (x_1 = x_2 \text{ and } y_1 < y_2).
 *
 * :param p1: Point.
 * :param p2: Point.
 * :returns: `0` if the two points are the same, `1` if ``p1`` is
 *  "greater" than ``p2`` and `-1` if ``p1`` is "smaller" than ``p2``.
 */
int geometry_point_comparison(const struct point *p1, const struct point *p2);

/**
 * Wrapper for geometry_point_comparison for use by qsort().
 */
int geometry_point_comparison_wrapper(const void *p1, const void *p2);

/**
 * Compute a signed triangle area.
 *
 * If the three points A, B and C are in counter-clockwise order, then the
 * area is positive, otherwise it is negative.
 *
 * :param A: Vertex of the triangle.
 * :param B: Vertex of the triangle.
 * :param C: Vertex of the triangle.
 * :returns: Signed area of the triangle.
 */
double geometry_triangle_area(
    const struct point *A, const struct point *B, const struct point *C
);

/**
 * Check if three points are collinear.
 *
 * The test employed here uses the area of the triangle described by the three
 * points. If the area is 0, it means they are indeed collinear.
 */
bool geometry_collinear(
    const struct point *A, const struct point *B, const struct point *C
);

/**
 * Check if the three points are in counter-clockwise order.
 */
bool geometry_counterclockwise(
    const struct point *A, const struct point *B, const struct point *C
);

/**
 * Check if the three points are in clockwise order.
 */
bool geometry_clockwise(
    const struct point *A, const struct point *B, const struct point *C
);

/**
 * Compute the angle between 3 points.
 *
 * :returns: The angle between the lines defined by the 3 points.
 */
double
geometry_angle(const struct point *A, const struct point *B, const struct point *C);

/**
 * Compute the intersection of two segments.
 *
 * :param p: Segment.
 * :param q: Segment.
 * :param r: Intersection point, if any.
 * :returns: The following return values are given:
 *
 *  * -2: invalid parameters.
 *  * -1: the segments do not intersect.
 *  * 0:the segments are parallel. This can occur in multiple cases: they
 *    are actually parallel or they overlap in more than 1 point.
 *  * 1: the segments intersect in ``r``.
 *
 * :seealso: Computational Geometry in C, Joseph O'Rourke, Second Edition, pp. 222.
 */
int geometry_segment_intersection(
    const struct segment *p, const struct segment *q, struct point *r
);

/**
 * Compute the intersection of two parabolas.
 *
 * The equation of a parabola, given a focus point :math:`(x_f, y_f)` and a
 * directrix :math:`x = x_d`, can be found by using the property that the
 * distances from a point :math:`(x, y)` to the focus and to the directrix
 * are equal
 *
 *  .. math::
 *
 *      \sqrt{(x - x_f)^2 + (y - y_f)^2} = |x - x_d|.
 *
 * By squaring the equation above and simplifying, we get:
 *
 *  .. math::
 *
 *      (y - y_f)^2 + (x_f^2 - x_d^2) = 2 (x_f - x_d) x
 *
 * which can be further simplified into the classical form:
 *
 *  .. math::
 *
 *      (y - y_0)^2 = 4h(x - x_0),
 *
 * where :math:`(x_0, y_0)` is any point on the parabola. Furthermore, we
 * can rewrite in the form:
 *
 *  .. math::
 *
 *      a y^2 + b y + c = x
 *
 * with the coefficients:
 *
 *  .. math::
 *
 *      \begin{cases}
 *          a = \frac{1}{2(x_f - x_d)} = \frac{1}{4h} \\
 *          b = -\frac{2 y_f}{4h} \\
 *          c = \frac{x_f^2 - x_d^2}{4h}.
 *      \end{cases}
 *
 * To find the intersection point, we use the above form for each one
 * and then solve the quadratic equation:
 *
 *  .. math::
 *
 *      (a_2 - a_1) y^2 + (b_2 - b_1) y + (c_2 - c_1) = 0
 *
 * to find the :math:`y` coordinate of the intersection. The :math:`x`
 * coordinate can then be found by replacing :math:`y` into the equation of
 * either of the two parabolas.
 *
 * :param p1: Point on the first parabola.
 * :param p2: Point on the second parabola.
 * :param xd: Directrix.
 * :param p: The intersection point of the two parabolas.
 * :returns: `true` if the two parabolas intersect, `false` otherwise.
 */
bool geometry_parabola_intersection(
    const struct point *p1, const struct point *p2, double xd, struct point *p
);

/**
 * Clip a segment to the inside of a rectangle.
 *
 * We use the Liang-Barsky line clipping algorithm to clip the given segment.
 *
 * :param segment: Unclipped segment.
 * :param rect: Rectangle.
 * :param clipped: Clipped segment.
 * :returns: Return values:
 *
 *  * -1: invalid parameters (`NULL`).
 *  * 0: segment is completely outside the rectangle.
 *  * 1: segment was clipped.
 *
 * :seealso: http://www.skytopia.com/project/articles/compsci/clipping.html
 */
int geometry_segment_clip(
    const struct segment *segment, const struct rectangle *rect, struct segment *clipped
);

/**
 * Compute the circle defined by 3 points.
 *
 * The center and radius of the circle are easily found by using the circle
 * formula:
 *
 *  .. math::
 *
 *      (x - x_0)^2 + (y - y_0)^2 = r^2
 *
 * and replacing the coordinates of the 3 points to get 3 equations. Once
 * the center :math:`(x_0, y_0)` is found, we can replace it in the above
 * formula to get the radius.
 *
 * :param a: Point on the circle.
 * :param b: Point on the circle.
 * :param c: Point on the circle.
 * :param center: Center of the circle formed by the 3 points.
 * :param radius: Radius of the circle.
 * :returns: `true` if the 3 points form a circle. `false` if the points
 *  are collinear or not in counterclockwise order.
 *
 * :seealso: *Computational Geometry in C*, Josepth O'Rourke, Second Edition, pp. 189.
 */
bool geometry_circle(
    struct point *a,
    struct point *b,
    struct point *c,
    struct point *center,
    double *radius
);

#endif /* !ALGORITHMS_GEOMETRY_H */
