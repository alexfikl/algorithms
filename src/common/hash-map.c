// SPDX-FileCopyrightText: 2014-2025 Alexandru Fikl <alexfikl@gmail.com>
// SPDX-License-Identifier: MIT

#include "hash-map.h"

/* Murmur Hash 2 constants */
static const uint32_t MURMUR_HASH_M = 0x5bd1e995;
static const uint32_t MURMUR_HASH_R = 24;

/* FNV-1a Hash constants */
/* @sa http://www.isthe.com/chongo/tech/comp/fnv/index.html#FNV-param */
static const uint32_t FNV_HASH_PRIME = 16777619;
static const uint32_t FNV_HASH_OFFSET_BASIS = 2166136261;

/* ADLER Hash constants */
static const uint32_t ADLER_HASH_MOD = 65521;

/* DJB Hash constants */
static const uint32_t DJB_HASH_CONSTANT = 5381;

/**
 * @brief Create a new hash map entry.
 *
 * @param[in] key           Key in the entry.
 * @param[in] value         Value in the entry.
 * @param[in] hash          Hash of the given key.
 * @return                  A newly initialized entry.
 */
static struct hash_entry_t *hash_entry_new(void *key, void *value, uint32_t hash) {
    struct hash_entry_t *entry = alg_calloc(1, sizeof(struct hash_entry_t));
    entry->key = key;
    entry->value = value;
    entry->hash = hash;

    return entry;
}

/**
 * @brief Retrieve a hash map entry from a linked list node.
 *
 * @param[in] node          Node that should contain a hash map entry.
 * @return                  Hash map entry contained in the node. Note that
 *                          this is just a cast on the pointer contained in
 *                          the node.
 */
static struct hash_entry_t *linked_list_hash_entry(const linked_list_node_t *node) {
    if (node == NULL) {
        return NULL;
    }

    return (struct hash_entry_t *) node->user_data;
}

/**
 * @brief Retrieve the bucket for a given key.
 *
 * @param[in] map               Hash map.
 * @param[in] key               Key for which to look for.
 * @param[out] hash             If not NULL, the hash of the key is returned.
 * @param[in] create            If true and the bucket for the given key does
 *                              not exist, we create it and return it.
 * @return                      The bucket containing the key, or a new bucket
 *                              if the bucket does not exist and create is true.
 */
static linked_list_t *hash_map_find_bucket(
    const struct hash_map_t *map, const void *key, uint32_t *hash, bool create
) {
    if (map == NULL || key == NULL) {
        return NULL;
    }

    linked_list_t *bucket = NULL;
    uint32_t key_hash = 0;

    /* compute the hash for the current key */
    key_hash = map->hash(key, map->key_size, ALG_HASH_DEFAULT_SEED);
    key_hash = key_hash % map->bucket_count;

    /* get the bucket */
    bucket = map->buckets[key_hash];
    if (bucket == NULL && create) {
        bucket = map->buckets[key_hash] = linked_list_new();
    }

    if (hash != NULL) {
        *hash = key_hash;
    }

    return bucket;
}

/**
 * @brief Find a node in a given bucket.
 *
 * @param[in] map           Hash map.
 * @param[in] bucket        Linked list of hash map entries.
 * @param[in] key           Key we are looking for.
 * @param[in] hash          Hash of the given key.
 * @return                  A node that contains the key. If there is none,
 *                          NULL is returned.
 */
static linked_list_node_t *hash_map_find_node(
    const struct hash_map_t *map,
    const linked_list_t *bucket,
    const void *key,
    uint32_t hash
) {
    if (map == NULL || bucket == NULL || key == NULL) {
        return NULL;
    }

    struct hash_entry_t *entry = NULL;
    linked_list_node_t *key_node = NULL;
    linked_list_node_t *node = NULL;

    node = bucket->head;
    while (node) {
        entry = linked_list_hash_entry(node);
        if (entry->hash == hash) {
            if ((map->cmp && map->cmp(entry->key, key) == 0) ||
                (memcmp(entry->key, key, map->key_size) == 0)) {
                key_node = node;
                break;
            }
        }

        node = node->next;
    }

    return key_node;
}

uint32_t hash_murmur(const uint8_t *key, size_t size, uint32_t seed) {
    uint32_t h = (uint32_t) (seed ^ size);
    uint32_t i = 0;
    uint32_t k = 0;

    while ((size - i) >= 4) {
        memcpy(&k, key + i, sizeof(uint32_t));

        k *= MURMUR_HASH_M;
        k ^= k >> MURMUR_HASH_R;
        k *= MURMUR_HASH_M;

        h *= MURMUR_HASH_M;
        h ^= k;

        i += sizeof(uint32_t);
    }

    /* handle the last few bytes of the input array */
    switch (size - i) {
    case 3:
        h ^= (uint32_t) (key[i + 2] << 16);
        ALG_FALLTHROUGH();
    case 2:
        h ^= (uint32_t) (key[i + 1] << 8);
        ALG_FALLTHROUGH();
    case 1:
        h ^= key[i];
        h *= MURMUR_HASH_M;
    }

    h ^= h >> 13;
    h *= MURMUR_HASH_M;
    h ^= h >> 15;

    return h;
}

uint32_t hash_jenkins(const uint8_t *key, size_t size, uint32_t seed) {
    ALG_UNUSED(seed);

    uint32_t hash = 0;

    for (size_t i = 0; i < size; ++i) {
        hash += key[i];
        hash += (hash << 10);
        hash ^= (hash >> 6);
    }

    hash += (hash << 3);
    hash ^= (hash >> 11);
    hash += (hash << 15);

    return hash;
}

uint32_t hash_fnv_1a(const uint8_t *key, size_t size, uint32_t seed) {
    ALG_UNUSED(seed);

    uint32_t hash = FNV_HASH_OFFSET_BASIS;

    for (size_t i = 0; i < size; ++i) {
        hash ^= key[i];
        hash *= FNV_HASH_PRIME;
    }

    return hash;
}

uint32_t hash_adler(const uint8_t *key, size_t size, uint32_t seed) {
    ALG_UNUSED(seed);

    uint32_t a = 1;
    uint32_t b = 0;

    for (size_t i = 0; i < size; ++i) {
        a = (a + key[i]) % ADLER_HASH_MOD;
        b = (b + a) % ADLER_HASH_MOD;
    }

    return (b << 16) | a;
}

uint32_t hash_djb(const uint8_t *key, size_t size, uint32_t seed) {
    ALG_UNUSED(seed);

    uint32_t hash = DJB_HASH_CONSTANT;

    for (size_t i = 0; i < size; ++i) {
        hash = ((hash << 5) + hash) + key[i];
    }

    return hash;
}

struct hash_map_t *
hash_map_new(size_t key_size, hash_key_compare_t cmp, hash_function_t hash) {
    return hash_map_new_ext(ALG_HASH_MAP_BUCKETS, key_size, cmp, hash);
}

struct hash_map_t *hash_map_new_ext(
    size_t bucket_count, size_t key_size, hash_key_compare_t cmp, hash_function_t hash
) {
    struct hash_map_t *map = alg_calloc(1, sizeof(struct hash_map_t));
    ALG_CHECK_ABORT(key_size != 0, "key size is zero");

    map->cmp = cmp;
    map->hash = (hash != NULL ? hash : hash_djb);

    map->key_size = key_size;
    map->entry_count = 0;
    map->bucket_count = bucket_count;
    map->buckets =
        (linked_list_t **) alg_calloc(map->bucket_count, sizeof(linked_list_t *));

    return map;
}

void hash_map_destroy(struct hash_map_t *map, bool destroy_data) {
    if (map == NULL) {
        return;
    }

    struct hash_entry_t *entry = NULL;
    linked_list_node_t *node = NULL;

    for (size_t i = 0; i < map->bucket_count; ++i) {
        if (map->buckets[i] == NULL) {
            continue;
        }

        if (destroy_data) {
            node = map->buckets[i]->head;
            while (node != NULL) {
                entry = linked_list_hash_entry(node);
                alg_free(entry->key);
                alg_free(entry->value);

                node = node->next;
            }
        }

        linked_list_destroy(map->buckets[i], true);
    }

    alg_free(map->buckets);
    alg_free(map);
}

size_t hash_map_size(const struct hash_map_t *map) {
    if (map == NULL) {
        return 0;
    }

    return map->entry_count;
}

void hash_map_set(struct hash_map_t *map, void *key, void *value) {
    if (map == NULL || key == NULL) {
        return;
    }

    uint32_t hash = 0;
    linked_list_t *bucket = hash_map_find_bucket(map, key, &hash, true);

    struct hash_entry_t *entry = hash_entry_new(key, value, hash);
    linked_list_append(bucket, entry);

    map->entry_count += 1;
}

bool hash_map_set_unique(struct hash_map_t *map, void *key, void *value) {
    if (map == NULL || key == NULL) {
        return false;
    }

    uint32_t hash = 0;
    struct hash_entry_t *entry = NULL;
    linked_list_node_t *node = NULL;

    linked_list_t *bucket = hash_map_find_bucket(map, key, &hash, true);

    /* look for the key in the bucket */
    node = hash_map_find_node(map, bucket, key, hash);
    entry = linked_list_hash_entry(node);

    /* if it does not exist, add it */
    if (entry == NULL) {
        entry = hash_entry_new(key, value, hash);
        linked_list_append(bucket, entry);

        map->entry_count += 1;
        return true;
    }

    return false;
}

void *hash_map_get(const struct hash_map_t *map, const void *key) {
    if (map == NULL) {
        return NULL;
    }

    uint32_t hash = 0;

    linked_list_t *bucket = hash_map_find_bucket(map, key, &hash, false);
    linked_list_node_t *node = hash_map_find_node(map, bucket, key, hash);
    struct hash_entry_t *entry = linked_list_hash_entry(node);

    if (entry != NULL) {
        return entry->value;
    }

    return NULL;
}

void hash_map_remove(struct hash_map_t *map, const void *key, bool destroy_data) {
    if (map == NULL || key == NULL) {
        return;
    }

    uint32_t hash = 0;
    linked_list_t *bucket = hash_map_find_bucket(map, key, &hash, false);
    linked_list_node_t *node = hash_map_find_node(map, bucket, key, hash);
    struct hash_entry_t *entry = linked_list_hash_entry(node);

    if (entry == NULL) {
        return;
    }

    if (destroy_data) {
        alg_free(entry->key);
        alg_free(entry->value);
    }

    linked_list_remove(bucket, node, true);
    map->entry_count -= 1;
}

int hash_map_foreach(const struct hash_map_t *map, hash_iterator_t apply, void *args) {
    if (map == NULL || apply == NULL) {
        return 0;
    }

    int ret = 0;
    struct hash_entry_t *entry = NULL;
    linked_list_node_t *node = NULL;

    for (size_t i = 0; i < map->bucket_count; ++i) {
        if (map->buckets[i] == NULL) {
            continue;
        }

        node = map->buckets[i]->head;
        while (node) {
            entry = linked_list_hash_entry(node);
            ret = apply(entry->key, entry->value, args);
            if (ret != 0) {
                return ret;
            }

            node = node->next;
        }
    }

    return ret;
}
