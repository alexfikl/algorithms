// SPDX-FileCopyrightText: 2014-2025 Alexandru Fikl <alexfikl@gmail.com>
// SPDX-License-Identifier: MIT

#ifndef ALGORITHMS_HASH_MAP_H
#define ALGORITHMS_HASH_MAP_H

#include <linked-list.h>

#ifndef ALG_HASH_MAP_BUCKETS
#define ALG_HASH_MAP_BUCKETS 128
#endif

#ifndef ALG_HASH_DEFAULT_SEED
#define ALG_HASH_DEFAULT_SEED 0
#endif

/**
 * Type definition for a hash function.
 *
 * :param key: Key to be hashed.
 * :param size: Size, in bytes, of the key.
 * :param seed: Seed for the hash.
 * :returns: The 32bit hash of the key.
 */
typedef uint32_t (*hash_function_t)(const uint8_t *key, size_t size, uint32_t seed);

/**
 * Comparison function for hash map keys.
 */
typedef int (*hash_key_compare_t)(const void *a, const void *b);

/**
 * Function to apply to a (key, value) pair.
 *
 * :param args: Extra arguments passed to the function.
 */
typedef int (*hash_iterator_t)(void *key, void *value, void *args);

/**
 * Implementation of the MurmurHash.
 *
 * NOTE: This is not a cryptographic hash function.
 *
 * :param key: Data to hash.
 * :param size: Size, in bytes, of the key.
 * :param seed: Seed for the has function.
 * :returns: The hash of the key.
 *
 * :seealso: http://en.wikipedia.org/wiki/MurmurHash and
 *  https://github.com/aappleby/smhasher/blob/92cf3702fcfaadc84eb7bef59825a23e0cd84f56/src/MurmurHash2.cpp
 */
uint32_t hash_murmur(const uint8_t *key, size_t size, uint32_t seed);

/**
 * Implementation of Bob Jenkins' one-at-a-time hash function.
 *
 * NOTE: This is not a cryptographic hash function.
 *
 * :param key: Data to hash.
 * :param size: Size, in bytes, of the key.
 * :param seed: Seed for the has function. (Unused)
 * :returns: The hash of the key.
 *
 * :seealso: https://en.wikipedia.org/wiki/Jenkins_hash_function
 */
uint32_t hash_jenkins(const uint8_t *key, size_t size, uint32_t seed);

/**
 * Implementation of the Fowler-Noll-Vo-1a hash function.
 *
 * NOTE: This is not a cryptographic hash function.
 *
 * :param key: Data to hash.
 * :param size: Size, in bytes, of the key.
 * :param seed: Seed for the has function. (Unused)
 * :returns: The hash of the key.
 *
 * :seealso: https://en.wikipedia.org/wiki/Fowler%E2%80%93Noll%E2%80%93Vo_hash_function
 */
uint32_t hash_fnv_1a(const uint8_t *key, size_t size, uint32_t seed);

/**
 * Implementation of the Adler-32 hash function.
 *
 * NOTE: This is not a cryptographic hash function.
 *
 * :param key: Data to hash.
 * :param size: Size, in bytes, of the key.
 * :param seed: Seed for the has function. (Unused)
 * :returns: The hash of the key.
 *
 * :seealso: https://en.wikipedia.org/wiki/Adler-32
 */
uint32_t hash_adler(const uint8_t *key, size_t size, uint32_t seed);

/**
 * Implementation of Daniel J. Bernstein's hash function.
 *
 * NOTE: This is not a cryptographic hash function.
 *
 * :param key: Data to hash.
 * :param size: Size, in bytes, of the key.
 * :param seed: Seed for the has function. (Unused)
 * :returns: The hash of the key.
 *
 * :seealso: http://stackoverflow.com/a/13809282
 */
uint32_t hash_djb(const uint8_t *key, size_t size, uint32_t seed);

/**
 * An entry in the hash map with the (key, value) pair given by the user.
 */
struct hash_entry_t {
    void *key;
    void *value;
    uint32_t hash;

    char padding[4];
};

/**
 * A hash map.
 */
struct hash_map_t {
    /** Comparison function for keys. */
    hash_key_compare_t cmp;
    /** Hash function. */
    hash_function_t hash;

    /**
     * Size of type pointed to by the void key pointer. It is used to
     * transform it into a string of characters for the hash.
     */
    size_t key_size;
    /** Number of elements in the hash map. */
    size_t entry_count;
    /** Size of the entry array. */
    size_t bucket_count;
    /** An array of linked lists used to store the entries. Each entry is
     * hashed into a integer in :math:`[0, n]` and added to the
     * resulting linked list.
     */
    linked_list_t **buckets;
};

/**
 * Create a new hash map.
 *
 * :param key_size: Size of the data pointed to by the hash map
 *  entry key. Note that if the keys have different
 *  sizes, they will not be properly hashed.
 * :param cmp: Comparison function for keys. If not provided
 *  ``memcmp`` is used to compare the keys.
 * :param hash: Hash function. (default :c:func:`hash_djb`)
 * :returns: A newly initialized hash map.
 */
struct hash_map_t *
hash_map_new(size_t key_size, hash_key_compare_t cmp, hash_function_t hash);

/**
 * Create a new hash map.
 *
 * :param bucket_count: Number of buckets in the hash map.
 * :param key_size: Size of the data pointed to by the hash map
 *  entry key. Note that if the keys have different
 *  sizes, they will not be properly hashed.
 * :param cmp: Comparison function for keys. If not provided
 *  ``memcmp`` is used to compare the keys.
 * :param hash: Hash function. (default :c:func:`hash_djb`)
 * :returns: A newly initialized hash map.
 */
struct hash_map_t *hash_map_new_ext(
    size_t bucket_count, size_t key_size, hash_key_compare_t cmp, hash_function_t hash
);

/**
 * Destroy a hash map.
 *
 * :param map: Hash map.
 * :param destroy_data: If `true`, the data pointed to by each key
 *  and value will also be freed.
 */
void hash_map_destroy(struct hash_map_t *map, bool destroy_data);

/**
 * Return the number of entries added to the hash map.
 *
 * :param map: Hash map.
 * :returns: Number of entries.
 */
size_t hash_map_size(const struct hash_map_t *map);

/**
 * Add a new entry to the hash map.
 *
 * :param map: Hash map.
 * :param key: Key of the new entry.
 * :param value: Value of the new entry.
 */
void hash_map_set(struct hash_map_t *map, void *key, void *value);

/**
 * Add a new entry to the hash map only if the key does not exist.
 *
 * :param map: Hash map.
 * :param key: Key of the new entry.
 * :param value: Value of the new entry.
 * :returns: `true` if a new entry was created and `false` if an
 *  entry with the given key already exists.
 */
bool hash_map_set_unique(struct hash_map_t *map, void *key, void *value);

/**
 * Retrieve a value from the hash map.
 *
 * :param map: Hash map.
 * :param key: Key
 * :returns: Pointer to the value corresponding to the entry
 *  that has `key` as its key. `NULL` if no such entry exists.
 */
void *hash_map_get(const struct hash_map_t *map, const void *key);

/**
 * Remove an entry from the hash map.
 *
 * :param map: Hash map.
 * :param key: Key we want to remove from the hash map.
 * :param destroy_data: If `true`, they key and value inside the entry
 *  are also freed.
 */
void hash_map_remove(struct hash_map_t *map, const void *key, bool destroy_data);

/**
 * Apply an function to each entry in the hash map.
 *
 * :param map: Hash map.
 * :param apply: Iterator function to apply on each entry.
 * :param args: Extra arguments provided to the `apply`
 *  function.
 * :returns: Returns error code from the last `apply` call.
 */
int hash_map_foreach(const struct hash_map_t *map, hash_iterator_t apply, void *args);

#endif /* !ALGORITHMS_HASH_MAP_H */
