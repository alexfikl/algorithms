// SPDX-FileCopyrightText: 2014-2025 Alexandru Fikl <alexfikl@gmail.com>
// SPDX-License-Identifier: MIT

#include "linked-list-intrusive.h"

static void alg_list_append_ext(alg_list_t *prev, alg_list_t *next, alg_list_t *new) {
    next->prev = new;
    new->next = next;
    new->prev = prev;
    prev->next = new;
}

void alg_list_append(alg_list_t *list, alg_list_t *node) {
    alg_list_append_ext(list, list->next, node);
}

void alg_list_node_init(alg_list_t *node) {
    node->next = node;
    node->prev = node;
}
