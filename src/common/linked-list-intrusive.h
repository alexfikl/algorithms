// SPDX-FileCopyrightText: 2014-2025 Alexandru Fikl <alexfikl@gmail.com>
// SPDX-License-Identifier: MIT

#ifndef ALGORITHMS_LINKED_LIST_INTRUISIVE_H
#define ALGORITHMS_LINKED_LIST_INTRUISIVE_H

#include <algorithms.h>
#include <stddef.h>

/**
 * @brief Intrusive doubly-linked list.
 */
typedef struct alg_list_t alg_list_t;
struct alg_list_t {
    alg_list_t *prev;
    alg_list_t *next;
};

/**
 * @brief Get offset of list member in a given struct.
 */
#define alg_list_node_offset(type, member) offsetof(type, member)

/**
 * @brief Get pointer to struct from location of list member.
 */
#define alg_list_node_container(ptr, type, member)                                     \
    ({                                                                                 \
        const typeof(((type *) 0)->member) *__mptr = (ptr);                            \
        (type *) ((char *) __mptr - offsetof(type, member));                           \
    })

/**
 * @brief Initialize a new linked list.
 */
#define alg_list_init(name) {&(name), &(name)}

/**
 * @brief Iterate over each node of a list.
 */
#define alg_list_foreach(p, head, member)                                              \
    for (p = alg_list_node_container((head)->next, typeof(*p), member);                \
         &(p->member) != (head);                                                       \
         p = alg_list_node_container(p->member.next, typeof(*p), member))

void alg_list_append(alg_list_t *list, alg_list_t *node);

void alg_list_node_init(alg_list_t *node);

#endif /* ALGORITHMS_LINKED_LIST_INTRUISIVE_H */
