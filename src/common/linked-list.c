// SPDX-FileCopyrightText: 2014-2025 Alexandru Fikl <alexfikl@gmail.com>
// SPDX-License-Identifier: MIT

#include "linked-list.h"

void linked_list_destructor_default(void *user_data) {
    alg_free(user_data);
}

linked_list_node_t *linked_list_node_new(void *user_data) {
    linked_list_node_t *node = alg_calloc(1, sizeof(linked_list_node_t));
    node->user_data = user_data;

    return node;
}

linked_list_t *linked_list_new(void) {
    return linked_list_new_ext(linked_list_destructor_default);
}

linked_list_t *linked_list_new_ext(linked_list_destructor_t destructor) {
    linked_list_t *list = alg_calloc(1, sizeof(linked_list_t));
    list->destructor = destructor;

    return list;
}

void linked_list_destroy(linked_list_t *list, bool destroy_data) {
    linked_list_clear(list, destroy_data);
    alg_free(list);
}

void linked_list_clear(linked_list_t *list, bool destroy_data) {
    if (list == NULL) {
        return;
    }

    linked_list_node_t *node;

    node = list->head;
    while (node != NULL) {
        linked_list_remove(list, node, destroy_data);
        node = list->head;
    }

    list->head = list->tail = NULL;
    list->n = 0;
}

size_t linked_list_size(const linked_list_t *list) {
    if (list == NULL) {
        return 0;
    }

    return list->n;
}

void linked_list_insert_node(
    linked_list_t *list, linked_list_node_t *before, linked_list_node_t *node
) {
    if (list == NULL || node == NULL || node->user_data == NULL) {
        return;
    }

    /*
     * cases:
     *  * list is completely empty (i.e. head == NULL)
     *  * we want to prepend to the list (before == NULL)
     *  * we want to append to the list (before == list->tail)
     *  * we want to insert the node after some existing inner node
     */
    node->next = node->prev = NULL;
    if (list->head == NULL) {
        list->head = list->tail = node;
    } else if (before == NULL) {
        node->next = list->head;
        list->head->prev = node;
        list->head = node;
    } else if (before == list->tail) {
        node->prev = before;
        before->next = node;
        list->tail = node;
    } else {
        node->next = before->next;
        node->prev = before;
        before->next->prev = node;
        before->next = node;
    }

    list->n += 1;
}

void linked_list_insert(
    linked_list_t *list, linked_list_node_t *before, void *user_data
) {
    if (list == NULL) {
        return;
    }

    linked_list_insert_node(list, before, linked_list_node_new(user_data));
}

void linked_list_append(linked_list_t *list, void *user_data) {
    if (list == NULL) {
        return;
    }

    linked_list_insert(list, list->tail, user_data);
}

void linked_list_prepend(linked_list_t *list, void *user_data) {
    if (list == NULL) {
        return;
    }

    linked_list_insert(list, NULL, user_data);
}

void linked_list_remove(
    linked_list_t *list, linked_list_node_t *node, bool destroy_data
) {
    if (list == NULL || list->head == NULL || node == NULL) {
        return;
    }

    if (node == list->head) {
        list->head = list->head->next;
    } else if (node == list->tail) {
        list->tail = list->tail->prev;
    } else {
        node->prev->next = node->next;
        node->next->prev = node->prev;
    }

    if (list->head != NULL) {
        list->head->prev = NULL;
    }
    if (list->tail != NULL) {
        list->tail->next = NULL;
    }

    if (node->user_data && destroy_data) {
        list->destructor(node->user_data);
    }
    alg_free(node);
    node = NULL;

    list->n -= 1;
}

linked_list_node_t *
linked_list_find(linked_list_t *list, linked_list_comparison_t cmp, void *user_data) {
    if (list == NULL || cmp == NULL || user_data == NULL) {
        return NULL;
    }

    linked_list_node_t *node;

    node = list->head;
    while (node) {
        if (cmp(node->user_data, user_data) == 0) {
            return node;
        }

        node = node->next;
    }

    return node;
}

void *linked_list_find_data(
    linked_list_t *list, linked_list_comparison_t cmp, void *user_data
) {
    linked_list_node_t *node = linked_list_find(list, cmp, user_data);

    if (node != NULL) {
        return node->user_data;
    }

    return NULL;
}

void linked_list_expand(linked_list_t *list, const linked_list_t *extra) {
    if (list == NULL || extra == NULL) {
        return;
    }

    linked_list_node_t *node = NULL;

    node = extra->head;
    while (node) {
        linked_list_append(list, node->user_data);
        node = node->next;
    }
}

void linked_list_foreach(
    const linked_list_t *list, linked_list_iterator_t apply, void *args
) {
    if (list == NULL) {
        return;
    }

    linked_list_node_t *node = NULL;

    node = list->head;
    while (node) {
        apply(node->user_data, args);
        node = node->next;
    }
}
