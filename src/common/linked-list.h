// SPDX-FileCopyrightText: 2014-2025 Alexandru Fikl <alexfikl@gmail.com>
// SPDX-License-Identifier: MIT

#ifndef ALGORITHMS_LINKED_LIST_H
#define ALGORITHMS_LINKED_LIST_H

#include <algorithms.h>

/**
 * Custom destructor for the data in each node.
 *
 * The function should free the pointer itself as well as any dynamically
 * allocated members.
 *
 * :param user_data: Data contained in a given node.
 */
typedef void (*linked_list_destructor_t)(void *user_data);

/**
 * Function used to iterate over data stored in list nodes.
 *
 * :param user_data: Data contained in a node.
 * :param args: Additional arguments.
 */
typedef void (*linked_list_iterator_t)(void *user_data, void *args);

/**
 * Prototype for function comparing two elements in the list.
 *
 * :param a: Data in a node of the list.
 * :param b: Data in a node of the list.
 * :returns: ``0`` if the two are equal, ``> 0`` if ``a > b`` and ``< 0``
 *  if ``a < b``.
 */
typedef int (*linked_list_comparison_t)(const void *a, const void *b);

/**
 * Doubly-linked list node.
 */
typedef struct linked_list_node_t linked_list_node_t;
struct linked_list_node_t {
    /** User data. */
    void *user_data;

    /** Link to previous node in the list. */
    linked_list_node_t *next;
    /** Link to next node in the list. */
    linked_list_node_t *prev;
};

/**
 * Doubly-linked list.
 */
typedef struct linked_list_t linked_list_t;
struct linked_list_t {
    /** Number of nodes in the list. */
    size_t n;
    /** Destructor for user data. */
    linked_list_destructor_t destructor;

    /** First node in the list. */
    linked_list_node_t *head;
    /** Last node in the list. */
    linked_list_node_t *tail;
};

/**
 * Create a new linked list node that doesn't point to anything.
 *
 * :param user_data: Data inside the node.
 * :returns: Allocated node.
 */
linked_list_node_t *linked_list_node_new(void *user_data);

/**
 * Allocate a new linked list.
 */
linked_list_t *linked_list_new(void);

/**
 * Allocate a new linked list.
 *
 * :param destructor: Destructor for the user data stored in the list.
 */
linked_list_t *linked_list_new_ext(linked_list_destructor_t destructor);

/**
 * Deallocate a linked list.
 *
 * :param list: Linked list.
 * :param free_data: If *true*, also deallocate the data in the nodes.
 */
void linked_list_destroy(linked_list_t *list, bool free_data);

/**
 * Deallocate all the nodes in the list.
 *
 * :param list: Linked list.
 * :param free_data: If *true*, also deallocate the data in the nodes.
 */
void linked_list_clear(linked_list_t *list, bool free_data);

/**
 * Get the number of nodes in the list.
 *
 * :param list: Linked list.
 */
size_t linked_list_size(const linked_list_t *list);

/**
 * Insert a node after a given node in a linked list.
 *
 * :param list: Linked list.
 * :param before: Node before the node that we will insert. If the
 *  node is `NULL`, we prepend the new node to the list.
 * :param node: Existing node. The node is not inserted if it or
 *  its user_data is `NULL`.
 */
void linked_list_insert_node(
    linked_list_t *list, linked_list_node_t *before, linked_list_node_t *node
);

/**
 * Insert data into a new node in a linked list.
 *
 * :param list: Linked list.
 * :param before: Node before the node that we will insert. If the
 *  node is `NULL`, we prepend the new node to the list.
 * :param user_data: Data in newly created node.
 */
void linked_list_insert(
    linked_list_t *list, linked_list_node_t *before, void *user_data
);

/**
 * Add a node at the beginning of the list.
 *
 * :param list: Linked list.
 * :param user_data: Data in the new node.
 */
void linked_list_append(linked_list_t *list, void *user_data);

/**
 * Add a node at the end of the list.
 *
 * :param list: Linked list.
 * :param user_data: Data in the new node.
 */
void linked_list_prepend(linked_list_t *list, void *user_data);

/**
 * Remove a node from the list.
 *
 * :param list: Linked list.
 * :param node: Node to remove. On exit, the node will be `NULL`.
 * :param free_data: If `true`, also deallocated the data in the node.
 */
void linked_list_remove(linked_list_t *list, linked_list_node_t *node, bool free_data);

/**
 * Find the node containing an element in the list.
 *
 * This function can be used to retrieve a node from a linked list for further
 * manipulation. It assumes that the node can be uniquely identified by the
 * given user data, therefor it does not handle duplicate nodes.
 *
 * :param list: Linked list.
 * :param cmp: Comparison function for the user data.
 * :param user_data: User data we are looking for.
 * :returns: The node containing the given user_data or `NULL` if it does not exist.
 */
linked_list_node_t *
linked_list_find(linked_list_t *list, linked_list_comparison_t cmp, void *user_data);

/**
 * Find the data in the list.
 *
 * Unlike linked_list_find(), this function is meant to retrieve the full
 * user data from a partial match provided to the function.
 *
 * :param list: Linked list.
 * :param cmp: Comparison function for the user data.
 * :param user_data: User data we are looking for.
 * :returns: The pointer to the user data from the linked list that matches
 *   the given information.
 */
void *linked_list_find_data(
    linked_list_t *list, linked_list_comparison_t cmp, void *user_data
);

/**
 * Add all the elements of list to another list.
 *
 * :param list: List to which to concatenate.
 * :param extra: List which to concatenate
 */
void linked_list_expand(linked_list_t *list, const linked_list_t *extra);

/**
 * Iterate over all the nodes in the list.
 *
 * :param list: Linked list.
 * :param apply: Function called with the user data in each node.
 * :param args: Extra argument passed to the iterator function.
 */
void linked_list_foreach(
    const linked_list_t *list, linked_list_iterator_t apply, void *args
);

#endif /* !ALGORITHMS_LINKED_LIST_H */
