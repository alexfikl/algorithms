// SPDX-FileCopyrightText: 2014-2025 Alexandru Fikl <alexfikl@gmail.com>
// SPDX-License-Identifier: MIT

#include "priority-queue.h"

struct priority_queue_t *priority_queue_new(linked_list_comparison_t cmp) {
    ALG_CHECK_ABORT(cmp != NULL, "queue must have a comparison function");

    struct priority_queue_t *queue = alg_malloc(sizeof(struct priority_queue_t));

    queue->cmp = cmp;
    queue->list = linked_list_new();

    return queue;
}

void priority_queue_destroy(struct priority_queue_t *queue, bool destroy_data) {
    if (queue == NULL) {
        return;
    }

    linked_list_destroy(queue->list, destroy_data);
    alg_free(queue);
}

size_t priority_queue_size(const struct priority_queue_t *queue) {
    return linked_list_size(queue->list);
}

void priority_queue_insert(struct priority_queue_t *queue, void *user_data) {
    if (queue == NULL) {
        return;
    }

    if (user_data == NULL) {
        ALG_WARN("NULL user_data provided to the queue");
    }

    linked_list_node_t *node = NULL;
    bool node_inserted = false;

    node = queue->list->head;
    while (node) {
        if (queue->cmp(node->user_data, user_data) < 0) {
            linked_list_insert(queue->list, node->prev, user_data);
            node_inserted = true;
            break;
        }

        node = node->next;
    }

    if (!node_inserted) {
        linked_list_insert(queue->list, queue->list->tail, user_data);
    }
}

void *priority_queue_peek(const struct priority_queue_t *queue) {
    if (queue == NULL) {
        return NULL;
    }

    return queue->list->head->user_data;
}

void *priority_queue_pop(struct priority_queue_t *queue) {
    if (queue == NULL) {
        return NULL;
    }

    void *user_data = queue->list->head->user_data;
    linked_list_remove(queue->list, queue->list->head, false);

    return user_data;
}

void priority_queue_foreach(
    struct priority_queue_t *queue, linked_list_iterator_t apply, void *args
) {
    linked_list_foreach(queue->list, apply, args);
}
