// SPDX-FileCopyrightText: 2014-2025 Alexandru Fikl <alexfikl@gmail.com>
// SPDX-License-Identifier: MIT

#ifndef ALGORITHMS_PRIORITY_QUEUE_H
#define ALGORITHMS_PRIORITY_QUEUE_H

#include <linked-list.h>

/**
 * Priority queue.
 */
struct priority_queue_t {
    /** Comparison function. */
    linked_list_comparison_t cmp;
    /** Storage for the queue. */
    linked_list_t *list;
};

/**
 * Create a new priority queue.
 *
 * :param cmp: Comparison function.
 * :returns: A newly allocated priority queue.
 */
struct priority_queue_t *priority_queue_new(linked_list_comparison_t cmp);

/**
 * Deallocate an existing priority queue.
 *
 * :param queue: Queue to destroy.
 * :param destroy_data: If `true`, it also deallocated the data inside each
 *  node of the queue.
 */
void priority_queue_destroy(struct priority_queue_t *queue, bool destroy_data);

/**
 * Return the number of elements in the queue.
 *
 * :param queue: Priority queue.
 * :returns: The number of elements in the queue.
 */
size_t priority_queue_size(const struct priority_queue_t *queue);

/**
 * Insert an element into the queue.
 *
 * The element is inserted into the queue based on the given comparison
 * function, before the first element it is smaller than.
 *
 * :param queue: Priority queue.
 * :param user_data: Data to insert into the queue.
 */
void priority_queue_insert(struct priority_queue_t *queue, void *user_data);

/**
 * Return the data at the top of the priority queue.
 *
 * :param queue: Prioriry queue.
 * :returns: Maximum value in the queue.
 */
void *priority_queue_peek(const struct priority_queue_t *queue);

/**
 * Return and remove the value at the top of the queue.
 *
 * :param queue: Priority queue.
 * :returns: Maximum value in the queue.
 */
void *priority_queue_pop(struct priority_queue_t *queue);

/**
 * Iterate over all the nodes in the queue.
 *
 * :param queue: Priority queue.
 * :param apply: Function called with the user data in each node.
 * :param args: Extra argument passed to the iterator function.
 */
void priority_queue_foreach(
    struct priority_queue_t *queue, linked_list_iterator_t apply, void *args
);

#endif /* !ALGORITHMS_PRIORITY_QUEUE_H */
