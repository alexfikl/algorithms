// SPDX-FileCopyrightText: 2014-2025 Alexandru Fikl <alexfikl@gmail.com>
// SPDX-License-Identifier: MIT

#include "tikz.h"

#define TIKZ_CIRCLE_RADIUS 0.07

static FILE *fd = NULL;

void tikz_preamble(const char *filename) {
    if (filename == NULL) {
        fd = stdout;
    } else {
        fd = fopen(filename, "w");
        ALG_CHECK_ABORT(fd != NULL, "Cannot open file \"%s\"", filename);
    }

    fprintf(fd, "\\documentclass[varwidth=true, border=10pt]{standalone}\n");
    fprintf(fd, "\\usepackage[utf8]{inputenc}\n\n");
    fprintf(fd, "%% Default packages\n");
    fprintf(fd, "\\usepackage{tikz}\n");
    fprintf(fd, "\\usepackage{amsmath}\n");
    fprintf(fd, "\\usepackage{graphicx}\n\n");
}

void tikz_add_package(const char *package, const char *options) {
    ALG_ASSERT(fd != NULL);
    if (package == NULL) {
        return;
    }

    fprintf(fd, "\\usepackage");
    if (options != NULL) {
        fprintf(fd, "[%s]", options);
    }
    fprintf(fd, "{%s}\n", package);
}

void tikz_add_library(const char *library) {
    ALG_ASSERT(fd != NULL);
    if (library == NULL) {
        return;
    }

    fprintf(fd, "\\usetikzlibrary{%s}\n", library);
}

void tikz_add_custom(const char *text) {
    ALG_ASSERT(fd != NULL);
    if (text == NULL) {
        return;
    }

    fprintf(fd, "%s\n", text);
}

void tikz_document_begin(void) {
    ALG_ASSERT(fd != NULL);
    fprintf(fd, "\n\\begin{document}\n");
}

void tikz_document_end(void) {
    ALG_ASSERT(fd != NULL);
    fprintf(fd, "\n\\end{document}\n");

    if (fd != stdout) {
        fclose(fd);
    }
    fd = NULL;
}

void tikz_picture_begin(const char *options) {
    ALG_ASSERT(fd != NULL);
    fprintf(fd, "\n\\begin{figure}\n");
    fprintf(fd, "\\centering\n");
    fprintf(fd, "\\resizebox{\\columnwidth}{!}{");
    fprintf(fd, "\n\\begin{tikzpicture}");
    if (options != NULL) {
        fprintf(fd, "[%s]", options);
    }
    fprintf(fd, "\n");
}

void tikz_picture_end(void) {
    ALG_ASSERT(fd != NULL);
    fprintf(fd, "\\end{tikzpicture}\n");
    fprintf(fd, "}\n");
    fprintf(fd, "\\end{figure}\n");
}

void tikz_add_coordinate(double x, double y, const char *name) {
    ALG_ASSERT(fd != NULL);
    ALG_CHECK_ABORT(name != NULL, "Coordinates must have names");
    fprintf(fd, "\\coordinate (%s) at (%g, %g);\n", name, x, y);
}

void tikz_add_point(const struct point *p, const char *label) {
    tikz_add_point_position(point_x(p), point_y(p), label);
}

void tikz_add_point_position(double x, double y, const char *label) {
    ALG_ASSERT(fd != NULL);
    fprintf(fd, "\\draw[fill] (%g, %g) circle (%g)", x, y, TIKZ_CIRCLE_RADIUS);
    if (label != NULL) {
        fprintf(fd, " node [above] {%s}", label);
    }
    fprintf(fd, ";\n");
}

void tikz_add_point_coordinate(const char *coordinate, const char *label) {
    ALG_ASSERT(fd != NULL);
    fprintf(fd, "\\draw[fill] (%s) circle (%g)", coordinate, TIKZ_CIRCLE_RADIUS);
    if (label != NULL) {
        fprintf(fd, " node [above] {%s}", label);
    }
    fprintf(fd, ";\n");
}

void tikz_add_node_position(double x, double y, const char *label) {
    ALG_ASSERT(fd != NULL);
    fprintf(fd, "\\node at (%g, %g) [above] {%s};\n", x, y, label);
}

void tikz_add_node_coordinate(const char *coordinate, const char *label) {
    ALG_ASSERT(fd != NULL);
    fprintf(fd, "\\node at (%s) [above] {%s};\n", coordinate, label);
}

void tikz_add_line_point(const struct point *p, const struct point *q) {
    tikz_add_line_position(point_x(p), point_y(p), point_x(q), point_y(q));
}

void tikz_add_line_position(double x1, double y1, double x2, double y2) {
    ALG_ASSERT(fd != NULL);
    fprintf(fd, "\\draw[thick] (%g, %g) -- (%g, %g);\n", x1, y1, x2, y2);
}

void tikz_add_line_coordinate(const char *c1, const char *c2) {
    ALG_ASSERT(fd != NULL);
    fprintf(fd, "\\draw[thick] (%s) -- (%s);\n", c1, c2);
}

void tikz_add_rectangle_position(double x1, double y1, double x2, double y2) {
    ALG_ASSERT(fd != NULL);
    fprintf(fd, "\\draw[thick] (%g, %g) rectangle (%g, %g);\n", x1, y1, x2, y2);
}

void tikz_add_rectangle_coordinate(const char *bottom_left, const char *upper_right) {
    ALG_ASSERT(fd != NULL);
    fprintf(fd, "\\draw[thick] (%s) rectangle (%s);\n", bottom_left, upper_right);
}

void tikz_add_rectangle(const struct rectangle *rect) {
    double x1, x2;
    double y1, y2;

    rectangle_coordinates(rect, &x1, &y1, &x2, &y2);
    tikz_add_rectangle_position(x1, y1, x2, y2);
}

void tikz_add_state(
    const char *label, const char *position, const char *text, bool is_initial
) {
    ALG_ASSERT(fd != NULL);
    fprintf(fd, "\\node");
    if (is_initial) {
        fprintf(fd, "[initial, state] ");
    } else {
        fprintf(fd, "[state] ");
    }
    fprintf(fd, "(%s) ", label);

    if (position) {
        fprintf(fd, "[%s] ", position);
    }

    fprintf(fd, "{ %s };\n", text);
}

void tikz_add_state_path(
    const char *from, const char *to, const char *position, const char *label
) {
    ALG_ASSERT(fd != NULL);
    fprintf(fd, "\\path (%s) edge", from);
    if (position == NULL || strlen(position) == 0) {
        fprintf(fd, " ");
    } else {
        fprintf(fd, " [%s] ", position);
    }

    fprintf(fd, "node {%s} (%s);\n", label, to);
}

void tikz_tree_root(const char *label, const char *value) {
    ALG_ASSERT(fd != NULL);
    fprintf(fd, "\\node ");
    if (label != NULL) {
        fprintf(fd, "[%s] ", label);
    }
    fprintf(fd, "{ %s }\n", value);
}

void tikz_tree_child_begin(const char *label, const char *value, size_t depth) {
    ALG_ASSERT(fd != NULL);
    for (size_t i = 0; i < depth; ++i) {
        fprintf(fd, "    ");
    }

    fprintf(fd, "child { node ");
    if (label != NULL) {
        fprintf(fd, "[%s] ", label);
    }
    fprintf(fd, "{ %s }\n", value);
}

void tikz_tree_child_end(size_t depth) {
    ALG_ASSERT(fd != NULL);
    for (size_t i = 0; i < depth; ++i) {
        fprintf(fd, "    ");
    }

    if (depth == 0) {
        fprintf(fd, ";\n");
    } else {
        fprintf(fd, "      }\n");
    }
}
