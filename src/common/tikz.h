// SPDX-FileCopyrightText: 2014-2025 Alexandru Fikl <alexfikl@gmail.com>
// SPDX-License-Identifier: MIT

#ifndef ALGORITHMS_TIKZ_H
#define ALGORITHMS_TIKZ_H

#include <algorithms.h>
#include <geometry.h>

/**
 * Write the preamble for the TikZ / TeX file.
 *
 * This function declares the document class an includes some default packages
 * (like `inputenc`, `amsmath`, `graphicx` and `tikz`).
 *
 * :param filename: Filename of the resulting TeX file. If `NULL`, the
 *  file is written to `stdout`.
 */
void tikz_preamble(const char *filename);

/**
 * Add additional packages to the preamble.
 *
 * Needs to be called after :c:func:`tikz_preamble` and before
 * :c:func:`tikz_document_begin`.
 *
 * :param package: Package name.
 * :param options: Additional package options.
 */
void tikz_add_package(const char *package, const char *options);

/**
 * Add additional TikZ library.
 *
 * Should be called after :c:func:`tikz_preamble` and before
 * :c:func:`tikz_document_begin`.
 *
 * :param library: Library name.
 */
void tikz_add_library(const char *library);

/**
 * Write custom text in the file.
 *
 * This function is provided to fill in holes in the API and should be use
 * sparingly.
 */
void tikz_add_custom(const char *text);

/**
 * Begin the TeX document.
 */
void tikz_document_begin(void);

/**
 * Close the TeX document.
 */
void tikz_document_end(void);

/**
 * Begin a TikZ picture environment.
 *
 * The environment is set inside a centered ``figure`` environment and is
 * resized to fit the page size using ``resizebox``.
 *
 * :param options: Options to pass directly to the ``tikzpicture`` environment.
 */
void tikz_picture_begin(const char *options);

/**
 * Close a TikZ picture environment.
 *
 * Must be called after a given :c:func:`tikz_picture_begin`.
 */
void tikz_picture_end(void);

/**
 * Define a point coordinate.
 *
 * :param x: x coordinate.
 * :param y: y coordinate.
 * :param name: ID by which the point can be referenced.
 */
void tikz_add_coordinate(double x, double y, const char *name);

/**
 * Add a point.
 *
 * :param p: Point.
 * :param label: Point label. If `NULL`, it is not written.
 */
void tikz_add_point(const struct point *p, const char *label);

/**
 * Add a point.
 *
 * The point is drawn at the given coordinates. The potential label is added
 * above the point.
 *
 * :param x: x coordinate.
 * :param y: y coordinate.
 * :param label: Point label. If `NULL`, it is not written.
 */
void tikz_add_point_position(double x, double y, const char *label);

/**
 * Add a point.
 *
 * The point is drawn at a given coordinate ID, thus this function must be
 * called after the coordinate was defined using :c:func:`tikz_add_coordinate`.
 *
 * :param coordinate: Coordinate ID.
 * :param label: Point label. If `NULL`, it is not written.
 */
void tikz_add_point_coordinate(const char *coordinate, const char *label);

/**
 * Add a line segment.
 *
 * :param p: Point of the left boundary.
 * :param q: Point of the right boundary.
 */
void tikz_add_line_point(const struct point *p, const struct point *q);

/**
 * Add a line segment.
 *
 * The line segment is drawn between :math:`(x_1, y_1)` and :math:`(x_2, y_2)`
 * using a `thick` line.
 *
 * :param x1: x coordinate of left boundary.
 * :param y1: y coordinate of left boundary.
 * :param x2: x coordinate of right boundary.
 * :param y2: y coordinate of right boundary.
 */
void tikz_add_line_position(double x1, double y1, double x2, double y2);

/**
 * Add a line segment.
 *
 * Similar to :c:func:`tikz_add_line_position`, but takes two coordinate IDs (which
 * must be declared beforehand with :c:func:`tikz_add_coordinate`).
 *
 * :param c1: Coordinate ID of the left point.
 * :param c2: Coordinate ID of the right point.
 */
void tikz_add_line_coordinate(const char *c1, const char *c2);

/**
 * Add a simple node.
 *
 * Unlike :c:func:`tikz_add_point_position`, this does not draw a point, but
 * just writes the label at the given position.
 *
 * :param x: x coordinate.
 * :param y: y coordinate.
 * :param label: Point label. If `NULL`, it is not written.
 */
void tikz_add_node_position(double x, double y, const char *label);

/**
 * Add a simple node.
 *
 * Unlike :c:func:`tikz_add_point_coordinate`, this does not draw a point, but
 * just writes the label at the given position.
 *
 * :param coordinate: Coordinate ID.
 * :param label: Point label. If `NULL`, it is not written.
 */
void tikz_add_node_coordinate(const char *coordinate, const char *label);

/**
 * Draw a rectangle.
 *
 * :param rect: Rectangle.
 */
void tikz_add_rectangle(const struct rectangle *rect);

/**
 * Draw a rectangle.
 *
 * :param x1: x coordinate of a point.
 * :param y1: y coordinate of a point.
 * :param x2: x coordinate of a point that is diagonally opposite
 *  to the first point.
 * :param y2: y coordinate of a point that is diagonally opposite
 *  to the first point.
 */
void tikz_add_rectangle_position(double x1, double y1, double x2, double y2);

/**
 * Draw a rectangle.
 *
 * :param bottom_left: Coordinate ID of the bottom left point of the rectangle.
 * :param upper_right: Coordinate ID of the top right point of the rectangle.
 */
void tikz_add_rectangle_coordinate(const char *bottom_left, const char *upper_right);

/**
 * Add a new state for an automata.
 *
 * Requires the `automata` library to be added in the preamble.
 *
 * :param label: State label / ID.
 * :param position: Position of the current state with respect to
 *  some other state. (optional)
 * :param text: Text inside the state.
 * :param is_initial: `true` if the current state is the starting state.
 */
void tikz_add_state(
    const char *label, const char *position, const char *text, bool is_initial
);

/**
 * Add a new path between two states.
 *
 * :param from: Start state.
 * :param to: End state.
 * :param position: Position of the path with respect to the two states,
 *  e.g. loop below, bend left, etc. (optional)
 * :param label: Additional label for the path.
 */
void tikz_add_state_path(
    const char *from, const char *to, const char *position, const char *label
);

/**
 * Add tree root.
 *
 * Begins the definition of a tree.
 *
 * :param label: Node label, corresponding to some predefined style.
 * :param value: Value inside the node.
 */
void tikz_tree_root(const char *label, const char *value);

/**
 * Add a tree child.
 *
 * This function defines a new node and opens the brackets for defining
 * the subtree further.
 *
 * :param label: Node label, corresponding to some predefined style.
 * :param value: Value inside the node.
 * :param depth: Depth of the child in the tree.
 */
void tikz_tree_child_begin(const char *label, const char *value, size_t depth);

/**
 * Finish adding a child (and its subtree)
 *
 * The function closes the curly brackets for the subtree.
 */
void tikz_tree_child_end(size_t depth);

#endif /* !ALGORITHMS_TIKZ_H */
