// SPDX-FileCopyrightText: 2014-2025 Alexandru Fikl <alexfikl@gmail.com>
// SPDX-License-Identifier: MIT

#include "convex-hull.h"

#include <tikz.h>

/**
 * @brief Remove the second to last node from the list.
 *
 * @param[in] hull          List of points in a convex hull.
 */
static void convex_hull_remove_middle(linked_list_t *hull) {
    ALG_ASSERT(linked_list_size(hull) > 2);
    linked_list_remove(hull, hull->tail->prev, false);
}

/**
 * @brief Check if the last 3 points in the list make a left turn.
 *
 * @param[in] hull          Current convex hull.
 * @return                  1 if the the points make a left turn or are
 *                          collinear, 0 otherwise.
 */
static int convex_hull_is_left_turn(linked_list_t *hull) {
    ALG_ASSERT(linked_list_size(hull) > 2);
    struct point *p1 = linked_list_point(hull->tail->prev->prev);
    struct point *p2 = linked_list_point(hull->tail->prev);
    struct point *p3 = linked_list_point(hull->tail);

    if (geometry_collinear(p1, p2, p3)) {
        return 1;
    }

    if (!geometry_counterclockwise(p1, p2, p3)) {
        return 0;
    }

    return 1;
}

linked_list_t *convex_hull_graham(struct point **points, size_t n) {
    linked_list_t *upper = linked_list_new();
    linked_list_t *lower = linked_list_new();
    linked_list_node_t *node = NULL;
    struct point *point = NULL;

    qsort(points, n, sizeof(struct point *), geometry_point_comparison_wrapper);

    /* put the two left-most points in the upper hull */
    linked_list_append(upper, points[0]);
    linked_list_append(upper, points[1]);

    /* construct the upper hull */
    for (size_t i = 2; i < n; ++i) {
        linked_list_append(upper, points[i]);

        /* iterate until we have no more left turns */
        while (linked_list_size(upper) > 2 && convex_hull_is_left_turn(upper)) {
            convex_hull_remove_middle(upper);
        }
    }

    /* put the two right-most points in the lower hull */
    linked_list_append(lower, points[n - 1]);
    linked_list_append(lower, points[n - 2]);

    /* construct the lower hull */
    for (size_t i = n - 3; i < n; --i) {
        linked_list_append(lower, points[i]);

        /* iterate until we have no more right turns */
        while (linked_list_size(lower) > 2 && convex_hull_is_left_turn(lower)) {
            convex_hull_remove_middle(lower);
        }
    }

    /* append all points in the lower hull to the upper hull */
    /* skip the head and tail to avoid duplicates */
    node = lower->head->next;
    while (node->next) {
        point = linked_list_point(node);
        linked_list_append(upper, point);

        node = node->next;
    }

    linked_list_destroy(lower, false);
    return upper;
}

/**
 * @brief Get the point from the given hull that maximizes the angle.
 *
 * @param[in] hull          Given set of points forming a hull.
 * @param[in] p0            Point.
 * @param[in] p1            Point.
 * @return                  The point q that maximizes the angle defined by
 *                          (p0, p1, q).
 */
static struct point *convex_hull_max_angle(
    const linked_list_t *hull, const struct point *p0, const struct point *p1
) {
    struct point *max_point = NULL;
    double max_angle = 0.0;

    linked_list_node_t *node = NULL;
    struct point *p = NULL;
    double angle = 0;

    node = hull->head;
    while (node) {
        p = linked_list_point(node);
        if (point_equal(p, p1)) {
            node = node->next;
            continue;
        }

        angle = geometry_angle(p0, p1, p);
        if (angle > max_angle) {
            max_point = p;
            max_angle = angle;
        }

        node = node->next;
    }

    return max_point;
}

linked_list_t *convex_hull_chan(struct point **points, size_t n) {
    linked_list_t *hull;
    linked_list_t **subhull;

    struct point *p0 = NULL;
    struct point *p1 = NULL;
    struct point *q0 = NULL;
    struct point *q1 = NULL;
    struct point *q = NULL;
    struct point *point = NULL;

    size_t t = 1;
    size_t m = 0;
    size_t buckets = 0;
    double angle;
    double max_angle;

    /* find the left-most and lowest points in the list */
    p0 = point_new(0.0, 1000000.0);
    p1 = points[0];
    for (size_t i = 1; i < n; ++i) {
        if (geometry_point_comparison(points[i], p1) < 0) {
            p1 = points[i];
        }

        if (point_y(points[i]) < point_y(p0)) {
            point_set_y(p0, point_y(points[i]));
        }
    }

    hull = linked_list_new();
    while (t < n) {
        /* initialize the list */
        linked_list_clear(hull, false);
        linked_list_append(hull, p0);
        linked_list_append(hull, p1);

        /* compute the size and number of buckets */
        m = (size_t) ALG_MIN(pow(2, pow(2, t)), n);
        buckets = (size_t) lround(ceil((double) n / (double) m));

        /* use Graham's Scan on each bucket to get a convex hull */
        subhull = alg_malloc(buckets * sizeof(linked_list_t *));
        for (size_t i = 0; i < buckets; ++i) {
            subhull[i] = convex_hull_graham(points + i * m, ALG_MIN(m, n - i * m));
        }

        for (size_t k = 0; k < m; ++k) {
            /* get two last points */
            q0 = linked_list_point(hull->tail->prev);
            q1 = linked_list_point(hull->tail);

            point = NULL;
            max_angle = 0.0;
            for (size_t i = 0; i < buckets; ++i) {
                /* find q in bucket i that maximizes the angle (q0, q1, q) */
                q = convex_hull_max_angle(subhull[i], q0, q1);

                /* update point that maximizes the angle for all buckets */
                angle = geometry_angle(q0, q1, q);
                if (point == NULL || angle > max_angle) {
                    point = q;
                    max_angle = angle;
                }
            }

            /* convex hull is closed */
            if (point == p1) {
                t = n;
                break;
            }

            linked_list_append(hull, point);
        }

        /* buckets are no longer needed */
        for (size_t i = 0; i < buckets; ++i) {
            linked_list_destroy(subhull[i], false);
        }
        alg_free(subhull);

        t += 1;
    }

    /* remove p0 from the hull */
    linked_list_remove(hull, hull->head, false);
    alg_free(p0);

    return hull;
}

void convex_hull_tikz(
    const struct point **points,
    size_t n,
    const linked_list_t *hull,
    const char *filename,
    bool with_labels
) {
    if (points == NULL || hull == NULL) {
        return;
    }

    char label[BUFSIZ];
    linked_list_node_t *node = NULL;
    struct point *p1 = NULL;
    struct point *p2 = NULL;

    tikz_preamble(filename);
    tikz_document_begin();
    tikz_picture_begin(NULL);

    /* write all points */
    for (size_t i = 0; i < n; ++i) {
        if (with_labels) {
            snprintf(label, BUFSIZ, "$%lu$", i);
            tikz_add_point(points[i], label);
        } else {
            tikz_add_point(points[i], NULL);
        }
    }

    /* write the edges forming the hull */
    node = hull->head;
    while (node->next) {
        p1 = linked_list_point(node);
        p2 = linked_list_point(node->next);
        tikz_add_line_point(p1, p2);

        node = node->next;
    }

    /* tie the tail to the head */
    p1 = linked_list_point(hull->tail);
    p2 = linked_list_point(hull->head);
    tikz_add_line_point(p1, p2);

    tikz_picture_end();
    tikz_document_end();
}
