// SPDX-FileCopyrightText: 2014-2025 Alexandru Fikl <alexfikl@gmail.com>
// SPDX-License-Identifier: MIT

#ifndef ALGORITHMS_CONVEX_HULL_H
#define ALGORITHMS_CONVEX_HULL_H

#include <algorithms.h>
#include <geometry.h>

/**
 * Construct the convex hull for a given set of points.
 *
 * This function uses a modified version of Graham's Scan, called Andrew's
 * Monotone Chain algorithm. The algorithm constructs the the hull by splitting
 * it into a lower and upper part. The main advantage of the algorithm is that
 * it removes the need to compute polar angles like Graham's Scan. However it
 * still maintains it's @f$ \mathcal{O}(n \log n) @f$ complexity.
 *
 * The algorithm has 3 main steps:
 *
 * * sort the array of points in lexicographical order (first x, than y).
 * * start the upper hull with the first two elements of the sorted array (i.e.
 *   the left-most points). Add the other points one by one and check that the
 *   last 3 points in the upper hull don't make a left turn.
 * * start the upper hall with the last two elements of the sorted array
 *   (i.e. the right-most points). Add the other points in reverse order and
 *   check that the last 3 points don't make a left turn.
 *
 * Once these steps are finished, we only have to concatenate the upper and
 * the lower hulls to get the complete hull. The hull is made out of a list
 * of points in clockwise order starting from the left-most point.
 *
 * :param points: List of points around which we want to compute
 *  the hull. Note that the points are sorted in place.
 * :param n: Number of points.
 * :returns: Convex hull around the set of points with vertices in clockwise order.
 *
 * :seealso:
 *  * A. M. Andrew, *Another Efficient ALgorithm for Convex Hulls in Two
 *    Dimensions*, Information Processing Letters, 1979,
 *    http://www.sciencedirect.com/science/article/pii/0020019079900723
 */
linked_list_t *convex_hull_graham(struct point **points, size_t n);

/**
 * Construct the convex hull using Chan's Algorithm.
 *
 * TODO
 *
 * :param points: List of points around which we want to compute
 *  the hull. Note that the points are sorted in place.
 * :param n: Number of points.
 * :returns: Convex hull around the set of points with vertices in clockwise order.
 *
 * :seealso:
 *  T. M. Chan, *Optimal Output-Sensitive Convex Hull Algorithms in Two and
 *  Three Dimensions*, Discrete Computational Geometry, 1996,
 *  https://www.cs.ucsb.edu/~suri/cs235/ChanCH.pdf
 */
linked_list_t *convex_hull_chan(struct point **points, size_t n);

/**
 * Write the points and the convex hull to a file.
 *
 * The function write a valid TeX file containing a TikZ picture of the points
 * and the convex hull. This can then be compiled into a PDF with ``pdflatex``
 * or any other program.
 *
 * :param points: List of points.
 * :param n: Number of points.
 * :param hull: Convex hull around the points.
 * :param filename: Output file name.
 * :param with_labels: Boolean to indicate that each point should
 *  have a label. If true, the position of the point is used as a label.
 *
 * :seealso: tikz.h
 */
void convex_hull_tikz(
    const struct point **points,
    size_t n,
    const linked_list_t *hull,
    const char *filename,
    bool with_labels
);

#endif /* !ALGORITHMS_CONVEX_HULL_H */
