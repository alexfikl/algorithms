// SPDX-FileCopyrightText: 2014-2025 Alexandru Fikl <alexfikl@gmail.com>
// SPDX-License-Identifier: MIT

#include "kdtree.h"

#include <tikz.h>

static kd_node_t *kd_node_new(double *x, void *data) {
    kd_node_t *node = alg_malloc(sizeof(kd_node_t));

    node->x = x;
    node->data = data;
    node->left = node->right = NULL;

    return node;
}

static void kd_node_destroy(kd_node_t *node, kd_destructor_t destructor) {
    if (node == NULL) {
        return;
    }

    if (destructor) {
        destructor(node->data);
    }
    alg_free(node->x);
    alg_free(node);
}

static void kd_node_string(const kd_node_t *node, uint8_t dim, char buffer[BUFSIZ]) {
    if (node == NULL) {
        snprintf(buffer, BUFSIZ, "nil");
        return;
    }

    switch (dim) {
    case 1:
        snprintf(buffer, BUFSIZ, "%.4lf", node->x[0]);
        break;
    case 2:
        snprintf(buffer, BUFSIZ, "(%.4lf, %.4lf)", node->x[0], node->x[1]);
        break;
    case 3:
        snprintf(
            buffer, BUFSIZ, "(%.4lf, %.4lf, %.4lf)", node->x[0], node->x[1], node->x[2]
        );
        break;
    default:
        ALG_ABORT("Dimension > 3 not supported.");
    }
}

static kd_node_t *kd_node_min(kd_node_t *left, kd_node_t *right, uint8_t d) {
    return (left->x[d] < right->x[d] ? left : right);
}

static void kd_subtree_destroy(kd_node_t *parent, kd_destructor_t destructor) {
    if (parent == NULL) {
        return;
    }

    kd_subtree_destroy(parent->left, destructor);
    kd_subtree_destroy(parent->right, destructor);

    kd_node_destroy(parent, destructor);
}

static void
kd_subtree_insert(kd_node_t **parent, uint8_t dim, uint8_t cd, double *x, void *data) {
    if (*parent == NULL) {
        *parent = kd_node_new(x, data);
    } else {
        uint8_t next = (cd + 1) % dim;

        if (x[cd] < (*parent)->x[cd]) {
            kd_subtree_insert(&((*parent)->left), dim, next, x, data);
        } else {
            kd_subtree_insert(&((*parent)->right), dim, next, x, data);
        }
    }
}

static kd_node_t *kd_subtree_findmin(kd_node_t *parent, uint8_t dim, uint8_t cd) {
    if (parent == NULL) {
        return NULL;
    }

    uint8_t next = (cd + 1) % dim;

    if (cd == dim) {
        if (parent->left == NULL) {
            return parent;
        } else {
            return kd_subtree_findmin(parent->left, dim, next);
        }
    } else {
        kd_node_t *left = kd_subtree_findmin(parent->left, dim, next);
        kd_node_t *right = kd_subtree_findmin(parent->right, dim, next);

        return kd_node_min(parent, kd_node_min(left, right, cd), cd);
    }
}

static kd_node_t *kd_subtree_find(
    kd_node_t *parent, double *x, double distance, uint8_t dim, uint8_t cd
) {
    ALG_UNUSED(parent);
    ALG_UNUSED(x);
    ALG_UNUSED(distance);
    ALG_UNUSED(dim);
    ALG_UNUSED(cd);

    return NULL;
}

static void kd_subtree_tikz(
    const kd_node_t *parent, char buffer[BUFSIZ], uint8_t dim, size_t depth
) {
    if (depth == 0) {
        kd_node_string(parent, dim, buffer);
        tikz_tree_root(NULL, buffer);
    } else {
        kd_node_string(parent, dim, buffer);
        tikz_tree_child_begin(NULL, buffer, depth);
    }

    if (parent) {
        kd_subtree_tikz(parent->left, buffer, dim, depth + 1);
        kd_subtree_tikz(parent->right, buffer, dim, depth + 1);
    }

    tikz_tree_child_end(depth);
}

kd_tree_t *kd_tree_new(uint8_t dim) {
    if (dim == 0) {
        return NULL;
    }

    kd_tree_t *tree = alg_malloc(sizeof(kd_tree_t));

    tree->dim = dim;
    tree->xmin = alg_malloc(dim * sizeof(double));
    tree->xmax = alg_malloc(dim * sizeof(double));
    tree->npoints = 0;
    tree->root = NULL;

    for (uint8_t i = 0; i < dim; ++i) {
        tree->xmin[i] = DBL_MAX;
        tree->xmax[i] = DBL_MIN;
    }

    return tree;
}

void kd_tree_destroy(kd_tree_t *tree, kd_destructor_t destructor) {
    if (tree == NULL) {
        return;
    }

    kd_subtree_destroy(tree->root, destructor);
    alg_free(tree->xmin);
    alg_free(tree->xmax);
    alg_free(tree);
}

void kd_tree_insert(kd_tree_t *tree, double *x, void *data) {
    if (tree == NULL || x == NULL) {
        return;
    }

    /* insert node (recursive) */
    kd_subtree_insert(&(tree->root), tree->dim, 0, x, data);

    /* update tree info */
    tree->npoints += 1;
    for (uint8_t i = 0; i < tree->dim; ++i) {
        if (x[i] < tree->xmin[i]) {
            tree->xmin[i] = x[i];
        }

        if (x[i] > tree->xmax[i]) {
            tree->xmax[i] = x[i];
        }
    }
}

void kd_tree_median(
    const double **x, size_t n, uint8_t dim, uint8_t cd, double *median
) {
    if (x == NULL || median == NULL) {
        return;
    }

    ALG_UNUSED(n);
    ALG_UNUSED(dim);
    ALG_UNUSED(cd);
    ALG_ERROR("NotImplementedError");
}

kd_node_t *kd_tree_findmin(const kd_tree_t *tree, uint8_t cd) {
    if (tree == NULL) {
        return NULL;
    }

    if (cd >= tree->dim) {
        return NULL;
    }

    return kd_subtree_findmin(tree->root, cd, 0);
}

kd_node_t *kd_tree_find(const kd_tree_t *tree, double *x) {
    if (tree == NULL || x == NULL) {
        return NULL;
    }

    return kd_subtree_find(tree->root, x, DBL_MAX, tree->dim, 0);
}

linked_list_t *kd_tree_range(const kd_tree_t *tree, double *rmin, double *rmax) {
    if (tree == NULL || rmin == NULL || rmax == NULL) {
        return NULL;
    }

    linked_list_t *points = linked_list_new();

    return points;
}

void kd_tree_tikz(const kd_tree_t *tree, const char *filename) {
    if (tree == NULL) {
        return;
    }

    char buffer[BUFSIZ];

    tikz_preamble(filename);
    tikz_add_library("arrows");
    tikz_document_begin();
    tikz_picture_begin(
        "->,every node/.style={draw,rectangle},level/.style={sibling distance=3cm}"
    );

    kd_subtree_tikz(tree->root, buffer, tree->dim, 0);

    tikz_picture_end();
    tikz_document_end();
}
