// SPDX-FileCopyrightText: 2014-2025 Alexandru Fikl <alexfikl@gmail.com>
// SPDX-License-Identifier: MIT

#ifndef ALGORITHMS_KD_TREE_H
#define ALGORITHMS_KD_TREE_H

#include <algorithms.h>
#include <linked-list.h>

/**
 * Custom destructor for user data.
 */
typedef void (*kd_destructor_t)(void *user_data);

typedef struct kd_node_t kd_node_t;
typedef struct kd_tree_t kd_tree_t;

/**
 * Note in the tree.
 */
struct kd_node_t {
    /** Coordinates of the node. */
    double *x;
    /** Additional data attached to the node. */
    void *data;

    /** Left child. */
    kd_node_t *left;
    /** Right child. */
    kd_node_t *right;
};

/**
 * Implements a k-d tree, a space partitioning data structure.
 */
struct kd_tree_t {
    /** Dimension of the space. */
    uint8_t dim;
    /**< Coordinates of the point closest to the origin. */
    double *xmin;
    /** Coordinates of the point furthest away from the origin. */
    double *xmax;

    /** Number of points / nodes in tree. */
    size_t npoints;
    /** Root node of the tree. */
    kd_node_t *root;
};

/**
 * Create a new k-d tree.
 *
 * :param dim: Dimension of the space.
 * :returns: An allocated k-d tree.
 */
kd_tree_t *kd_tree_new(uint8_t dim);

/**
 * Deallocate a k-d tree.
 */
void kd_tree_destroy(kd_tree_t *tree, kd_destructor_t destructor);

/**
 * Insert an element into the tree.
 *
 * :param tree: Tree.
 * :param x: Coordinates of the new point. The coordinates need
 *  to be dynamically allocated and the ownership is
 *  transferred to the tree (i.e. deallocated by the tree).
 * :param data: Additional data attached to the new point.
 */
void kd_tree_insert(kd_tree_t *tree, double *x, void *data);

/**
 * Find the median from a set of points.
 *
 * Uses the median of medians algorithm to find the median in
 * :math:`\mathcal{O}(n)` time.
 *
 * :param x: Set of points.
 * :param n: Number of points.
 * :param dim: Dimension.
 * :param cd: Dimension along which to find the median.
 * :param median: Median point.
 */
void kd_tree_median(
    const double **x, size_t n, uint8_t dim, uint8_t cd, double *median
);

/**
 * Find minimum node along a given dimension.
 *
 * :param tree: Tree.
 * :param cd: Desired dimension.
 * :returns: The node with the minimum value along the given dimension.
 */
kd_node_t *kd_tree_findmin(const kd_tree_t *tree, uint8_t cd);

/**
 * Find the nearest point in the tree to a given point.
 *
 * :param tree: Tree.
 * :param x: Desired point coordinates.
 * :returns: Node nearest to the given point.
 */
kd_node_t *kd_tree_find(const kd_tree_t *tree, double *x);

/**
 * Find all points in a given range.
 *
 * :param tree: Tree.
 * :param rmin: Point of the range closest to the origin.
 * :param rmax: Point of the range furthest away from the origin.
 * :returns: List of points in the given range, i.e. all points
 *  that have ``rmin[i] <= x[i] <= rmax[i]``.
 */
linked_list_t *kd_tree_range(const kd_tree_t *tree, double *rmin, double *rmax);

/**
 * Dump the k-d tree to a TeX file based on TikZ.
 *
 * :param tree: Tree.
 * :param filename: Name of the file.
 */
void kd_tree_tikz(const kd_tree_t *tree, const char *filename);

#endif /* ALGORITHMS_KD_TREE_H */
