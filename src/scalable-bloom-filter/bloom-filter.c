// SPDX-FileCopyrightText: 2014-2025 Alexandru Fikl <alexfikl@gmail.com>
// SPDX-License-Identifier: MIT

#include <limits.h>

#include "bloom-filter.h"

/**< Constant for log(2)^2 where log is the natural logarithm */
#define LOG2SQR 0.4804530139182014

/**< Set the n-th bit of the string f */
#define FILTER_SET_BIT(f, n)                                                           \
    do {                                                                               \
        f[n / CHAR_BIT] |= (1 << (n % CHAR_BIT));                                      \
    } while (0);

/**< Get the n-th bit of the string f */
#define FILTER_GET_BIT(f, n)                                                           \
    ((f[n / CHAR_BIT] & (1 << (n % CHAR_BIT))) >> (n % CHAR_BIT))

/**
 * @brief Compute the number of slices / hash functions for a given probability.
 *
 * The number of slices / hash functions is given by:
 * @f[
 *      k = - \frac{\log p}{\log 2}
 * @f]
 * where @c log is the natural logarithm.
 *
 * \param[in] p         Desired failure probability.
 * \return              The minimum number of hash functions necessary.
 */
static size_t bloom_filter_slices(double p) {
    return (size_t) lround(ceil(-log(p) / log(2.0)));
}

/**
 * \brief Compute the size in bits of each slice.
 *
 * The formula is:
 * @f[
 *      m = -n \frac{\log p}{k log^2 2}
 * @f]
 * where @c log is the natural logarithm.
 *
 * \param[in] n         Expected number of items to insert into the filter.
 * \param[in] p         Required failure probability.
 * \param[in] k         Number of slices.
 * \return              The size, in bytes, of each slices.
 */
static size_t bloom_filter_slice_size(size_t n, double p, size_t k) {
    return (size_t) lround(ceil(n * (-log(p) / (k * CHAR_BIT * LOG2SQR)))) * CHAR_BIT;
}

/**
 * \brief Compute the maximum number of elements we can add to the filter.
 *
 * The formula is:
 * @f[
 *      n = -m \frac{ k \log^2 2}{log p}
 * @f]
 * where @c log is the natural logarithm.
 *
 * \param[in] m         Size of the bitfield in each slice.
 * \param[in] p         Required failure probability.
 * \param[in] k         Number of slices.
 * \return              The maximum number of elements that can be added.
 */
static size_t bloom_filter_maximum_size(size_t m, double p, size_t k) {
    return (size_t) lround(ceil(m * (k * LOG2SQR) / (-log(p))));
}

struct bloom_filter_t *bloom_filter_new(size_t n, double p, hash_function_t hash) {
    if (p < 0 || p > 1) {
        ALG_ERROR("Probability has to be in [0, 1]: p = %g", p);
        return NULL;
    }

    struct bloom_filter_t *bf = alg_calloc(1, sizeof(struct bloom_filter_t));

    bf->p = p;
    bf->n = n;
    bf->k = bloom_filter_slices(p);
    bf->m = bloom_filter_slice_size(bf->n, bf->p, bf->k);
    bf->added = 0;
    bf->hash = (hash == NULL ? hash_murmur : hash);

    bf->slice = alg_malloc(bf->k * sizeof(struct bloom_filter_slice_t));
    for (size_t i = 0; i < bf->k; ++i) {
        bf->slice[i].m = bf->m;
        bf->slice[i].filter = alg_calloc(bf->m / CHAR_BIT, sizeof(char));
    }

    return bf;
}

struct bloom_filter_t *bloom_filter_new_ext(size_t m, double p, hash_function_t hash) {
    if (p < 0 || p > 1) {
        ALG_ERROR("Probability has to be in [0, 1]: p = %g", p);
        return NULL;
    }

    struct bloom_filter_t *bf = alg_calloc(1, sizeof(struct bloom_filter_t));

    bf->p = p;
    bf->m = (size_t) lround(ceil((double) m / CHAR_BIT)) * CHAR_BIT;
    bf->k = bloom_filter_slices(p);
    bf->n = bloom_filter_maximum_size(bf->m, bf->p, bf->k);
    bf->added = 0;
    bf->hash = (hash == NULL ? hash_murmur : hash);

    bf->slice = alg_malloc(bf->k * sizeof(struct bloom_filter_slice_t));
    for (size_t i = 0; i < bf->k; ++i) {
        bf->slice[i].m = bf->m;
        bf->slice[i].filter = alg_calloc(bf->m / CHAR_BIT, sizeof(char));
    }

    return bf;
}

void bloom_filter_destroy(struct bloom_filter_t *bf) {
    for (size_t i = 0; i < bf->k; ++i) {
        alg_free(bf->slice[i].filter);
    }

    alg_free(bf->slice);
    alg_free(bf);
}

void bloom_filter_reset(struct bloom_filter_t *bf) {
    bf->added = 0;
    for (size_t i = 0; i < bf->k; ++i) {
        memset(bf->slice[i].filter, 0, bf->slice[i].m * sizeof(char));
    }
}

void bloom_filter_dump(const struct bloom_filter_t *bf) {
    ALG_INFO("Probability:    %g", bf->p);
    ALG_INFO("Hash Functions: %lu", bf->k);
    ALG_INFO("Bitfield size:  %lu", bf->m);
    ALG_INFO("Max elements:   %lu", bf->n);

    for (size_t i = 0; i < bf->k; ++i) {
        printf("[INFO] bloom_filter_dump: [%lu] ", i);
        for (size_t j = 0; j < bf->m; ++j) {
            printf("%d ", FILTER_GET_BIT(bf->slice[i].filter, j));
        }
        printf("\n");
    }
}

void bloom_filter_add(struct bloom_filter_t *bf, const void *key, size_t size) {
    uint32_t hash = 0;
    uint32_t hash1 = bf->hash(key, size, 0);
    uint32_t hash2 = bf->hash(key, size, 0x34ebb8e);

    for (size_t i = 0; i < bf->k; ++i) {
        hash = (uint32_t) ((hash1 + i * hash2) % bf->slice[i].m);
        FILTER_SET_BIT(bf->slice[i].filter, hash);
    }

    ++bf->added;
}

int bloom_filter_contains(
    const struct bloom_filter_t *bf, const void *key, size_t size
) {
    uint32_t hash = 0;
    uint32_t hash1 = bf->hash(key, size, 0);
    uint32_t hash2 = bf->hash(key, size, 0x34ebb8e);

    for (size_t i = 0; i < bf->k; ++i) {
        hash = (uint32_t) ((hash1 + i * hash2) % bf->slice[i].m);
        if (FILTER_GET_BIT(bf->slice[i].filter, hash) == 0) {
            return 0;
        }
    }

    return 1;
}

size_t bloom_filter_size(const struct bloom_filter_t *bf) {
    return bf->added;
}

int bloom_filter_is_full(const struct bloom_filter_t *bf) {
    return (bf->added > bf->n);
}
