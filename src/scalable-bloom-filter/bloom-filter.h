// SPDX-FileCopyrightText: 2014-2025 Alexandru Fikl <alexfikl@gmail.com>
// SPDX-License-Identifier: MIT

#ifndef ALGORITHMS_BLOOM_FILTER_H
#define ALGORITHMS_BLOOM_FILTER_H

#include <algorithms.h>
#include <hash-map.h>

#ifndef BLOOM_FILTER_PROBABILITY
#define BLOOM_FILTER_PROBABILITY 0.001
#endif

/**
 * A slice of the bloom filter.
 *
 * The bloom filter has k hash functions, to have a more robust implementation,
 * each hash function operates on its own slice. This avoids bad cases such as:
 * :math:`h_1(x) = \cdots = h_k(x) = i`.
 */
struct bloom_filter_slice_t {
    /** Size, in bits, of the slice. */
    size_t m;
    /** Bit field in this slice. */
    uint8_t *filter;
};

/**
 * A simple bloom filter structure.
 *
 * A bloom filter is a probabilistic data structure. It uses bit fields and
 * hash functions to provide a way to check whether a given value is part of
 * a given set.
 *
 * Because of its probabilistic nature, it is possible to get a false positive
 * (i.e. an element isn't in the set, but the bloom filter thinks it is), but
 * it is not possible to get false negatives (i.e. an element that is in the
 * set but the bloom filter thinks it is not).
 *
 * To achieve a good probability of false positives, the bitfield has to be of
 * a considerate size. Furthermore, our bloom filter uses multiple hashes and
 * multiple bitfields to provide an even smaller probability.
 */
struct bloom_filter_t {
    /** Failure probability. */
    double p;
    /** Size, in bits, of each filter. */
    size_t m;
    /** Maximum number of items to add. */
    size_t n;
    /** Number of items already added. */
    size_t added;

    /** Hash function. */
    hash_function_t hash;

    /** Number of slices / hashes. */
    size_t k;
    /** Array of slices. */
    struct bloom_filter_slice_t *slice;
};

/**
 * Create a new bloom filter.
 *
 * Given the maximum expected number of items :math:`n` we will add to the bloom
 * filter and the desired failure probability :math:`p`, we can compute :math:`k`,
 * the number of hash functions required, and :math:`m`, the size of the bitfield,
 * to achieve the desired probability.
 *
 * The number of hash functions is given by (see [bloom-wiki]_):
 *
 * .. math::
 *
 *      k = - \frac{\log p}{\log 2}
 *
 * and the size in bits of each slice is:
 *
 * .. math::
 *
 *      m = -n \frac{\log p}{k \log^2 2}
 *
 * giving a total size for the bloom filter of :math:`m \times k`. Note that
 * the bit size :math:`m` is also made into a multiple of 8, so that it can be
 * stored into a regular ``uint8_t`` array.
 *
 * Even though we need :math:`k` independent hash function, according to [bloom-hash]_,
 * we can construct these :math:`k` functions from only 2 by using:
 *
 * .. math::
 *
 *      g_i(x) = h_1(x) + i h_2(x), \forall i \in [1, k].
 *
 * This is shown to not increase the failure probability of the bloom filter
 * and is a much simpler way to construct it. In our case we have chosen to
 * use hash functions that use a seed, thus the difference between
 * :math:`h_1` and :math:`h_2` is a different seed.
 *
 * :param n: Maximum expected number of items to add to the filter.
 * :param p: Desired failure probability.
 * :param hash: Hash function of type :c:func:`has_function_t`. If this is
 *  *NULL*, the ``Murmur2`` hash is used.
 * :returns: A fully initialized bloom filter.
 *
 *  .. [bloom-wiki] https://en.wikipedia.org/wiki/Bloom_filter
 *  .. [bloom-hash] http://www.eecs.harvard.edu/~michaelm/postscripts/rsa2008.pdf
 */
struct bloom_filter_t *bloom_filter_new(size_t n, double p, hash_function_t hash);

/**
 * Create a new bloom filter.
 *
 * A new bloom filter is create with the number of slices given by:
 *
 * .. math::
 *
 *      k = - \frac{\log p}{\log 2},
 *
 * where each slice has a bitfield with :math:`m` bits. This gives a maximum number
 * of elements we can add of:
 *
 * .. math::
 *
 *      n = -m \frac{k \log^2 2}{\log p}.
 *
 * Note that, in the case the bit field size *m* is not a multiple of 8, the
 * closest multiple of 8 bigger than *m* is chosen instead. This is done
 * so that we can store the bitfield into a full ``uint8_t`` array.
 *
 * :param m: Size, in bits, of each bitfield.
 * :param p: Desired false positive probability.
 * :param hash: Hash function. If *NULL*, the default ``Murmur2`` hash is used.
 */
struct bloom_filter_t *bloom_filter_new_ext(size_t m, double p, hash_function_t hash);

/**
 * Free a bloom filter struct.
 *
 * :param bf: Bloom filter to free.
 */
void bloom_filter_destroy(struct bloom_filter_t *bf);

/**
 * Reset all the bitfields in all the slices to 0.
 *
 * :param bf: Bloom filter for which to reset the bitfields.
 */
void bloom_filter_reset(struct bloom_filter_t *bf);

/**
 * Print information about the current state of the bloom filter.
 *
 * This prints :math:`(p, k, m, n)` and the individual bits of each slice.
 */
void bloom_filter_dump(const struct bloom_filter_t *bf);

/**
 * Add an element to the bloom filter.
 *
 * :param bf: Bloom filter.
 * :param key: Element to add to the filter.
 * :param size: Size, in bytes, of the element.
 */
void bloom_filter_add(struct bloom_filter_t *bf, const void *key, size_t size);

/**
 * Check if an element is in the bloom filter.
 *
 * This function can give false positives with a probability @b p.
 *
 * :param bf: Bloom filter.
 * :param key: Element to check for in the filter.
 * :param size: Size in bytes of the element.
 * :returns: 0 if the element is not in the filter and a non-zero
 *  element if the is in the filter with a probability :math:`p`.
 */
int bloom_filter_contains(
    const struct bloom_filter_t *bf, const void *key, size_t size
);

/**
 * Get the number of items already in the bloom filter.
 *
 * :param bf: Bloom filter.
 * :returns: The number of items current set in the filter.
 */
size_t bloom_filter_size(const struct bloom_filter_t *bf);

/**
 * Check if more items than the expected maximum have been added.
 *
 * :param bf: Bloom filter.
 * :returns: A non-zero value if the bloom filter is not yet full.
 */
int bloom_filter_is_full(const struct bloom_filter_t *bf);

#endif /* !ALGORITHMS_BLOOM_FILTER_H */
