// SPDX-FileCopyrightText: 2014-2025 Alexandru Fikl <alexfikl@gmail.com>
// SPDX-License-Identifier: MIT

#include "sbf.h"

struct sbf_t *sbf_new(double p) {
    if (p < 0.0 || p > 1.0) {
        ALG_WARN("Probability not in [0, 1]: p = %g", p);
        return NULL;
    }

    struct sbf_t *sbf = alg_calloc(1, sizeof(struct sbf_t));

    /* set initial probability */
    sbf->p0 = p;
    sbf->probability_growth_ratio = SBF_DEFAULT_P_GROWTH_RATIO;

    /* set initial bitfield size */
    sbf->m0 = SBF_DEFAULT_BITFIELD_SIZE;
    sbf->bitfield_growth_ratio = SBF_DEFAULT_S_GROWTH_RATIO;

    /* allocate array of bloom filters */
    sbf->n = SBF_DEFAULT_FILTERS_COUNT;
    sbf->filter =
        (struct bloom_filter_t **) alg_calloc(sbf->n, sizeof(struct bloom_filter_t *));

    /* create the first bloom filter */
    sbf->last = 0;
    sbf->filter[0] = bloom_filter_new_ext(sbf->m0, sbf->p0, NULL);

    return sbf;
}

struct sbf_t *sbf_new_ext(size_t n, double p) {
    if (p < 0.0 || p > 1.0) {
        ALG_WARN("Probability not in [0, 1]: p = %g", p);
        return NULL;
    }

    struct sbf_t *sbf = alg_calloc(1, sizeof(struct sbf_t));

    /* set initial probability */
    sbf->p0 = p;
    sbf->probability_growth_ratio = SBF_DEFAULT_P_GROWTH_RATIO;

    /*
     * if this constructor is used, the user has an idea about the number of
     * elements they are going to add, so we allocate only 2 bloom filters
     * in our array in the off-chance that the user didn't quite get it right.
     */
    sbf->n = 2;
    sbf->last = 0;
    sbf->filter =
        (struct bloom_filter_t **) alg_calloc(sbf->n, sizeof(struct bloom_filter_t *));

    /* create the first bloom filter */
    sbf->filter[0] = bloom_filter_new(n, sbf->p0, NULL);

    /* get bitfield size from the first plain bloom filter */
    sbf->m0 = sbf->filter[0]->m;
    sbf->bitfield_growth_ratio = SBF_DEFAULT_S_GROWTH_RATIO;

    return sbf;
}

void sbf_destroy(struct sbf_t *sbf) {
    for (size_t i = 0; i <= sbf->last; ++i) {
        bloom_filter_destroy(sbf->filter[i]);
    }
    alg_free(sbf->filter);

    alg_free(sbf);
}

void sbf_set_probability_growth_ratio(struct sbf_t *sbf, double r) {
    if (r < 0.0 || r > 1.0) {
        ALG_WARN("Growth ratio not changed. Growth ratio not in [0, 1]: r = %g", r);
        return;
    }

    sbf->probability_growth_ratio = r;
}

void sbf_set_size_growth_ratio(struct sbf_t *sbf, double s) {
    if (s <= 1.0) {
        ALG_WARN("Growth ratio not changed. Growth ratio not > 1: s = %g", s);
        return;
    }

    sbf->bitfield_growth_ratio = s;
}

void sbf_dump(const struct sbf_t *sbf) {
    ALG_INFO("p0: %g", sbf->p0);
    ALG_INFO("Growth ratio r: %g", sbf->probability_growth_ratio);
    ALG_INFO("m0: %lu", sbf->m0);
    ALG_INFO("Growth ratio s: %g", sbf->bitfield_growth_ratio);
    ALG_INFO("Bloom filter count: %lu / %lu", sbf->last, sbf->n);

    for (size_t i = 0; i <= sbf->last; ++i) {
        ALG_INFO(
            "Filter %lu: p %g k %lu m %lu n %lu",
            i,
            sbf->filter[i]->p,
            sbf->filter[i]->k,
            sbf->filter[i]->m,
            sbf->filter[i]->n
        );
    }
}

size_t sbf_size(const struct sbf_t *sbf) {
    size_t added = 0;

    for (size_t i = 0; i < sbf->last; ++i) {
        added += sbf->filter[i]->added;
    }

    return added;
}

void sbf_add(struct sbf_t *sbf, const void *key, size_t size) {
    size_t m = 0;
    double p = 0;

    if (bloom_filter_is_full(sbf->filter[sbf->last])) {
        /* compute (m, p) for the next bloom filter */
        p = sbf->filter[sbf->last]->p * sbf->probability_growth_ratio;
        m = (size_t) lround(sbf->filter[sbf->last]->m * sbf->bitfield_growth_ratio);

        /* allocate a bigger bloom filter array, if necessary */
        if (sbf->last > (sbf->n - 1)) {
            sbf->n *= SBF_DEFAULT_SIZE_GROWTH_RATIO;
            sbf->filter = (struct bloom_filter_t **) realloc(
                sbf->filter, sbf->n * sizeof(struct bloom_filter_t *)
            );
        }

        /* create new bloom filter with the given data */
        sbf->last += 1;
        sbf->filter[sbf->last] = bloom_filter_new(m, p, NULL);
    }

    bloom_filter_add(sbf->filter[sbf->last], key, size);
}

bool sbf_contains(const struct sbf_t *sbf, const void *key, size_t size) {
    for (size_t i = 0; i < sbf->last; ++i) {
        if (bloom_filter_contains(sbf->filter[i], key, size)) {
            return true;
        }
    }

    return false;
}
