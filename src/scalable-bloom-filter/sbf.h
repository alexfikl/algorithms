// SPDX-FileCopyrightText: 2014-2025 Alexandru Fikl <alexfikl@gmail.com>
// SPDX-License-Identifier: MIT

#ifndef ALGORITHMS_SBF_H
#define ALGORITHMS_SBF_H

#include <bloom-filter.h>

#define SBF_DEFAULT_P_GROWTH_RATIO 0.85
#define SBF_DEFAULT_S_GROWTH_RATIO 2.0
#define SBF_DEFAULT_SIZE_GROWTH_RATIO 2
#define SBF_DEFAULT_BITFIELD_SIZE 32
#define SBF_DEFAULT_FILTERS_COUNT 8

/**
 * Scalable Bloom filter.
 */
struct sbf_t {
    size_t m0;
    double bitfield_growth_ratio;

    double p0;
    double probability_growth_ratio;

    size_t n;
    size_t last;
    struct bloom_filter_t **filter;
};

/**
 * Construct a new scalable bloom filter with the desired probability.
 *
 * In the beginning, the scalable bloom filter will only contain one plain
 * bloom filter.
 *
 * :param p: Desired error probability.
 * :returns: An allocated scalable bloom filter.
 *
 * :seealso: http://gsd.di.uminho.pt/members/cbm/ps/dbloom.pdf
 */
struct sbf_t *sbf_new(double p);

/**
 * Construct a new scalable bloom filter.
 *
 * The first bloom filter will be constructed such that it can hold :math:`n`
 * elements with a false positive probability of :math:`p`. If more than :math:`n`
 * elements are added, the filter will grow accordingly.
 *
 * :param n: Estimated maximum number of elements in the filter.
 * :param p: Desired error probability.
 * :returns: An allocated scalable bloom filter.
 */
struct sbf_t *sbf_new_ext(size_t n, double p);

/**
 * Deallocate the bloom filter data.
 */
void sbf_destroy(struct sbf_t *sbf);

/**
 * Set the growth ratio for the slice sizes in the bloom filters.
 *
 * When approaching the maximum number of elements we can add in the filter
 * :math:`i`, a new filter is created with a slice size of:
 *
 * .. math::
 *
 *      m_{i + 1} = m_i\, s.
 *
 * The growth ratio should not be changed after adding elements to the filter
 * because it could make the false positive probability bigger.
 *
 * :param sbf: Scalable bloom filter.
 * :param s: Growth ratio.
 */
void sbf_set_size_growth_ratio(struct sbf_t *sbf, double s);

/**
 * Set the growth ratio for the error probability.
 *
 * When approaching the maximum number of elements we can add in the filter
 * :math:`i`, a new filter is created with an error probability of:
 *
 * .. math::
 *
 *      P_{i + 1} = P_i\, r.
 *
 * The growth ratio should not be changed after adding elements to the filter
 * because it could make the false positive probability bigger.
 *
 * :param sbf: Scalable bloom filter.
 * :param r: Growth ratio.
 */
void sbf_set_probability_growth_ratio(struct sbf_t *sbf, double r);

/**
 * Dump basic information about the bloom filter.
 */
void sbf_dump(const struct sbf_t *sbf);

/**
 * Get the number of elements current in the bloom filter.
 *
 * :param sbf: Scalable bloom filter.
 * :returns: Number of elements added.
 */
size_t sbf_size(const struct sbf_t *sbf);

/**
 * Add a new element to the bloom filter.
 *
 * :param sbf: Scalable bloom filter.
 * :param key: Data to add.
 * :param size: Size of the data in bytes.
 */
void sbf_add(struct sbf_t *sbf, const void *key, size_t size);

/**
 * Check if the bloom filter contains a given element.
 *
 * :param sbf: Scalable bloom filter.
 * :param key: Data to add.
 * :param size: Size of the data in bytes.
 * :returns: *true* or *false* depending on whether the element has
 *  been added to the bloom filter. By construction, there are no false
 *  negatives, but there are false positives.
 */
bool sbf_contains(const struct sbf_t *sbf, const void *key, size_t size);

#endif /* ALGORITHMS_SBF_H */
