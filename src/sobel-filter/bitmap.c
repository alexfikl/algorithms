// SPDX-FileCopyrightText: 2014-2025 Alexandru Fikl <alexfikl@gmail.com>
// SPDX-License-Identifier: MIT

#include "bitmap.h"

#include <math.h>

#define PNG_HEADER_SIZE 4

/**
 * @brief Convert from RGB to Grayscale.
 *
 * @param[in] r     Red chromaticity in [0, 255].
 * @param[in] g     Green chromaticity in [0, 255].
 * @param[in] b     Blue chromaticity in [0, 255].
 * @return          Luma value also in [0, 255].
 *
 * @sa https://en.wikipedia.org/wiki/Grayscale
 * @sa https://en.wikipedia.org/wiki/Luma_%28video%29
 */
static png_byte pixel_grayscale(png_byte r, png_byte g, png_byte b) {
    static const double gamma = 1.9;
    static const double cr = 0.212671;
    static const double cg = 0.715160;
    static const double cb = 0.072169;

    double R, G, B;
    double Y;

    /* convert RGB values to the "linear" / not gamma corrected form */
    R = pow((double) r / 255.0, gamma);
    G = pow((double) g / 255.0, gamma);
    B = pow((double) b / 255.0, gamma);

    /* compute the luminance in the XYZ space */
    Y = cr * R + cg * G + cb * B;

    /* return a value in [0, 255] based on Y */
    return (png_byte) (fmax(0.0, fmin(Y, 1.0)) * 255.0);
}

/**
 * @brief Get the pixel size for a given color type.
 *
 * The associations are:
 *  * @c PNG_COLOR_TYPE_GRAY has 1 gray channel.
 *  * @c PNG_COLOR_TYPE_GRAY_ALPHA has 1 gray and 1 alpha channel.
 *  * @c PNG_COLOR_TYPE_RGB has an (R, G, B) triplet.
 *  * @c PNG_COLOR_TYPE_RGB_ALPHA has an (R, G, B) triplet and 1 alpha channel.
 *
 * @param[in] color_type        Given color type.
 * @return                      Number of channels in a pixel of the given type.
 *
 * @sa png_get_channels
 */
static png_byte bitmap_color_type_channels(png_byte color_type) {
    png_byte channels = 0;

    switch (color_type) {
    case PNG_COLOR_TYPE_GRAY:
        channels = 1;
        break;
    case PNG_COLOR_TYPE_GRAY_ALPHA:
        channels = 2;
        break;
    case PNG_COLOR_TYPE_RGB:
        channels = 3;
        break;
    case PNG_COLOR_TYPE_RGB_ALPHA:
        channels = 4;
        break;
    default:
        channels = 0;
        break;
    }

    return channels;
}

bitmap_t *
bitmap_new(size_t height, size_t width, png_byte color_type, png_byte bit_depth) {
    bitmap_t *bmp = alg_calloc(1, sizeof(bitmap_t));

    bmp->height = height;
    bmp->width = width;
    bitmap_set_color_type(bmp, color_type);
    bitmap_set_bit_depth(bmp, bit_depth);

    bmp->pixel = (png_bytepp) alg_malloc(height * sizeof(png_bytep));
    for (size_t i = 0; i < height; ++i) {
        bmp->pixel[i] =
            (png_bytep) alg_malloc(bmp->width * bmp->channels * sizeof(png_byte));
    }

    return bmp;
}

void bitmap_destroy(bitmap_t *bmp) {
    for (size_t i = 0; i < bmp->height; ++i) {
        alg_free(bmp->pixel[i]);
    }
    alg_free(bmp->pixel);
    alg_free(bmp);
}

bitmap_t *bitmap_copy(const bitmap_t *bmp) {
    bitmap_t *copy = NULL;

    copy = bitmap_new(bmp->height, bmp->width, bmp->color_type, bmp->bit_depth);
    for (size_t i = 0; i < bmp->height; ++i) {
        memcpy(
            &(copy->pixel[i][0]),
            &(bmp->pixel[i][0]),
            bmp->width * bmp->channels * sizeof(png_byte)
        );
    }

    return copy;
}

void bitmap_set_color_type(bitmap_t *bmp, png_byte color_type) {
    bmp->color_type = color_type;
    bmp->channels = bitmap_color_type_channels(color_type);

    ALG_CHECK_ABORT(bmp->channels, "Unsupported color type: code %d", color_type);
}

bool bitmap_has_alpha(const bitmap_t *bmp) {
    return ((bmp->color_type ^ PNG_COLOR_MASK_ALPHA) == 0 ? false : true);
}

void bitmap_set_bit_depth(bitmap_t *bmp, png_byte bit_depth) {
    ALG_CHECK_ABORT(bit_depth == 8, "Required bit depth of 8 (given %d)", bit_depth);

    bmp->bit_depth = bit_depth;
}

bitmap_t *bitmap_read_png(const char *filename, bool grayscale, bool preserve_alpha) {
    int ret = 0;

    uint32_t width = 0;
    uint32_t height = 0;
    png_byte color_type = 0;
    png_byte bmp_color_type = 0;
    png_byte bit_depth = 0;

    uint8_t sig[PNG_HEADER_SIZE];
    png_structp png;
    png_infop info;
    png_bytepp data;
    png_bytep pixel;
    png_bytep bmp_pixel;
    size_t channels;

    bitmap_t *bmp = NULL;
    FILE *fd = NULL;

    /* open file */
    fd = fopen(filename, "rb");
    ALG_CHECK_ABORT(fd != NULL, "Could not open file \"%s\"", filename);

    /* check that the file is a valid PNG */
    ret = (int) fread(sig, sizeof(char), PNG_HEADER_SIZE, fd);
    ALG_CHECK_ABORT(
        ret == PNG_HEADER_SIZE, "Could not read from file \"%s\"", filename
    );

    ret = png_sig_cmp(sig, 0, PNG_HEADER_SIZE);
    ALG_CHECK_ABORT(ret == 0, "File \"%s\" is not a PNG file", filename);

    /* create libpng reading structs */
    png = png_create_read_struct(PNG_LIBPNG_VER_STRING, NULL, NULL, NULL);
    ALG_CHECK_ABORT(png, "Cannot create libpng read struct");

    info = png_create_info_struct(png);
    ALG_CHECK_ABORT(info, "Cannot create libpng info struct");

    /* setup libpng error handling */
    if (setjmp(png_jmpbuf(png))) {
        ALG_DEBUG("Error during IO initialization");
        png_destroy_read_struct(&png, &info, NULL);
        return NULL;
    }

    /* setup I/O */
    png_init_io(png, fd);
    png_set_sig_bytes(png, PNG_HEADER_SIZE);

    png_read_info(png, info);
    height = png_get_image_height(png, info);
    width = png_get_image_width(png, info);
    color_type = png_get_color_type(png, info);
    bit_depth = png_get_bit_depth(png, info);
    channels = bitmap_color_type_channels(color_type);

    png_set_interlace_handling(png);
    png_read_update_info(png, info);

    ALG_DEBUG("File: %s", filename);
    ALG_DEBUG("Size: %ux%u", width, height);
    ALG_DEBUG("Bytes per row: %lu", png_get_rowbytes(png, info));
    ALG_DEBUG("Color type: %d", color_type);
    ALG_DEBUG("Bit depth: %d", bit_depth);

    /* change color type according to options */
    if (grayscale) {
        bmp_color_type = PNG_COLOR_TYPE_GRAY;
    } else {
        bmp_color_type = color_type;
    }

    if (preserve_alpha) {
        bmp_color_type |= (color_type ^ PNG_COLOR_MASK_ALPHA);
    }

    /* create our bitmap struct */
    bmp = bitmap_new(height, width, bmp_color_type, bit_depth);

    /* read image data */
    data = (png_bytepp) alg_malloc(height * sizeof(png_bytep));
    for (size_t i = 0; i < bmp->height; ++i) {
        data[i] = (png_bytep) alg_malloc(png_get_rowbytes(png, info));
    }
    png_read_image(png, data);

    /* copy pixels and convert to grayscale if needed */
    for (size_t i = 0; i < bmp->height; ++i) {
        for (size_t j = 0; j < bmp->width; ++j) {
            pixel = &(data[i][j * channels]);
            bmp_pixel = &(bmp->pixel[i][j * bmp->channels]);

            switch (color_type) {
            case PNG_COLOR_TYPE_GRAY_ALPHA:
                if (preserve_alpha) {
                    bmp_pixel[bmp->channels - 1] = pixel[1];
                }
                ALG_FALLTHROUGH()
            case PNG_COLOR_TYPE_GRAY:
                bmp_pixel[0] = pixel[0];
                break;
            case PNG_COLOR_TYPE_RGB_ALPHA:
                if (preserve_alpha) {
                    bmp_pixel[bmp->channels - 1] = pixel[3];
                }
                ALG_FALLTHROUGH()
            case PNG_COLOR_TYPE_RGB:
                if (grayscale) {
                    bmp_pixel[0] = pixel_grayscale(pixel[0], pixel[1], pixel[2]);
                } else {
                    bmp_pixel[0] = pixel[0];
                    bmp_pixel[1] = pixel[1];
                    bmp_pixel[2] = pixel[2];
                }
                break;
            default:
                ALG_ABORT("Unsupported color type: code %d", color_type);
                break;
            }
        }
    }

    /* free everything */
    for (size_t i = 0; i < bmp->height; ++i) {
        alg_free(data[i]);
    }
    alg_free(data);
    png_destroy_read_struct(&png, &info, NULL);

    fclose(fd);
    return bmp;
}

void bitmap_write_png(const bitmap_t *bmp, const char *filename) {
    png_structp png;
    png_infop info;
    FILE *fd = NULL;

    /* open file */
    fd = fopen(filename, "wb");
    ALG_CHECK_ABORT(fd != NULL, "Could not open file \"%s\"", filename);

    /* create libpng reading structs */
    png = png_create_write_struct(PNG_LIBPNG_VER_STRING, NULL, NULL, NULL);
    ALG_CHECK_ABORT(png, "Cannot create libpng write struct");

    info = png_create_info_struct(png);
    ALG_CHECK_ABORT(info, "Cannot create libpng info struct");

    /* setup libpng error handling */
    if (setjmp(png_jmpbuf(png))) {
        ALG_DEBUG("Error during IO initialization");
        png_destroy_write_struct(&png, &info);
        return;
    }

    /* initialize IO */
    png_init_io(png, fd);
    png_set_IHDR(
        png,
        info,
        (uint32_t) bmp->width,
        (uint32_t) bmp->height,
        bmp->bit_depth,
        bmp->color_type,
        PNG_INTERLACE_NONE,
        PNG_COMPRESSION_TYPE_BASE,
        PNG_FILTER_TYPE_BASE
    );
    png_write_info(png, info);

    /* write data */
    png_write_image(png, bmp->pixel);
    png_write_end(png, NULL);

    png_destroy_write_struct(&png, &info);
    fclose(fd);
}
