// SPDX-FileCopyrightText: 2014-2025 Alexandru Fikl <alexfikl@gmail.com>
// SPDX-License-Identifier: MIT

#ifndef ALGORITHMS_BITMAP_H
#define ALGORITHMS_BITMAP_H

#include <algorithms.h>
#include <png.h>

/**
 * @brief Bitmap type to store RGB or Grayscale images.
 */
typedef struct {
    /**
     * @brief Height, in pixels, of an image.
     */
    size_t height;

    /**
     * @brief Width, in pixels, of an image.
     */
    size_t width;

    /**
     * @brief List of pixel data.
     *
     * A pixel is described by the color data contained in it. This could be
     * the triplets (red, green, blue) of chromaticities in case of RGB or
     * just a luma value in the case of Grayscale. If required, the pixel
     * also contains an alpha transparency value.
     *
     * This makes the size of the array ``height x width x channels``.
     */
    png_byte **pixel;

    /**
     * @brief Number of channels in each pixel, based on the color type.
     */
    uint8_t channels;

    /**
     * @brief Color type of image, based on libpng types.
     */
    png_byte color_type;

    /**
     * @brief Bit depth of the image. Only 8 (i.e. uint8_t) is supported.
     */
    png_byte bit_depth;

    char padding[5];
} bitmap_t;

/**
 * @brief Allocate a new bitmap.
 *
 * @param[in] height        Height, in pixels, of the image.
 * @param[in] width         Width, in pixels, of the image.
 * @param[in] color_type    Type of the pixel color.
 * @param[in] bit_depth     Bit depth.
 * @return                  A fully allocated bitmap. The pixel data contained
 *                          within is not initialized.
 */
bitmap_t *
bitmap_new(size_t height, size_t width, png_byte color_type, png_byte bit_depth);

/**
 * @brief Deallocate a bitmap.
 *
 * @param[in,out] bmp       Bitmap to deallocate.
 */
void bitmap_destroy(bitmap_t *bmp);

/**
 * @brief Make an exact copy of a bitmap.
 *
 * @param[in] bmp           Bitmap to copy.
 * @return                  A fully allocated copy of the given bitmap.
 */
bitmap_t *bitmap_copy(const bitmap_t *bmp);

/**
 * @brief Set the color type of an existing bitmap.
 *
 * No changes are done to the actual pixel data.
 *
 * @param[in,out] bmp       Bitmap to change.
 * @param[in] color_type    New color type for the bitmap.
 */
void bitmap_set_color_type(bitmap_t *bmp, png_byte color_type);

/**
 * @brief Check that a given bitmap has an alpha channel.
 *
 * @param[in] bmp           Bitmap to check.
 */
bool bitmap_has_alpha(const bitmap_t *bmp);

/**
 * @brief Set the bit depth of an existing bitmap.
 *
 * No changes are done to the actual pixel data.
 *
 * @param[in,out] bmp       Bitmap to change.
 * @param[in] bit_depth     New bit depth for the bitmap.
 */
void bitmap_set_bit_depth(bitmap_t *bmp, png_byte bit_depth);

/**
 * @brief Read the bitmap pixel data from a given PNG file.
 *
 * @param[in] filename      PNG file name.
 * @param[in] grayscale     Boolean to indicate that the PNG pixel data should
 *                          be converted to grayscale.
 * @param[in] preserve_alpha Boolean to indicate that the resulting bitmap
 *                          should preserve the alpha transparency of the
 *                          original PNG file.
 * @return                  A fully allocated and initialize bitmap with data
 *                          from the PNG file.
 */
bitmap_t *bitmap_read_png(const char *filename, bool grayscale, bool preserve_alpha);

/**
 * @brief Write a bitmap to a given PNG file.
 *
 * @param[in] bmp           Bitmap to write.
 * @param[in] filename      Output filename.
 */
void bitmap_write_png(const bitmap_t *bmp, const char *filename);

#endif // ALGORITHMS_BITMAP_H
