// SPDX-FileCopyrightText: 2014-2025 Alexandru Fikl <alexfikl@gmail.com>
// SPDX-License-Identifier: MIT

#include "sobel.h"
#include "algorithms.h"

/**
 * @brief Apply the Sobel operator on the neighborhood of a given pixel.
 *
 * This function computed the @c x and @c y gradients at a given point in
 * the image using the @c Gx and @c Gy Sobel operators. Once the values are
 * computed, it returns the magnitude of the gradient:
 * \f[
 *      A = \sqrt{A_x^2 + A_y^2}.
 * \f]
 *
 * @param[in] pixel             Pixel data.
 * @param[in] n                 Row of the center pixel.
 * @param[in] m                 Column of the center pixel.
 * @return                      Magnitude of the gradient at the given pixel.
 */
static png_byte sobel_filter_apply(png_bytepp pixel, int64_t n, int64_t m) {
    double grad_x = 0.0;
    double grad_y = 0.0;
    double magnitude = 0.0;

    for (int64_t i = -1; i <= 1; ++i) {
        for (int64_t j = -1; j <= 1; ++j) {
            grad_x += sobel_gx[i + 1][j + 1] * pixel[n + i][m + j];
            grad_y += sobel_gy[i + 1][j + 1] * pixel[n + i][m + j];
        }
    }

    magnitude = sqrt(grad_x * grad_x + grad_y * grad_y);
    return (png_byte) ALG_MIN(255.0, magnitude);
}

bitmap_t *sobel_filter(const bitmap_t *orig) {
    bitmap_t *bmp = NULL;
    ALG_CHECK_ABORT(
        orig->color_type == PNG_COLOR_TYPE_GRAY, "Bitmap not in grayscale."
    );

    bmp = bitmap_copy(orig);
    for (size_t i = 1; i < orig->height - 1; ++i) {
        for (size_t j = 1; j < orig->width - 1; ++j) {
            bmp->pixel[i][j] =
                sobel_filter_apply(orig->pixel, (int64_t) i, (int64_t) j);
        }
    }

    return bmp;
}
