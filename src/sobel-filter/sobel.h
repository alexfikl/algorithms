// SPDX-FileCopyrightText: 2014-2025 Alexandru Fikl <alexfikl@gmail.com>
// SPDX-License-Identifier: MIT

#ifndef ALGORITHMS_SOBEL_H
#define ALGORITHMS_SOBEL_H

#include <bitmap.h>

/**
 * @brief Sobel operator in the x direction.
 */
static const double sobel_gx[3][3] = {
    {-1.0, 0.0, 1.0}, {-2.0, 0.0, 2.0}, {-1.0, 0.0, 1.0}
};

/**
 * @brief Sobel operator in the y direction.
 */
static const double sobel_gy[3][3] = {
    {-1.0, -2.0, -1.0}, {0.0, 0.0, 0.0}, {1.0, 2.0, 1.0}
};

/**
 * @brief Apply the Sobel edge detection filter on a given bitmap.
 *
 * @param[in] orig      Bitmap on which we want to apply the filter.
 * @return              A new Grayscale image with the edges marked by higher
 *                      values (i.e. closer to 255, or white).
 */
bitmap_t *sobel_filter(const bitmap_t *orig);

#endif /* ALGORITHMS_SOBEL_H */
