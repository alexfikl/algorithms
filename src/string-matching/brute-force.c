// SPDX-FileCopyrightText: 2014-2025 Alexandru Fikl <alexfikl@gmail.com>
// SPDX-License-Identifier: MIT

#include "string-matching-private.h"

int string_match_bf(const char *string, size_t n, const char *sub, size_t m) {
    for (size_t i = 0; i <= (n - m); ++i) {
        if (memcmp(string + i, sub, m) == 0) {
            return i;
        }
    }

    return -1;
}
