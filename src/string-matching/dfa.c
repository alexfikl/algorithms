// SPDX-FileCopyrightText: 2014-2025 Alexandru Fikl <alexfikl@gmail.com>
// SPDX-License-Identifier: MIT

#include "string-matching-private.h"
#include <deterministic-finite-automaton.h>

static dfa_t *string_match_dfa_new(const char *sub, size_t m) {
    dfa_t *dfa = dfa_new(m);
    int state = dfa_initial_state(dfa);
    size_t prev_state;

    for (size_t i = 0; i < m; ++i) {
        prev_state = dfa_target_state(dfa, state, sub[i]);
        dfa_set_transition(dfa, state, sub[i], prev_state);
    }

    return dfa;
}

int string_match_dfa(const char *string, size_t n, const char *sub, size_t m) {
    dfa_t *automaton = string_match_dfa_new(sub, m);
    const size_t terminal_state = dfa_terminal_state(automaton);
    size_t state = dfa_initial_state(automaton);

    for (size_t i = 0; i < n; ++i) {
        state = dfa_target_state(automaton, state, string[i]);
        if (state == terminal_state) {
            return i - m + 1;
        }
    }

    return -1;
}
