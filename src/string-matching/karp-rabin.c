// SPDX-FileCopyrightText: 2014-2025 Alexandru Fikl <alexfikl@gmail.com>
// SPDX-License-Identifier: MIT

#include "string-matching-private.h"

#include <hash-map.h>

/* TODO: could use faster hash function? */
static hash_function_t fn = hash_djb;

int string_match_kr(const char *string, size_t n, const char *sub, size_t m) {
    const uint32_t sub_hash = fn((uint8_t *) sub, m, ALG_HASH_DEFAULT_SEED);
    uint32_t string_hash = 0;

    for (size_t i = 0; i <= n - m; ++i) {
        string_hash = fn((uint8_t *) (string + i), m, ALG_HASH_DEFAULT_SEED);
        if (sub_hash == string_hash) {
            if (memcmp(string + i, sub, m) == 0) {
                return i;
            }
        }
    }

    return -1;
}
