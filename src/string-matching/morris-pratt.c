// SPDX-FileCopyrightText: 2014-2025 Alexandru Fikl <alexfikl@gmail.com>
// SPDX-License-Identifier: MIT

#include "string-matching-private.h"

#include <hash-map.h>

static void compute_next_table(const char *sub, size_t m, int *next) {
    int j;

    j = next[0] = -1;
    for (size_t i = 0; i < m; ++i, ++j) {
        while (j > -1 && sub[i] != sub[j]) {
            j = next[j];
        }
        next[i + 1] = j + 1;
    }
}

int string_match_mp(const char *string, size_t n, const char *sub, size_t m) {
    int *next = (int *) alg_malloc(m * sizeof(int));
    int result = -1;
    int i = 0;

    compute_next_table(sub, m, next);

    for (size_t j = 0; j < n; ++i, ++j) {
        while (i > -1 && sub[i] != string[j]) {
            i = next[i];
        }

        if (i + 1 >= (int) m) {
            result = j - i;
            break;
        }
    }

    alg_free(next);
    return result;
}
