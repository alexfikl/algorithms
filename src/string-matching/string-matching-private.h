// SPDX-FileCopyrightText: 2014-2025 Alexandru Fikl <alexfikl@gmail.com>
// SPDX-License-Identifier: MIT

#ifndef ALGORITHMS_STRING_MATCHING_PRIVATE_H
#define ALGORITHMS_STRING_MATCHING_PRIVATE_H

#include "string-matching.h"

/**
 * @brief Substring search using a naive bruteforce algorithm.
 *
 * Characteristics:
 *  * no preprocessing stage
 *  * constant space
 *  * complexity of O(nm)
 *
 * @sa http://www-igm.univ-mlv.fr/~lecroq/string/node3.html#SECTION0030
 */
int string_match_bf(const char *string, size_t n, const char *sub, size_t m);

int string_match_dfa(const char *string, size_t n, const char *sub, size_t m);

int string_match_kr(const char *string, size_t n, const char *sub, size_t m);

int string_match_mp(const char *string, size_t n, const char *sub, size_t m);

#endif
