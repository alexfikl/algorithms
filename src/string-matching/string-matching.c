// SPDX-FileCopyrightText: 2014-2025 Alexandru Fikl <alexfikl@gmail.com>
// SPDX-License-Identifier: MIT

#include "string-matching-private.h"

int string_match(
    const char *string,
    size_t n,
    const char *sub,
    size_t m,
    string_matching_type_t algorithm
) {
    if (sub == NULL || m == 0) {
        return 0;
    }

    if (string == NULL || n == 0) {
        return -1;
    }

    if (m > n) {
        return -1;
    }

    ALG_ASSERT(strlen(string) <= n);
    ALG_ASSERT(strlen(sub) <= m);

    switch (algorithm) {
    case MATCH_BRUTE_FORCE:
        return string_match_bf(string, n, sub, m);
    case MATCH_KARP_RABIN:
        return string_match_kr(string, n, sub, m);
    case MATCH_MORRIS_PRATT:
        return string_match_mp(string, n, sub, m);
    default:
        ALG_ABORT("Unknown algorithm ID: %d", algorithm);
    }

    return -1;
}
