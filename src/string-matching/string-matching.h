// SPDX-FileCopyrightText: 2014-2025 Alexandru Fikl <alexfikl@gmail.com>
// SPDX-License-Identifier: MIT

#ifndef ALGORITHMS_STRING_MATCHING_H
#define ALGORITHMS_STRING_MATCHING_H

#include <algorithms.h>

typedef enum {
    MATCH_BRUTE_FORCE,
    MATCH_KARP_RABIN,
    MATCH_MORRIS_PRATT,
    MATCH_DEFAULT = MATCH_BRUTE_FORCE
} string_matching_type_t;

/**
 * @brief Check if a given substring is contained in a string.
 *
 * @param[in] string        String.
 * @param[in] n             Size of string.
 * @param[in] sub           Substring to search for.
 * @param[in] m             Size of substring.
 * @param[in] algorithm     The type of algorithm to use.
 * @return                  A positive integer representing the position of the
 *                          substring in the string. If the substring cannot
 *                          be found, -1 is returned.
 * @sa http://www-igm.univ-mlv.fr/~lecroq/string/
 */
int string_match(
    const char *string,
    size_t n,
    const char *sub,
    size_t m,
    string_matching_type_t algorithm
);

#endif
