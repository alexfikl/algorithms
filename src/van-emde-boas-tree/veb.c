// SPDX-FileCopyrightText: 2014-2025 Alexandru Fikl <alexfikl@gmail.com>
// SPDX-License-Identifier: MIT

#include "veb.h"

#define high(x) high_impl(x, veb->cluster_size)
#define low(x) low_impl(x, veb->cluster_size)
#define index(i, j) index_impl(i, j, veb->cluster_size)

#define MIN_UNDEFINED (-1)
#define MAX_UNDEFINED (-2)
#define MIN_UNIVERSE_SIZE (2)

/**
 * @brief Get the cluster to which a given element belongs.
 *
 * @param[in] x             Element we are looking for.
 * @param[in] u             Size of the cluster we are looking into.
 * @return                  Cluster containing x.
 */
static inline int64_t high_impl(int64_t x, int64_t u) {
    return (int64_t) lround(floor(x / u));
}

/**
 * @brief Get the position of x in a cluster.
 *
 * @param[in] x             Element we are looking for.
 * @param[in] u             Size of the cluster we are looking into.
 * @return                  Position in the cluster.
 */
static inline int64_t low_impl(int64_t x, int64_t u) {
    return x % u;
}

/**
 * @brief Get the element.
 *
 * @param[in] i             Cluster to which the element belongs.
 * @param[in] j             Position in the cluster.
 * @param[in] u             Size of the cluster we're looking into.
 * @return                  Element.
 */
static inline int64_t index_impl(int64_t i, int64_t j, int64_t u) {
    return i * u + j;
}

/**
 * @brief Swap two integer values.
 */
static inline void swap(int64_t *a, int64_t *b) {
    int64_t tmp = *a;
    *a = *b;
    *b = tmp;
}

veb_t *veb_new(int64_t universe_size) {
    veb_t *veb = alg_malloc(sizeof(veb_t));

    veb->universe_size = universe_size;
    veb->cluster_size = (int64_t) lround(floor(sqrt(universe_size)));
    veb->min = MIN_UNDEFINED;
    veb->max = MAX_UNDEFINED;

    veb->summary = NULL;
    veb->cluster = NULL;

    if (universe_size > MIN_UNIVERSE_SIZE) {
        veb->cluster =
            (veb_t **) alg_calloc((size_t) veb->cluster_size, sizeof(veb_t *));
    }

    return veb;
}

void veb_preallocate(veb_t *veb) {
    if (veb == NULL) {
        return;
    }

    if (veb->universe_size <= MIN_UNIVERSE_SIZE) {
        return;
    }

    /* recursively allocate the summary */
    if (veb->summary == NULL) {
        veb->summary = veb_new(veb->cluster_size);
    }
    veb_preallocate(veb->summary);

    /* recursively allocate the clusters */
    for (int64_t i = 0; i < veb->cluster_size; ++i) {
        if (veb->cluster[i] == NULL) {
            veb->cluster[i] = veb_new(veb->cluster_size);
        }
        veb_preallocate(veb->cluster[i]);
    }
}

void veb_free(veb_t *veb) {
    if (veb == NULL) {
        return;
    }

    /* recursively free clusters */
    if (veb->universe_size > MIN_UNIVERSE_SIZE) {
        for (int64_t i = 0; i < veb->cluster_size; ++i) {
            veb_free(veb->cluster[i]);
        }
        alg_free(veb->cluster);
    }

    veb_free(veb->summary);
    alg_free(veb);
}

int64_t veb_min(const veb_t *veb) {
    return veb->min;
}

int64_t veb_max(const veb_t *veb) {
    return veb->max;
}

bool veb_is_empty(const veb_t *veb) {
    return veb == NULL || veb->min < 0 || veb->max < 0 || veb->min > veb->max;
}

static void veb_dump_level(const veb_t *veb, int indent, int level) {
    if (veb == NULL || veb->universe_size == MIN_UNIVERSE_SIZE) {
        return;
    }

    printf("%*sSummary:", indent, " ");
    if (veb->summary != NULL) {
        printf(" min %ld max %ld", veb->summary->min, veb->summary->max);
    }
    printf("\n");
    veb_dump_level(veb->summary, indent + 2, 1);

    printf("%*sClusters:\n", indent, " ");
    for (int64_t i = 0; i < veb->cluster_size; ++i) {
        if (veb->cluster[i] == NULL) {
            continue;
        }

        printf(
            "%*s[%d][%ld] min %ld max %ld",
            indent,
            " ",
            level,
            i,
            veb->cluster[i]->min,
            veb->cluster[i]->max
        );
        if (veb->cluster[i]->universe_size == MIN_UNIVERSE_SIZE) {
            printf(" (leaf)");
        }
        printf("\n");

        veb_dump_level(veb->cluster[i], indent + 2, level + 1);
    }
    printf("\n");
}

void veb_dump(const veb_t *veb) {
    if (veb == NULL) {
        return;
    }

    printf("Universe size: %ld\n", veb->universe_size);
    printf("root min %ld max %ld\n", veb->min, veb->max);
    veb_dump_level(veb, 2, 1);
}

bool veb_contains(const veb_t *veb, int64_t x) {
    if (veb_is_empty(veb)) {
        return false;
    }

    if (x == veb->min || x == veb->max) {
        return true;
    }

    if (veb->universe_size == MIN_UNIVERSE_SIZE) {
        return false;
    }

    return veb_contains(veb->cluster[high(x)], low(x));
}

void veb_insert(veb_t *veb, int64_t x) {
    /* do not insert elements outside of the universe */
    if (veb == NULL) {
        return;
    }

    if (x >= veb->universe_size || x < 0) {
        return;
    }

    /* empty tree */
    if (veb->min == MIN_UNDEFINED) {
        veb->min = veb->max = x;
        return;
    }

    /* update min and max */
    if (x < veb->min) {
        swap(&x, &(veb->min));
    }

    if (x > veb->max) {
        swap(&x, &(veb->max));
    }

    /* we reached a leaf node, nothing left to do */
    if (veb->universe_size == MIN_UNIVERSE_SIZE) {
        return;
    }

    int64_t i = high(x);
    int64_t j = low(x);

    /* update the summary if inserting into an empty cluster */
    /* NOTE: the clusters may be allocated by veb_preallocate */
    if (veb_is_empty(veb->cluster[i])) {
        if (veb->cluster[i] == NULL) {
            veb->cluster[i] = veb_new(veb->cluster_size);
        }

        if (veb->summary == NULL) {
            veb->summary = veb_new(veb->cluster_size);
        }

        veb_insert(veb->summary, i);
    }

    /* recursively insert into the cluster */
    veb_insert(veb->cluster[i], j);
}

void veb_delete(veb_t *veb, int64_t x) {
    /* skip empty trees */
    if (veb_is_empty(veb)) {
        return;
    }

    /* skip deleting elements outside [min, max] range */
    if (x < veb->min || x > veb->max) {
        return;
    }

    if (veb->min == veb->max) {
        veb->min = MIN_UNDEFINED;
        veb->max = MAX_UNDEFINED;

        return;
    }

    if (veb->universe_size == MIN_UNIVERSE_SIZE) {
        if (x == veb->min) {
            veb->min = veb->max;
        }

        if (x == veb->max) {
            veb->max = veb->min;
        }

        return;
    }

    int64_t i = 0;

    if (x == veb->min) {
        i = veb->summary->min;
        x = veb->min = index(i, veb->cluster[i]->min);
    }

    i = high(x);
    veb_delete(veb->cluster[i], low(x));
    if (veb->cluster[i]->min == MIN_UNDEFINED) {
        veb_delete(veb->summary, i);
    }

    if (x == veb->max) {
        i = veb->summary->max;
        veb->max = index(i, veb->cluster[i]->max);
    }
}

int64_t veb_next(const veb_t *veb, int64_t x) {
    /* skip empty trees */
    if (veb_is_empty(veb)) {
        return MAX_UNDEFINED;
    }

    /* value is smaller than min => successor is necessarily min */
    if (x < veb->min) {
        return veb->min;
    }

    /* value is larger than max => successor doesn't exist */
    if (x >= veb->max) {
        return MAX_UNDEFINED;
    }

    /* special case for MIN_UNIVERSE */
    if (veb->universe_size == MIN_UNIVERSE_SIZE) {
        if (x == veb->min) {
            return veb->max;
        }

        return MAX_UNDEFINED;
    }

    int64_t i = high(x);
    int64_t j = low(x);

    if (j < veb->cluster[i]->max) {
        /* successor in same cluster so we look for it here */
        j = veb_next(veb->cluster[i], j);
        return index(i, j);
    } else {
        /* successor in different cluster so we find next non-empty cluster */
        i = veb_next(veb->summary, i);

        if (i == MAX_UNDEFINED) {
            /* no non-empty cluster */
            return MAX_UNDEFINED;
        } else {
            /* non-empty cluster found so its min has to be the successor */
            j = veb->cluster[i]->min;
            return index(i, j);
        }
    }
}

int64_t veb_prev(const veb_t *veb, int64_t x) {
    ALG_INFO("Looking for predecessor of %ld", x);

    /* skip empty trees */
    if (veb_is_empty(veb)) {
        ALG_INFO("Tree is empty");
        return MIN_UNDEFINED;
    }

    ALG_INFO("min %ld max %ld", veb->min, veb->max);
    /* value is larger than max => predecessor has to be max */
    if (x > veb->max) {
        ALG_INFO("Predecessor is max %ld", veb->max);
        return veb->max;
    }

    /* value is smaller than min => predecessor doesn't exist */
    if (x <= veb->min) {
        ALG_INFO("No predecessor. Smaller than min %ld", veb->min);
        return MIN_UNDEFINED;
    }

    /* special case for MIN_UNIVERSE */
    if (veb->universe_size == MIN_UNIVERSE_SIZE) {
        if (x == veb->max) {
            ALG_INFO("[MIN_UNIVERSE] Predecessor is min %ld", veb->min);
            return veb->min;
        }

        ALG_INFO("[MIN_UNIVERSE] No predecessor.");
        return MIN_UNDEFINED;
    }

    int64_t i = high(x);
    int64_t j = low(x);

    ALG_INFO("Cluster size %ld", veb->cluster_size);
    ALG_INFO("Value in cluster %ld at position %ld (min)", i, j);
    ALG_ASSERT(veb->cluster != NULL);
    ALG_ASSERT(veb->cluster[i] != NULL);
    if (j > veb->cluster[i]->min) {
        /* predecessor in the same cluster so we look for it here */
        ALG_INFO("Predecessor in same cluster.");
        j = veb_prev(veb->cluster[i], j);
        ALG_INFO("Predecessor at position %ld", j);
        return index(i, j);
    } else {
        /* predecessor in different cluster so we find previous non-empty one */
        ALG_INFO("Predecessor in different cluster.");
        i = veb_prev(veb->summary, i);

        if (i == MIN_UNDEFINED) {
            /* no previous non-empty cluster found */
            ALG_INFO("No previous non-empty cluster found");
            return MIN_UNDEFINED;
        } else {
            /* previous cluster found so its max has to be the predecessor */
            j = veb->cluster[i]->max;
            ALG_INFO("Predecessor in cluster %ld at position %ld", i, j);
            return index(i, j);
        }
    }
}
