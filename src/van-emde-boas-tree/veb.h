// SPDX-FileCopyrightText: 2014-2025 Alexandru Fikl <alexfikl@gmail.com>
// SPDX-License-Identifier: MIT

#ifndef ALGORITHMS_VAN_EMDE_BOAS_TREE_H
#define ALGORITHMS_VAN_EMDE_BOAS_TREE_H

#include <algorithms.h>

typedef struct veb_t veb_t;

struct veb_t {
    int64_t universe_size;
    int64_t cluster_size;

    int64_t min;
    int64_t max;

    veb_t *summary;
    veb_t **cluster;
};

veb_t *veb_new(int64_t u);

void veb_preallocate(veb_t *veb);

void veb_free(veb_t *veb);

int64_t veb_min(const veb_t *veb);

int64_t veb_max(const veb_t *veb);

bool veb_is_empty(const veb_t *veb);

void veb_dump(const veb_t *veb);

bool veb_contains(const veb_t *veb, int64_t x);

void veb_insert(veb_t *veb, int64_t x);

void veb_delete(veb_t *veb, int64_t x);

int64_t veb_next(const veb_t *veb, int64_t x);

int64_t veb_prev(const veb_t *veb, int64_t x);

#endif /* ALGORITHMS_VAN_EMDE_BOAS_TREE_H */
