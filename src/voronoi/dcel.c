// SPDX-FileCopyrightText: 2014-2025 Alexandru Fikl <alexfikl@gmail.com>
// SPDX-License-Identifier: MIT

#include "dcel.h"

static dcel_vertex_t *dcel_vertex_new(double x, double y) {
    dcel_vertex_t *vertex = alg_malloc(sizeof(dcel_vertex_t));
    vertex->x = x;
    vertex->y = y;
    vertex->edge = NULL;

    return vertex;
}

dcel_face_t *dcel_face_new(struct point *p, dcel_half_edge_t *edge) {
    dcel_face_t *face = alg_malloc(sizeof(dcel_face_t));
    dcel_face_init(face, p, edge);

    return face;
}

void dcel_face_init(dcel_face_t *face, struct point *p, dcel_half_edge_t *edge) {
    if (face == NULL) {
        return;
    }

    face->p = p;
    face->edge = edge;

    if (edge != NULL) {
        edge->face = face;
    }
}

dcel_half_edge_t *dcel_half_edge_new(dcel_vertex_t *origin, dcel_face_t *face) {
    dcel_half_edge_t *edge = alg_calloc(1, sizeof(dcel_half_edge_t));

    edge->origin = origin;
    edge->face = face;
    edge->next = edge;
    edge->prev = edge;

    return edge;
}

void dcel_half_edge_set_twin(dcel_half_edge_t *edge, dcel_half_edge_t *twin) {
    edge->twin = twin;
}

void dcel_half_edge_set_origin(dcel_half_edge_t *edge, struct point *p) {
    if (edge == NULL || p == NULL) {
        return;
    }

    if (edge->origin != NULL) {
        edge->origin->x = point_x(p);
        edge->origin->y = point_y(p);
    } else {
        edge->origin = dcel_vertex_new(point_x(p), point_y(p));
        edge->origin->edge = edge;
    }
}
