// SPDX-FileCopyrightText: 2014-2025 Alexandru Fikl <alexfikl@gmail.com>
// SPDX-License-Identifier: MIT

#ifndef ALGORITHMS_DCEL_H
#define ALGORITHMS_DCEL_H

#include <algorithms.h>
#include <geometry.h>

typedef struct dcel_vertex_t dcel_vertex_t;
typedef struct dcel_half_edge_t dcel_half_edge_t;
typedef struct dcel_face_t dcel_face_t;

struct dcel_vertex_t {
    double x;
    double y;

    dcel_half_edge_t *edge;
};

struct dcel_half_edge_t {
    dcel_vertex_t *origin;
    dcel_face_t *face;
    dcel_half_edge_t *twin;

    dcel_half_edge_t *next;
    dcel_half_edge_t *prev;
};

struct dcel_face_t {
    struct point *p;

    dcel_half_edge_t *edge;
};

dcel_face_t *dcel_face_new(struct point *p, dcel_half_edge_t *edge);

void dcel_face_init(dcel_face_t *face, struct point *p, dcel_half_edge_t *edge);

dcel_half_edge_t *dcel_half_edge_new(dcel_vertex_t *origin, dcel_face_t *face);

void dcel_half_edge_set_twin(dcel_half_edge_t *edge, dcel_half_edge_t *twin);

void dcel_half_edge_set_origin(dcel_half_edge_t *edge, struct point *p);

#endif /* !ALGORITHMS_DCEL_H */
