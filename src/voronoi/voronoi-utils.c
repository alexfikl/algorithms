// SPDX-FileCopyrightText: 2014-2025 Alexandru Fikl <alexfikl@gmail.com>
// SPDX-License-Identifier: MIT

#include "voronoi-utils.h"

int voronoi_event_comparison(const void *a, const void *b) {
    const voronoi_event_t *A = (const voronoi_event_t *) a;
    const voronoi_event_t *B = (const voronoi_event_t *) b;

    return -geometry_point_comparison(A->p, B->p);
}

voronoi_arc_t *linked_list_arc(const linked_list_node_t *node) {
    if (node == NULL) {
        return NULL;
    }

    return (voronoi_arc_t *) node->user_data;
}

voronoi_event_t *voronoi_event_queue_pop(struct priority_queue_t *queue) {
    return (voronoi_event_t *) priority_queue_pop(queue);
}

voronoi_arc_t *voronoi_arc_new(dcel_face_t *face) {
    if (face == NULL) {
        return NULL;
    }

    voronoi_arc_t *arc = alg_calloc(1, sizeof(voronoi_arc_t));
    arc->face = face;

    return arc;
}

void voronoi_arc_destroy(voronoi_arc_t *arc) {
    alg_free(arc);
}

voronoi_event_t *voronoi_site_event_new(dcel_face_t *face, size_t i) {
    voronoi_event_t *event = alg_malloc(sizeof(voronoi_event_t));

    event->type = VORONOI_SITE_EVENT;
    event->p = face->p;

    event->face = face;
    event->index = i;

    event->vertex = NULL;
    event->arc = NULL;

    return event;
}

voronoi_event_t *voronoi_circle_event_new(
    struct point *p, struct point *vertex, linked_list_node_t *node
) {
    voronoi_event_t *event = alg_malloc(sizeof(voronoi_event_t));
    voronoi_arc_t *arc = linked_list_arc(node);

    /* set the point at which the event will occurt */
    event->type = VORONOI_CIRCLE_EVENT;
    event->p = p;

    event->face = NULL;
    event->index = 0;

    event->vertex = vertex;
    event->arc = node;

    /* make sure that the arc has a pointer to the event */
    arc->event = event;

    return event;
}

void voronoi_event_destroy(voronoi_event_t *event) {
    alg_free(event);
}
