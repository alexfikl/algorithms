// SPDX-FileCopyrightText: 2014-2025 Alexandru Fikl <alexfikl@gmail.com>
// SPDX-License-Identifier: MIT

#ifndef ALGORITHMS_VORONOI_UTILS_H
#define ALGORITHMS_VORONOI_UTILS_H

#include <dcel.h>
#include <priority-queue.h>

typedef struct voronoi_segment_t voronoi_segment_t;
typedef struct voronoi_arc_t voronoi_arc_t;
typedef struct voronoi_event_t voronoi_event_t;

typedef enum {
    VORONOI_SITE_EVENT,   /**< Site event.                        */
    VORONOI_CIRCLE_EVENT, /**< Circle event.                      */
    VORONOI_INVALID_EVENT /**< Invalidated circle event.          */
} voronoi_event_type_t;

struct voronoi_event_t {
    voronoi_event_type_t type; /**< Type of the event (site or circle) */
    char padding[4];

    struct point *p; /**< Point at which the event will occur.
                          This is the site for a site event
                          and the point on the circle that
                          intersects the sweep line for a circle
                          event.                             */

    struct point *vertex;    /**< Circle center in a circle event.   */
    linked_list_node_t *arc; /**< Arc to be destroy in circle event. */

    dcel_face_t *face; /**< Face in a site event.              */
    size_t index;      /**< Index of the point in a site event.*/
};

struct voronoi_arc_t {
    dcel_face_t *face;         /**< Face containing the event point.   */
    dcel_half_edge_t *edge[2]; /**< Edges on the two breakpoints of the arc. */

    voronoi_event_t *event; /**< Circle event that removes the arc. */
};

/**
 * @brief Compare two events based on their x coordinate.
 *
 * @param[in] a             Event.
 * @param[in] b             Event.
 * @return                  Return values:
 *                              -1      if a_x < b_x
 *                               0      if a_x == b_x
 *                               1      if a_x > b_x
 */
int voronoi_event_comparison(const void *a, const void *b);

/**
 * @brief Retrieve an arc from a node in the beachline.
 *
 * @param[in] node          Node in the beachline linked list.
 * @return                  The arc contained in the node.
 */
voronoi_arc_t *linked_list_arc(const linked_list_node_t *node);

/**
 * @brief Retrieve the left-most (x direction) event in the event queue.
 *
 * @param[in] queue         Priority queue of events.
 * @return                  Event at the top of the queue.
 */
voronoi_event_t *voronoi_event_queue_pop(struct priority_queue_t *queue);

/**
 * @brief Create a new arc.
 *
 * The arc is defined by the site event that generated it. This site is stored
 * as a face in a doubly connected edge list.
 *
 * @param[in] face          Face containing the site.
 * @return                  Newly allocated arc.
 */
voronoi_arc_t *voronoi_arc_new(dcel_face_t *face);

/**
 * @brief Destroy an existing arc.
 *
 * Note that all the data inside the arc is not freed.
 *
 * @param[in,out] arc       Arc to destroy.
 */
void voronoi_arc_destroy(voronoi_arc_t *arc);

/**
 * @brief Create a new site event.
 *
 * @param[in] face          Face containing the site at which the event occurred.
 * @param[in] index         Index of the site in a list of sites.
 * @return                  A new site event.
 */
voronoi_event_t *voronoi_site_event_new(dcel_face_t *face, size_t index);

/**
 * @brief Create a new circle event.
 *
 * @param[in] p             Point at which the event will happen, i.e. when
 *                          the circle defined by 3 sites on the beachline
 *                          becomes tangent to the sweep line.
 * @param[in] vertex        Center of the circle. This could become a new
 *                          vertex in the diagram.
 * @param[in] node          Node containing the arc that will be destroyed
 *                          by this circle event.
 * @return                  A new circle event.
 */
voronoi_event_t *voronoi_circle_event_new(
    struct point *p, struct point *vertex, linked_list_node_t *node
);

/**
 * @brief Destroy an event.
 *
 * @param[in,out] event     Event.
 */
void voronoi_event_destroy(voronoi_event_t *event);

#endif /* !ALGORITHMS_VORONOI_UTILS_H */
