// SPDX-FileCopyrightText: 2014-2025 Alexandru Fikl <alexfikl@gmail.com>
// SPDX-License-Identifier: MIT

#include "voronoi.h"
#include "voronoi-utils.h"

#include <tikz.h>

static void beachline_dump(linked_list_t *beachline) {
    linked_list_node_t *node = NULL;
    voronoi_arc_t *arc = NULL;

    node = beachline->head;
    while (node != NULL) {
        arc = linked_list_arc(node);
        ALG_INFO(
            "ARC for SITE at <x=%g, y=%g>", point_x(arc->face->p), point_y(arc->face->p)
        );
        if (arc->event != NULL) {
            ALG_INFO(
                "CIRCLE EVENT at <x=%g, y=%g>",
                point_x(arc->event->p),
                point_y(arc->event->p)
            );
        }

        node = node->next;
    }
}

/**
 * @brief Check if an arc is intersected by a horizontal line through a point.
 *
 * @param[in] p             Point.
 * @param[in] node          Node containing an arc.
 * @param[out] r            Intersection point if we draw a line from the point
 *                          to the parabola.
 * @return                  True if the horizontal line starting from the
 *                          given point intersects the arc.
 */
static bool voronoi_arc_intersection(
    const struct point *p, const linked_list_node_t *node, struct point *r
) {
    if (p == NULL || node == NULL) {
        return false;
    }

    voronoi_arc_t *arc = linked_list_arc(node);
    return geometry_parabola_intersection(arc->face->p, p, point_x(p), r);
}

/**
 * @brief Create half-edges for a new arc triple.
 *
 * This function is called when a new arc is inserted into the beachline.
 * The inserted arc is arc[0], while arc[1] and arc[2] are its down and up
 * neighbors.
 *
 * When we insert a new arc into the beachline, a new edge has to be created
 * so that it can expand as the beachline evolves. The new half-edge and its
 * twin are correctly set in the right arcs after this function.
 *
 * Furthermore, the twin edge is inserted into the intersected arc's face
 * edge list while maintaining counterclocwise ordering.
 *
 * @param[in,out] arc       List of three arcs involved in a site event.
 */
static void voronoi_arc_add_edges(voronoi_arc_t *arc[3]) {
    dcel_half_edge_t *edge = dcel_half_edge_new(NULL, arc[1]->face);
    dcel_half_edge_t *twin = dcel_half_edge_new(NULL, arc[0]->face);
    dcel_half_edge_set_twin(edge, twin);
    dcel_half_edge_set_twin(twin, edge);

    /* middle arc has only one edge */
    arc[1]->edge[0] = arc[1]->edge[1] = edge;

    /* right edge of left arc is now the middle edge's twin */
    arc[0]->edge[1] = twin;

    /* left edge of right arc is now the middle edge's twin */
    arc[2]->edge[0] = twin;

    /* add edge to middle arc's face */
    arc[1]->face->edge = edge;

    /* insert twin to the intersected arc's face */
    if (arc[0]->edge[0]) {
        arc[0]->edge[1]->next = arc[0]->edge[0]->next;
    }
    arc[0]->edge[1]->prev = arc[0]->edge[0];
    if (arc[0]->edge[0]) {
        arc[0]->edge[0]->next->prev = arc[0]->edge[1];
        arc[0]->edge[0]->next = arc[0]->edge[1];
    }
}

/**
 * @brief Check for a circle event.
 *
 * We look at the arc in the given node from the beachline and its left and
 * right neighbors to see if their sites can form a circle. If this is the
 * case and the circle event's x coordinate is after the sweep line, a new
 * event is created and inserted into the queue.
 *
 * The coordinate of the event is (c_x + r, c_y), where c is the center of
 * the circle and r is its radius.
 *
 * If the node does not have a left or right neighbor, nothing happens. Also,
 * if the arc in the node already contains a pointer to an existing circle
 * event, the event is invalidated and the new one is put in its place.
 *
 * @param[in] queue         Event queue.
 * @param[in] node          Node on the beachline containing an arc.
 * @param[in] sweep_line    Position of the sweep line.
 */
static void voronoi_check_circle_event(
    struct priority_queue_t *queue, linked_list_node_t *node, double sweep_line
) {
    if (queue == NULL || node == NULL) {
        return;
    }

    voronoi_event_t *event = NULL;
    voronoi_arc_t *prev = NULL;
    voronoi_arc_t *next = NULL;
    voronoi_arc_t *arc = NULL;
    bool is_circle = false;
    struct point *p = point_new(0.0, 0.0);
    struct point *center = point_new(0.0, 0.0);
    double radius = 0;

    /* check that we have a triple (p_i, p_j, p_k) of arcs / sites */
    if (node->prev == NULL || node->next == NULL) {
        ALG_INFO("Circle event not possible, node not part of a triple");
        return;
    }

    /* get corresponding arcs */
    arc = linked_list_arc(node);
    prev = linked_list_arc(node->prev);
    next = linked_list_arc(node->next);
    ALG_INFO("Checking circle event for:");
    ALG_INFO("Site A<x=%g, y=%g>", point_x(prev->face->p), point_y(prev->face->p));
    ALG_INFO("Site B<x=%g, y=%g>", point_x(arc->face->p), point_y(arc->face->p));
    ALG_INFO("Site C<x=%g, y=%g>", point_x(next->face->p), point_y(next->face->p));

    /* invalidate any old events */
    if (arc->event && !ALG_FUZZY_EQ(point_x(arc->event->p), sweep_line)) {
        ALG_INFO("Arc contained different circle event");
        arc->event->type = VORONOI_INVALID_EVENT;
    }

    /* compute the circle described by the 3 sites */
    is_circle =
        geometry_circle(prev->face->p, arc->face->p, next->face->p, center, &radius);

    /* compute point at which the sweep line will be tangent to the circle */
    point_set_x(p, point_x(center) + radius);
    point_set_y(p, point_y(center));

    /* if they do indeed describe a circle, add a circle event */
    if (is_circle && point_x(p) > sweep_line) {
        ALG_INFO(
            "Circle center <x=%g, y=%g> Radius r=%g",
            point_x(center),
            point_y(center),
            radius
        );
        ALG_INFO("New event at site<x=%g, y=%g>", point_x(p), point_y(p));
        event = voronoi_circle_event_new(p, center, node);
        priority_queue_insert(queue, event);
    } else {
        ALG_INFO("No circle event for this triple");
    }

    point_free(p);
    point_free(center);
}

/**
 * @brief Handle a site event.
 *
 * @code{.unparsed}
 *  \                               /
 *   \     i                       /
 *    \         /\        k       /
 *     *,     ,*  `\            ,/
 *       `...x      `-,       ,/
 *           |         *-...-"
 *           |
 *           |
 * __________j_________________________
 * @endcode
 *
 * On a site event, the following things must happen:
 *  * we must find the arc p_i in the beachline that is intersected by the new
 * arc p_j.
 *  * once found, two new arcs are created and inserted into the beachline
 * to form a triple (p_i, p_j, p_i). the left and right arcs are represented
 * by the same site event p_i, the main difference between them is the edges
 * they describe.
 *  * a check for new circle events is performed for p_i, p_j and p_k together
 * with their left and right neighbors.
 *
 * @todo handle the case when p_j->x == intersection(p_i, p_k). in this case
 * we have an instant circle event. two edges instead of one have to be
 * created for p_j.
 *
 * @param[in,out] beachline     List of arcs on the beachline.
 * @param[in,out] queue         Event queue.
 * @param[in] event             Current event.
 */
static void voronoi_handle_site_event(
    linked_list_t *beachline, struct priority_queue_t *queue, voronoi_event_t *event
) {
    ALG_ASSERT(beachline != NULL);
    ALG_ASSERT(queue != NULL);
    ALG_ASSERT(event != NULL);

    linked_list_node_t *node = NULL;
    voronoi_arc_t *arc[3] = {NULL, NULL, NULL};
    struct point *p = NULL;

    ALG_INFO(
        "Inserting site[%lu]<x=%g, y=%g> into beachline",
        event->index,
        point_x(event->p),
        point_y(event->p)
    );

    /* insert first site into the beachline */
    if (linked_list_size(beachline) == 0) {
        arc[1] = voronoi_arc_new(event->face);
        linked_list_append(beachline, arc[1]);
        return;
    }

    /* search the beachline for the arc to the left of event->p */
    p = point_new(0.0, 0.0);
    node = beachline->head;
    while (node != NULL && !voronoi_arc_intersection(event->p, node, p)) {
        node = node->next;
    }
    ALG_INFO("Intersection point<x=%g, y=%g>", point_x(p), point_y(p));

    /* create new arcs to insert into the beachline. */
    arc[0] = linked_list_arc(node);
    arc[1] = voronoi_arc_new(event->face);
    arc[2] = voronoi_arc_new(arc[0]->face);
    voronoi_arc_add_edges(arc);
    ALG_INFO(
        "Arc intersected: site<x=%g, y=%g>",
        point_x(arc[0]->face->p),
        point_y(arc[0]->face->p)
    );

    /* insert into the beachline to get triple (0, 1, 2) */
    linked_list_insert(beachline, node, arc[2]);
    linked_list_insert(beachline, node, arc[1]);
    ALG_INFO("New beachline:");
    beachline_dump(beachline);

    /* check new circle events formed by the insertion of the new arc */
    node = node->next;
    voronoi_check_circle_event(queue, node->prev, point_x(event->p));
    voronoi_check_circle_event(queue, node, point_x(event->p));
    voronoi_check_circle_event(queue, node->next, point_x(event->p));

    point_free(p);
}

/**
 * @brief Handle circle event.
 *
 * On a site event, the following things happen:
 *  * we remove the arc that is going to be destroyed from the beachline.
 *  * we update the edges in the 3 arcs involved in the event.
 *  * we check for new circle events caused by the removal of the arc.
 *
 * @param[in,out] beachline         List of sites on the beach.
 * @param[in,out] queue             Event queue.
 * @param[in] event                 Current event.
 */
static void voronoi_handle_circle_event(
    linked_list_t *beachline, struct priority_queue_t *queue, voronoi_event_t *event
) {
    ALG_ASSERT(beachline != NULL);
    ALG_ASSERT(queue != NULL);
    ALG_ASSERT(event != NULL);

    linked_list_node_t *prev = event->arc->prev;
    linked_list_node_t *next = event->arc->next;
    voronoi_arc_t *arc = linked_list_arc(event->arc);

    /* remove arc from the beachline */
    ALG_INFO(
        "Removing site<x=%g, y=%g> from beachline", point_x(event->p), point_y(event->p)
    );
    linked_list_remove(beachline, event->arc, false);

    /* set origin for the half-edges involved in the event */

    /* check circle events caused by the removal of the arc */
    if (prev) {
        voronoi_check_circle_event(queue, prev, point_x(event->p));
    }
    if (next) {
        voronoi_check_circle_event(queue, next, point_x(event->p));
    }

    voronoi_arc_destroy(arc);
}

/**
 * @brief Close all the remaining arcs in the beachline.
 *
 * @param[in] voronoi       Voronoi diagram.
 * @param[in] beachline     List of arcs on the beach line.
 */
static void voronoi_bounding_box(voronoi_t *voronoi, linked_list_t *beachline) {
    ALG_UNUSED(voronoi);

    linked_list_node_t *node = NULL;
    voronoi_arc_t *arc = NULL;

    node = beachline->head;
    while (node) {
        arc = linked_list_arc(node);
        printf("arc site<x=%g, y=%g>", point_x(arc->face->p), point_y(arc->face->p));

        node = node->next;
    }
}

voronoi_t *voronoi_new(struct point **points, size_t n) {
    double dx = 0;
    double dy = 0;
    struct rectangle *box = rectangle_new_point(points[0], points[0]);

    for (size_t i = 1; i < n; ++i) {
        rectangle_update(box, points[i]);
    }

    /* make bounding box slightly larger so that all the points are inside */
    dx = rectangle_width(box);
    dy = rectangle_height(box);
    rectangle_enlarge(box, dx / 5.0, dy / 5.0);

    return voronoi_new_ext(points, n, box);
}

voronoi_t *voronoi_new_ext(struct point **points, size_t n, struct rectangle *box) {
    voronoi_t *voronoi = alg_calloc(1, sizeof(voronoi_t));
    struct priority_queue_t *queue = priority_queue_new(voronoi_event_comparison);
    linked_list_t *beachline = linked_list_new();

    voronoi_event_t *event = NULL;

    voronoi->nfaces = n;
    voronoi->box = box;

    /* push site events into the priority queue */
    voronoi->faces = (dcel_face_t *) alg_malloc(n * sizeof(dcel_face_t));
    for (size_t i = 0; i < voronoi->nfaces; ++i) {
        dcel_face_init(&(voronoi->faces[i]), points[i], NULL);

        if (rectangle_contains(box, points[i]) <= 0) {
            ALG_WARN(
                "SKIPPED: point[%lu]<x=%g, y=%g> is outside bounding box",
                i,
                point_x(points[i]),
                point_y(points[i])
            );
            continue;
        }

        ALG_INFO("site<x=%g, y=%g>", point_x(points[i]), point_y(points[i]));
        event = voronoi_site_event_new(&(voronoi->faces[i]), i);
        priority_queue_insert(queue, event);
    }

    /* process the event queue */
    event = voronoi_event_queue_pop(queue);
    while (event) {
        switch (event->type) {
        case VORONOI_SITE_EVENT:
            ALG_INFO("============ SITE EVENT ==============");
            voronoi_handle_site_event(beachline, queue, event);
            break;
        case VORONOI_CIRCLE_EVENT:
            ALG_INFO("=========== CIRCLE EVENT =============");
            voronoi_handle_circle_event(beachline, queue, event);
            break;
        case VORONOI_INVALID_EVENT:
            /* invalidated events are skipped */
            break;
        }

        voronoi_event_destroy(event);
        event = voronoi_event_queue_pop(queue);
    }

    /* handle remaining arcs in the beachline for unbounded edges */
    voronoi_bounding_box(voronoi, beachline);

    return voronoi;
}

void voronoi_tikz(const voronoi_t *voronoi, const char *filename, bool with_labels) {
    if (voronoi == NULL || filename == NULL) {
        return;
    }

    char label[BUFSIZ];

    tikz_preamble(filename);
    tikz_document_begin();
    tikz_picture_begin(NULL);

    /* draw bounding box */
    tikz_add_rectangle(voronoi->box);

    /* draw all sites */
    for (size_t i = 0; i < voronoi->nfaces; ++i) {
        if (with_labels) {
            snprintf(label, BUFSIZ, "$%lu$", i);
            tikz_add_point_position(
                point_x(voronoi->faces[i].p), point_y(voronoi->faces[i].p), label
            );
        } else {
            tikz_add_point_position(
                point_x(voronoi->faces[i].p), point_y(voronoi->faces[i].p), NULL
            );
        }
    }

    tikz_picture_end();
    tikz_document_end();
}
