// SPDX-FileCopyrightText: 2014-2025 Alexandru Fikl <alexfikl@gmail.com>
// SPDX-License-Identifier: MIT

#ifndef ALGORITHMS_VORONOI_H
#define ALGORITHMS_VORONOI_H

#include <algorithms.h>
#include <dcel.h>
#include <geometry.h>

typedef struct {
    struct rectangle *box;

    size_t nvertices;
    dcel_vertex_t *vertices;

    size_t nedges;
    dcel_half_edge_t *half_edges;

    size_t nfaces;
    dcel_face_t *faces;
} voronoi_t;

/**
 * @brief Create a new Voronoi diagram from a set of points.
 *
 * @param[in] points        Set of points.
 * @param[in] n             Number of points.
 * @return                  The Voronoi diagram for the points.
 *
 * @sa http://www.cs.wustl.edu/~pless/546/lectures/L14.html
 * @sa https://www.cs.hmc.edu/~mbrubeck/voronoi.html
 */
voronoi_t *voronoi_new(struct point **points, size_t n);

voronoi_t *voronoi_new_ext(struct point **points, size_t n, struct rectangle *box);

void voronoi_tikz(const voronoi_t *voronoi, const char *filename, bool with_labels);

#endif /* !ALGORITHMS_VORONOI_H */
