// SPDX-FileCopyrightText: 2014-2025 Alexandru Fikl <alexfikl@gmail.com>
// SPDX-License-Identifier: MIT

#define GREATEST_USE_ABBREVS 0

#include <geometry.h>
#include <greatest.h>

typedef struct {
    char padding[4];

    int argc;
    char **argv;
} test_context_t;

static test_context_t ctx = {.argc = 0, .argv = NULL};

static void test_setup_cb(void *arg) {
    ALG_UNUSED(arg);
    algorithms_init(ctx.argc, ctx.argv);
}

static void test_teardown_cb(void *arg) {
    ALG_UNUSED(arg);
    algorithms_finalize();
}

GREATEST_TEST test_point_new() {
    struct point *p = point_new(1.0, 1.0);

    GREATEST_ASSERT(ALG_FUZZY_EQ(point_x(p), 1.0) && ALG_FUZZY_EQ(point_y(p), 1.0));
    alg_free(p);

    GREATEST_PASS();
}

GREATEST_TEST test_segment_new() {
    struct point *A = point_new(0.0, 0.0);
    struct point *B = point_new(1.0, 0.0);
    struct segment *edge = segment_new(A, B);

    GREATEST_ASSERT(ALG_FUZZY_EQ(segment_length(edge), 1.0));
    alg_free(edge);
    point_free(A);
    point_free(B);

    GREATEST_PASS();
}

GREATEST_TEST test_point_comparison() {
    struct point *A = point_new(2.0, 1.0);
    struct point *B = point_new(1.0, 0.0);

    GREATEST_ASSERT_EQ(1, geometry_point_comparison(A, B));

    point_set_x(A, 1.0);
    point_set_y(A, 0.0);
    point_set_x(B, 2.0);
    point_set_y(B, 0.0);
    GREATEST_ASSERT_EQ(-1, geometry_point_comparison(A, B));

    point_set_x(A, 1.0);
    point_set_y(A, 1.0);
    point_set_x(B, 1.0);
    point_set_y(B, 0.0);
    GREATEST_ASSERT_EQ(1, geometry_point_comparison(A, B));

    point_set_x(A, 1.0);
    point_set_y(A, 0.0);
    point_set_x(B, 1.0);
    point_set_y(B, 1.0);
    GREATEST_ASSERT_EQ(-1, geometry_point_comparison(A, B));

    point_set_x(A, 3.0);
    point_set_y(A, 1.0);
    point_set_x(B, 3.0);
    point_set_y(B, 1.0);
    GREATEST_ASSERT_EQ(0, geometry_point_comparison(A, B));

    point_free(A);
    point_free(B);

    GREATEST_PASS();
}

GREATEST_TEST test_point_sort() {
    static const size_t n = 122;
    struct point **points;
    size_t i = 0;
    double offset = 0.0;

    points = (struct point **) alg_malloc(n * sizeof(struct point *));
    for (double x = -0.5; x <= 0.5; x += 0.1) {
        offset = (i % 2 == 1) * 0.05;
        for (double y = -0.5 + offset; y <= 0.5 + offset; y += 0.1) {
            points[i++] = point_new(x, y);
        }
    }
    points[i++] = point_new(0.5, 0.5);
    ALG_ASSERT(i == n);

    qsort(points, n, sizeof(struct point *), geometry_point_comparison_wrapper);

    for (size_t j = 0; j < (n - 1); ++j) {
        GREATEST_ASSERT(point_x(points[j]) <= point_x(points[j + 1]));
        if (ALG_FUZZY_EQ(point_x(points[j]), point_x(points[j + 1]))) {
            GREATEST_ASSERT(point_y(points[j]) <= point_y(points[j + 1]));
        }
    }

    for (size_t j = 0; j < n; ++j) {
        point_free(points[j]);
    }
    alg_free(points);

    GREATEST_PASS();
}

GREATEST_TEST test_triangle_area() {
    struct point *A = point_new(0.0, 0.0);
    struct point *B = point_new(2.0, 0.0);
    struct point *C = point_new(0.0, 2.0);

    GREATEST_ASSERT(ALG_FUZZY_EQ(2.0, geometry_triangle_area(A, B, C)));
    GREATEST_ASSERT(ALG_FUZZY_EQ(-2.0, geometry_triangle_area(C, B, A)));

    point_free(A);
    point_free(B);
    point_free(C);

    GREATEST_PASS();
}

GREATEST_TEST test_collinear() {
    struct point *A = point_new(1.0, 0.0);
    struct point *B = point_new(2.0, 0.0);
    struct point *C = point_new(3.0, 0.0);

    GREATEST_ASSERT(geometry_collinear(A, B, C));

    point_set_x(C, 3.0);
    point_set_y(C, 1.0);
    GREATEST_ASSERT(!geometry_collinear(A, B, C));

    point_free(A);
    point_free(B);
    point_free(C);

    GREATEST_PASS();
}

GREATEST_TEST test_counterclockwise() {
    struct point *A = point_new(0.0, 0.0);
    struct point *B = point_new(2.0, 0.0);
    struct point *C = point_new(1.0, 3.0);

    GREATEST_ASSERT(geometry_counterclockwise(A, B, C));
    GREATEST_ASSERT(geometry_counterclockwise(B, C, A));
    GREATEST_ASSERT(geometry_counterclockwise(C, A, B));

    GREATEST_ASSERT(!geometry_counterclockwise(B, A, C));
    GREATEST_ASSERT(!geometry_counterclockwise(C, B, A));
    GREATEST_ASSERT(!geometry_counterclockwise(A, C, B));

    point_free(A);
    point_free(B);
    point_free(C);

    GREATEST_PASS();
}

GREATEST_TEST test_clockwise() {
    struct point *A = point_new(0.0, 0.0);
    struct point *B = point_new(2.0, 0.0);
    struct point *C = point_new(1.0, 3.0);

    GREATEST_ASSERT(geometry_clockwise(C, B, A));
    GREATEST_ASSERT(geometry_clockwise(B, A, C));
    GREATEST_ASSERT(geometry_clockwise(A, C, B));

    GREATEST_ASSERT(!geometry_clockwise(A, B, C));
    GREATEST_ASSERT(!geometry_clockwise(B, C, A));
    GREATEST_ASSERT(!geometry_clockwise(C, A, B));

    point_free(A);
    point_free(B);
    point_free(C);

    GREATEST_PASS();
}

GREATEST_SUITE(geometry_suite) {
    GREATEST_SET_SETUP_CB(test_setup_cb, NULL);
    GREATEST_SET_TEARDOWN_CB(test_teardown_cb, NULL);

    GREATEST_RUN_TEST(test_point_new);
    GREATEST_RUN_TEST(test_segment_new);
    GREATEST_RUN_TEST(test_point_comparison);
    GREATEST_RUN_TEST(test_point_sort);
    GREATEST_RUN_TEST(test_triangle_area);
    GREATEST_RUN_TEST(test_collinear);
    GREATEST_RUN_TEST(test_counterclockwise);
    GREATEST_RUN_TEST(test_clockwise);
}

/* Add definitions that need to be in the test runner's main file. */
GREATEST_MAIN_DEFS();

int main(int argc, char **argv) {
    ctx.argc = argc;
    ctx.argv = argv;

    GREATEST_MAIN_BEGIN(); /* command-line arguments, initialization. */
    GREATEST_RUN_SUITE(geometry_suite);
    GREATEST_MAIN_END(); /* display results */
}
