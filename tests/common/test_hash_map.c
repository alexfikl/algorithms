// SPDX-FileCopyrightText: 2014-2025 Alexandru Fikl <alexfikl@gmail.com>
// SPDX-License-Identifier: MIT

#define GREATEST_USE_ABBREVS 0

#include <greatest.h>
#include <hash-map.h>

#include <time.h>

typedef struct {
    int key;
    int value;
} test_struct_t;

typedef struct {
    char padding[4];

    int argc;
    char **argv;

    struct hash_map_t *map;
} test_context_t;

static test_context_t ctx = {.argc = 0, .argv = NULL, .map = NULL};

static int test_comparison(const void *a, const void *b) {
    const test_struct_t *ta = (const test_struct_t *) a;
    const test_struct_t *tb = (const test_struct_t *) b;

    if (ta->key == tb->key) {
        return 0;
    }

    return (ta->key > tb->key ? 1 : -1);
}

static int test_count_42(void *key, void *value, void *args) {
    ALG_UNUSED(value);

    test_struct_t *tkey = (test_struct_t *) key;
    int *count = (int *) args;

    if (tkey->key == 42) {
        *count += 1;
    }

    return 0;
}

static int test_traverse_success(void *key, void *value, void *args) {
    ALG_UNUSED(key);
    ALG_UNUSED(value);
    ALG_UNUSED(args);

    return 0;
}

static int test_traverse_failure(void *key, void *value, void *args) {
    ALG_UNUSED(value);
    ALG_UNUSED(args);

    test_struct_t *test = (test_struct_t *) key;

    if (test->key == 13) {
        return 1;
    }

    return 0;
}

static test_struct_t *test_struct_new(int key, int value) {
    test_struct_t *test = alg_calloc(1, sizeof(test_struct_t));
    test->key = key;
    test->value = value;

    return test;
}

static void test_setup_cb(void *arg) {
    ALG_UNUSED(arg);

    algorithms_init(ctx.argc, ctx.argv);
    ctx.map = hash_map_new(sizeof(test_struct_t), test_comparison, NULL);

    test_struct_t *key = NULL;
    test_struct_t *value = NULL;

    for (int i = 0; i < 23; ++i) {
        key = test_struct_new(i, i);
        value = test_struct_new(i + 1, i + 1);
        hash_map_set(ctx.map, key, value);
    }
}

static void test_teardown_cb(void *arg) {
    ALG_UNUSED(arg);

    hash_map_destroy(ctx.map, true);
    ctx.map = NULL;

    algorithms_finalize();
}

GREATEST_TEST test_hash_murmur() {
    const char *ta = "some test string!";
    const char *tb = "some other test string";
    uint32_t hash_ta = hash_murmur((const uint8_t *) ta, strlen(ta), 0);
    uint32_t hash_tb = hash_murmur((const uint8_t *) tb, strlen(tb), 0);

    GREATEST_ASSERT_EQ(hash_ta, 1455326055);
    GREATEST_ASSERT_EQ(hash_tb, 3315776201);

    GREATEST_PASS();
}

GREATEST_TEST test_hash() {
    test_struct_t *ta = test_struct_new(1, 1);
    test_struct_t *tb = test_struct_new(1, 1);
    uint32_t hash_ta = hash_djb((uint8_t *) ta, sizeof(test_struct_t), 0);
    uint32_t hash_tb = hash_djb((uint8_t *) tb, sizeof(test_struct_t), 0);

    GREATEST_ASSERT_EQ(hash_ta, hash_tb);

    alg_free(ta);
    alg_free(tb);

    GREATEST_PASS();
}

GREATEST_TEST test_new() {
    struct hash_map_t *map = hash_map_new(sizeof(test_struct_t), test_comparison, NULL);

    GREATEST_ASSERT(map->cmp != NULL);
    GREATEST_ASSERT_EQ(0, hash_map_size(map));
    GREATEST_ASSERT_EQ(23, hash_map_size(ctx.map));

    hash_map_destroy(map, true);

    GREATEST_PASS();
}

GREATEST_TEST test_set() {
    struct hash_map_t *map = ctx.map;
    test_struct_t *key = test_struct_new(42, 42);
    test_struct_t *value = test_struct_new(42, 42);
    test_struct_t *retrieved_value = NULL;
    int count = 0;

    hash_map_set(map, key, value);
    retrieved_value = hash_map_get(map, key);
    GREATEST_ASSERT(test_comparison(value, retrieved_value) == 0);

    hash_map_set(map, key, value);
    hash_map_foreach(map, test_count_42, &count);
    GREATEST_ASSERT_EQ(2, count);

    /* remove the duplicate entry otherwise we're going to free it twice */
    hash_map_remove(map, key, false);

    GREATEST_PASS();
}

GREATEST_TEST test_set_duplicate() {
    struct hash_map_t *map = ctx.map;
    test_struct_t *key = test_struct_new(42, 42);
    test_struct_t *value = test_struct_new(42, 42);
    int count = 0;

    hash_map_set(map, key, value);
    GREATEST_ASSERT(!hash_map_set_unique(map, key, value));

    hash_map_foreach(map, test_count_42, &count);
    GREATEST_ASSERT_EQ(1, count);

    GREATEST_PASS();
}

GREATEST_TEST test_iterator() {
    struct hash_map_t *map = ctx.map;

    GREATEST_ASSERT(!hash_map_foreach(map, test_traverse_success, NULL));
    GREATEST_ASSERT(hash_map_foreach(map, test_traverse_failure, NULL));

    GREATEST_PASS();
}

GREATEST_SUITE(hash_map_suite) {
    GREATEST_SET_SETUP_CB(test_setup_cb, NULL);
    GREATEST_SET_TEARDOWN_CB(test_teardown_cb, NULL);

    GREATEST_RUN_TEST(test_hash_murmur);
    GREATEST_RUN_TEST(test_hash);
    GREATEST_RUN_TEST(test_new);
    GREATEST_RUN_TEST(test_set);
    GREATEST_RUN_TEST(test_set_duplicate);
    GREATEST_RUN_TEST(test_iterator);
}

/* Add definitions that need to be in the test runner's main file. */
GREATEST_MAIN_DEFS();

int main(int argc, char **argv) {
    ctx.argc = argc;
    ctx.argv = argv;

    GREATEST_MAIN_BEGIN(); /* command-line arguments, initialization. */
    GREATEST_RUN_SUITE(hash_map_suite);
    GREATEST_MAIN_END(); /* display results */
}
