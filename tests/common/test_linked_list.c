// SPDX-FileCopyrightText: 2014-2025 Alexandru Fikl <alexfikl@gmail.com>
// SPDX-License-Identifier: MIT

#define GREATEST_USE_ABBREVS 0

#include <greatest.h>
#include <linked-list.h>

typedef struct {
    int key;
    int value;
} test_struct_t;

typedef struct {
    int argc;
    int counter;

    char **argv;
    linked_list_t *list;
} test_context_t;

static test_context_t ctx = {.argc = 0, .argv = NULL, .counter = 0, .list = NULL};

static void test_setup_cb(void *arg) {
    ALG_UNUSED(arg);

    algorithms_init(ctx.argc, ctx.argv);
    ctx.list = linked_list_new();
}

static void test_teardown_cb(void *arg) {
    ALG_UNUSED(arg);

    linked_list_destroy(ctx.list, true);
    ctx.list = NULL;

    algorithms_finalize();
}

static test_struct_t *test_struct_new(int key, int value) {
    test_struct_t *test = alg_calloc(1, sizeof(test_struct_t));
    test->key = key;
    test->value = value;

    return test;
}

static void linked_list_iterator(void *data, void *args) {
    test_struct_t *test = (test_struct_t *) data;
    bool *equal = (bool *) args;

    *equal = *equal && (test->value == ctx.counter);
    ctx.counter -= 1;
}

GREATEST_TEST test_new() {
    linked_list_t *list = ctx.list;

    GREATEST_ASSERT(
        list->head == NULL && list->tail == NULL && linked_list_size(list) == 0
    );

    GREATEST_PASS();
}

GREATEST_TEST test_one_node() {
    linked_list_t *list = ctx.list;
    test_struct_t *test = NULL;

    test = test_struct_new(0, 0);
    linked_list_append(list, test);

    GREATEST_ASSERT(linked_list_size(list) == 1);
    GREATEST_ASSERT(list->head == list->tail);
    GREATEST_ASSERT(list->head->next == NULL && list->head->prev == NULL);
    GREATEST_ASSERT(list->tail->next == NULL && list->tail->prev == NULL);
    GREATEST_ASSERT(list->head->user_data == test);

    test = (test_struct_t *) list->head->user_data;
    GREATEST_ASSERT(test->key == 0 && test->value == 0);

    GREATEST_PASS();
}

GREATEST_TEST test_append() {
    linked_list_t *list = ctx.list;
    test_struct_t *test_head = NULL;
    test_struct_t *test_tail = NULL;

    test_head = test_struct_new(0, 0);
    linked_list_append(list, test_head);

    test_tail = test_struct_new(1, 1);
    linked_list_append(list, test_tail);

    GREATEST_ASSERT(linked_list_size(list) == 2);
    GREATEST_ASSERT(list->head->prev == NULL);
    GREATEST_ASSERT(list->head->next == list->tail);
    GREATEST_ASSERT(list->tail->next == NULL);
    GREATEST_ASSERT(list->tail->prev == list->head);
    GREATEST_ASSERT(list->head->user_data == test_head);
    GREATEST_ASSERT(list->tail->user_data == test_tail);

    test_head = (test_struct_t *) list->head->user_data;
    test_tail = (test_struct_t *) list->tail->user_data;

    GREATEST_ASSERT(test_head->key == 0 && test_head->value == 0);
    GREATEST_ASSERT(test_tail->key == 1 && test_tail->value == 1);

    GREATEST_PASS();
}

GREATEST_TEST test_prepend() {
    linked_list_t *list = ctx.list;
    test_struct_t *test_head = NULL;
    test_struct_t *test_tail = NULL;

    test_tail = test_struct_new(1, 1);
    linked_list_prepend(list, test_tail);

    test_head = test_struct_new(0, 0);
    linked_list_prepend(list, test_head);

    GREATEST_ASSERT(linked_list_size(list) == 2);
    GREATEST_ASSERT(list->head->prev == NULL);
    GREATEST_ASSERT(list->head->next == list->tail);
    GREATEST_ASSERT(list->tail->prev == list->head);
    GREATEST_ASSERT(list->tail->next == NULL);
    GREATEST_ASSERT(list->head->user_data == test_head);
    GREATEST_ASSERT(list->tail->user_data == test_tail);

    test_head = (test_struct_t *) list->head->user_data;
    test_tail = (test_struct_t *) list->tail->user_data;

    GREATEST_ASSERT(test_head->key == 0 && test_head->value == 0);
    GREATEST_ASSERT(test_tail->key == 1 && test_tail->value == 1);

    GREATEST_PASS();
}

GREATEST_TEST test_traverse_empty() {
    linked_list_t *list = ctx.list;
    linked_list_node_t *node = NULL;
    size_t element_count = 0;

    node = list->head;
    while (node) {
        node = node->next;
        ++element_count;
    }

    GREATEST_ASSERT_EQ(0, element_count);
    GREATEST_PASS();
}

GREATEST_TEST test_traverse_forward() {
    linked_list_t *list = ctx.list;
    linked_list_node_t *node = NULL;
    test_struct_t *test = NULL;
    int counter = 0;
    bool correct_order = true;

    for (int i = 0; i < 10; ++i) {
        linked_list_insert(list, list->tail, test_struct_new(i, i));
    }

    counter = 0;
    node = list->head;
    while (node) {
        test = (test_struct_t *) node->user_data;
        correct_order = correct_order && (test->key == counter);

        counter += 1;
        node = node->next;
    }

    GREATEST_ASSERT_EQ(10, linked_list_size(list));
    GREATEST_ASSERT_EQ(true, correct_order);
    GREATEST_PASS();
}

GREATEST_TEST test_traverse_backward() {
    linked_list_t *list = ctx.list;
    linked_list_node_t *node = NULL;
    test_struct_t *test = NULL;
    int counter = 0;
    bool correct_order = true;

    for (int i = 0; i < 10; ++i) {
        linked_list_insert(list, NULL, test_struct_new(i, i));
    }

    counter = 0;
    node = list->tail;
    while (node) {
        test = (test_struct_t *) node->user_data;
        correct_order = correct_order && (test->key == counter);

        counter += 1;
        node = node->prev;
    }

    GREATEST_ASSERT_EQ(10, linked_list_size(list));
    GREATEST_ASSERT_EQ(true, correct_order);
    GREATEST_PASS();
}

GREATEST_TEST test_expand() {
    linked_list_t *list = ctx.list;
    linked_list_t *extra = linked_list_new();
    linked_list_node_t *node = NULL;
    test_struct_t *test = NULL;
    int key = 0;

    for (int i = 0; i < 3; ++i) {
        test = test_struct_new(i, i);
        linked_list_append(list, test);
    }

    for (int i = 3; i < 6; ++i) {
        test = test_struct_new(i, i);
        linked_list_append(extra, test);
    }

    linked_list_expand(list, extra);
    GREATEST_ASSERT(linked_list_size(list) == 6);

    node = list->head;
    while (node) {
        test = (test_struct_t *) node->user_data;
        GREATEST_ASSERT_EQ(key, test->key);

        node = node->next;
        key += 1;
    }
    linked_list_destroy(extra, false);

    GREATEST_ASSERT_EQ(key, 6);
    GREATEST_PASS();
}

GREATEST_TEST test_iterator() {
    linked_list_t *list = ctx.list;
    test_struct_t *test = NULL;
    bool correct_order = true;

    test = test_struct_new(0, 0);
    linked_list_prepend(list, test);

    test = test_struct_new(1, 1);
    linked_list_prepend(list, test);

    test = test_struct_new(2, 2);
    linked_list_prepend(list, test);

    ctx.counter = (int) (linked_list_size(list) - 1);
    linked_list_foreach(list, linked_list_iterator, &correct_order);

    GREATEST_ASSERT_EQ(true, correct_order);
    GREATEST_PASS();
}

GREATEST_SUITE(linked_list_suite) {
    GREATEST_SET_SETUP_CB(test_setup_cb, NULL);
    GREATEST_SET_TEARDOWN_CB(test_teardown_cb, NULL);

    GREATEST_RUN_TEST(test_new);
    GREATEST_RUN_TEST(test_one_node);
    GREATEST_RUN_TEST(test_append);
    GREATEST_RUN_TEST(test_prepend);
    GREATEST_RUN_TEST(test_traverse_empty);
    GREATEST_RUN_TEST(test_traverse_forward);
    GREATEST_RUN_TEST(test_traverse_backward);
    GREATEST_RUN_TEST(test_expand);
    GREATEST_RUN_TEST(test_iterator);
}

/* Add definitions that need to be in the test runner's main file. */
GREATEST_MAIN_DEFS();

int main(int argc, char **argv) {
    ctx.argc = argc;
    ctx.argv = argv;

    GREATEST_MAIN_BEGIN(); /* command-line arguments, initialization. */
    GREATEST_RUN_SUITE(linked_list_suite);
    GREATEST_MAIN_END(); /* display results */
}
