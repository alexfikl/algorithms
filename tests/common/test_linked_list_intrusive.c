// SPDX-FileCopyrightText: 2014-2025 Alexandru Fikl <alexfikl@gmail.com>
// SPDX-License-Identifier: MIT

#define GREATEST_USE_ABBREVS 0

#include <greatest.h>
#include <linked-list-intrusive.h>

typedef struct {
    int key;
    int value;
    alg_list_t list;
} key_value_t;

static key_value_t *key_value_new(int key, int value) {
    key_value_t *kv = alg_calloc(1, sizeof(key_value_t));

    kv->key = key;
    kv->value = value;
    alg_list_node_init(&(kv->list));

    return kv;
}

GREATEST_TEST test_new() {
    alg_list_t list = alg_list_init(list);
    key_value_t *element;

    for (int i = 0; i < 10; ++i) {
        element = key_value_new(i, i * i);
        alg_list_append(&list, &(element->list));
    }

    alg_list_foreach(element, &list, list) {
        printf("element: %d %d\n", element->key, element->value);
    }

    GREATEST_PASS();
}

GREATEST_SUITE(linked_list_intrusive_suite) {
    GREATEST_RUN_TEST(test_new);
}

/* Add definitions that need to be in the test runner's main file. */
GREATEST_MAIN_DEFS();

int main(int argc, char **argv) {
    GREATEST_MAIN_BEGIN(); /* command-line arguments, initialization. */
    GREATEST_RUN_SUITE(linked_list_intrusive_suite);
    GREATEST_MAIN_END(); /* display results */
}
