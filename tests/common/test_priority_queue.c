// SPDX-FileCopyrightText: 2014-2025 Alexandru Fikl <alexfikl@gmail.com>
// SPDX-License-Identifier: MIT

#define GREATEST_USE_ABBREVS 0

#include <greatest.h>
#include <priority-queue.h>

#include <time.h>

typedef struct {
    int key;
    int value;
} test_struct_t;

typedef struct {
    char padding[4];

    int argc;
    char **argv;

    struct priority_queue_t *queue;
} test_context_t;

static test_context_t ctx = {.argc = 0, .argv = NULL, .queue = NULL};

static int test_comparison(const void *a, const void *b) {
    const test_struct_t *ta = (const test_struct_t *) a;
    const test_struct_t *tb = (const test_struct_t *) b;

    if (ta->key == tb->key) {
        return 0;
    }

    return (ta->key > tb->key ? 1 : -1);
}

static void test_max(void *user_data, void *args) {
    test_struct_t *test = (test_struct_t *) user_data;
    int *max = (int *) args;

    if (test->key > *max) {
        *max = test->key;
    }
}

static test_struct_t *test_struct_new(int key, int value) {
    test_struct_t *test = alg_calloc(1, sizeof(test_struct_t));
    test->key = key;
    test->value = value;

    return test;
}

static void test_setup_cb(void *arg) {
    ALG_UNUSED(arg);

    algorithms_init(ctx.argc, ctx.argv);
    ctx.queue = priority_queue_new(test_comparison);

    test_struct_t *test = NULL;

    srand((uint32_t) time(NULL));
    for (size_t i = 0; i < 10; ++i) {
        test = test_struct_new(rand() % 42, rand() % 42);
        priority_queue_insert(ctx.queue, test);
    }
}

static void test_teardown_cb(void *arg) {
    ALG_UNUSED(arg);

    priority_queue_destroy(ctx.queue, true);
    ctx.queue = NULL;

    algorithms_finalize();
}

GREATEST_TEST test_new() {
    struct priority_queue_t *queue = priority_queue_new(test_comparison);

    GREATEST_ASSERT(queue->cmp != NULL);
    GREATEST_ASSERT(linked_list_size(queue->list) == 0);

    priority_queue_destroy(queue, true);

    GREATEST_PASS();
}

GREATEST_TEST test_priority() {
    struct priority_queue_t *queue = ctx.queue;
    linked_list_node_t *node = NULL;

    node = queue->list->head;
    while (node->next) {
        GREATEST_ASSERT(test_comparison(node->user_data, node->next->user_data) >= 0);
        node = node->next;
    }

    GREATEST_PASS();
}

GREATEST_TEST test_peek() {
    struct priority_queue_t *queue = ctx.queue;
    test_struct_t *test = NULL;
    int max = 0;
    size_t size = 0;

    linked_list_foreach(queue->list, test_max, &max);

    size = priority_queue_size(queue);
    test = (test_struct_t *) priority_queue_peek(queue);
    GREATEST_ASSERT_EQ(max, test->key);
    GREATEST_ASSERT_EQ(size, priority_queue_size(queue));

    GREATEST_PASS();
}

GREATEST_TEST test_pop() {
    struct priority_queue_t *queue = ctx.queue;
    test_struct_t *test = NULL;
    int max = 0;
    size_t size = 0;

    linked_list_foreach(queue->list, test_max, &max);

    size = priority_queue_size(queue);
    test = (test_struct_t *) priority_queue_pop(queue);
    GREATEST_ASSERT_EQ(max, test->key);
    GREATEST_ASSERT_EQ(size, priority_queue_size(queue) + 1);
    alg_free(test);

    test = (test_struct_t *) priority_queue_pop(queue);
    GREATEST_ASSERT_EQ(size, priority_queue_size(queue) + 2);
    alg_free(test);

    GREATEST_PASS();
}

GREATEST_SUITE(linked_queue_suite) {
    GREATEST_SET_SETUP_CB(test_setup_cb, NULL);
    GREATEST_SET_TEARDOWN_CB(test_teardown_cb, NULL);

    GREATEST_RUN_TEST(test_new);
    GREATEST_RUN_TEST(test_priority);
    GREATEST_RUN_TEST(test_peek);
    GREATEST_RUN_TEST(test_pop);
}

/* Add definitions that need to be in the test runner's main file. */
GREATEST_MAIN_DEFS();

int main(int argc, char **argv) {
    ctx.argc = argc;
    ctx.argv = argv;

    GREATEST_MAIN_BEGIN(); /* command-line arguments, initialization. */
    GREATEST_RUN_SUITE(linked_queue_suite);
    GREATEST_MAIN_END(); /* display results */
}
