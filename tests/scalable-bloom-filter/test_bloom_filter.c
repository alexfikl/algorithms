// SPDX-FileCopyrightText: 2014-2025 Alexandru Fikl <alexfikl@gmail.com>
// SPDX-License-Identifier: MIT

#define _DEFAULT_SOURCE

#include <bloom-filter.h>
#include <greatest.h>

typedef struct {
    char padding[4];

    int argc;
    char **argv;
} test_context_t;

static test_context_t ctx = {.argc = 0, .argv = NULL};

static const int s_urls = 9000;
static const char s_filename[] = "urls.txt";

static void test_setup_cb(void *arg) {
    ALG_UNUSED(arg);
    algorithms_init(ctx.argc, ctx.argv);
}

static void test_teardown_cb(void *arg) {
    ALG_UNUSED(arg);
    algorithms_finalize();
}

static void bloom_filter_add_data_file(struct bloom_filter_t *bf) {
    char buffer[BUFSIZ];
    FILE *fd = NULL;

    fd = fopen(s_filename, "r");
    if (fd == NULL) {
        printf("Cannot open file \"%s\".\n", s_filename);
        exit(1);
    }

    while (fgets(buffer, BUFSIZ, fd) != NULL) {
        bloom_filter_add(bf, buffer, strlen(buffer));
    }

    fclose(fd);
}

static int bloom_filter_check_data_file(struct bloom_filter_t *bf) {
    int false_negatives = 0;
    char buffer[BUFSIZ];
    FILE *fd = NULL;

    fd = fopen(s_filename, "r");
    if (fd == NULL) {
        printf("Cannot open file \"%s\".\n", s_filename);
        exit(1);
    }

    while (fgets(buffer, BUFSIZ, fd) != NULL) {
        if (!bloom_filter_contains(bf, buffer, strlen(buffer))) {
            ++false_negatives;
        }
    }

    fclose(fd);

    return false_negatives;
}

GREATEST_TEST bloom_filter_no_false_negatives() {
    struct bloom_filter_t *bf = bloom_filter_new(s_urls, 0.001, NULL);
    int false_negatives = 0;
    ALG_CHECK_ABORT(bf != NULL, "Could not allocate bloom filter");

    bloom_filter_add_data_file(bf);
    false_negatives = bloom_filter_check_data_file(bf);
    bloom_filter_destroy(bf);

    GREATEST_ASSERT_EQ(0, false_negatives);
    GREATEST_PASS();
}

GREATEST_SUITE(bloom_filter_suite) {
    GREATEST_SET_SETUP_CB(test_setup_cb, NULL);
    GREATEST_SET_TEARDOWN_CB(test_teardown_cb, NULL);

    GREATEST_RUN_TEST(bloom_filter_no_false_negatives);
    //     RUN_TEST(bloom_filter_probability);
}

/* Add definitions that need to be in the test runner's main file. */
GREATEST_MAIN_DEFS();

int main(int argc, char **argv) {
    ctx.argc = argc;
    ctx.argv = argv;

    GREATEST_MAIN_BEGIN(); /* command-line arguments, initialization. */
    GREATEST_RUN_SUITE(bloom_filter_suite);
    GREATEST_MAIN_END(); /* display results */
}
