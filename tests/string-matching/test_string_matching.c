// SPDX-FileCopyrightText: 2014-2025 Alexandru Fikl <alexfikl@gmail.com>
// SPDX-License-Identifier: MIT

#define GREATEST_USE_ABBREVS 0

#include <greatest.h>
#include <string-matching.h>

typedef struct {
    char padding[4];

    int argc;
    char **argv;
} test_context_t;

static test_context_t ctx = {.argc = 0, .argv = NULL};

static const int n = 24;
static const char string[] = "GCATCGCAGAGAGTATACAGTACG";
static const int m = 8;
static const char sub[] = "GCAGAGAG";

static void test_setup_cb(void *arg) {
    ALG_UNUSED(arg);
    algorithms_init(ctx.argc, ctx.argv);
}

static void test_teardown_cb(void *arg) {
    ALG_UNUSED(arg);
    algorithms_finalize();
}

int test_string_matching(string_matching_type_t algorithm) {
    int result = -1;

    result = string_match(string, n, sub, m, algorithm);
    printf("result: %d\n", result);
    GREATEST_ASSERT(result == 5);

    result = string_match("adhfkadhfka", 11, "adhf", 4, algorithm);
    GREATEST_ASSERT(result == 0);

    result = string_match("adhftadhfka", 11, "hfka", 4, algorithm);
    GREATEST_ASSERT(result == 7);

    result = string_match("adhfkadhfka", 11, "hkax", 4, algorithm);
    GREATEST_ASSERT(result == -1);

    result = string_match("adhfkadhfka", 11, "adhx", 4, algorithm);
    GREATEST_ASSERT(result == -1);

    result = string_match(string, 0, sub, m, algorithm);
    GREATEST_ASSERT(result == -1);

    result = string_match(NULL, n, sub, m, algorithm);
    GREATEST_ASSERT(result == -1);

    result = string_match(string, n, NULL, m, algorithm);
    GREATEST_ASSERT(result == 0);

    result = string_match(string, n, sub, 0, algorithm);
    GREATEST_ASSERT(result == 0);

    result = string_match(string, n, "XXX", 3, algorithm);
    GREATEST_ASSERT(result == -1);

    result = string_match("XXX", 3, sub, m, algorithm);
    GREATEST_ASSERT(result == -1);

    return GREATEST_TEST_RES_PASS;
}

GREATEST_TEST test_brute_force() {
    int result = -1;

    result = test_string_matching(MATCH_BRUTE_FORCE);
    if (result == GREATEST_TEST_RES_FAIL) {
        return GREATEST_TEST_RES_FAIL;
    }

    GREATEST_PASS();
}

GREATEST_TEST test_karp_rabin() {
    int result = -1;

    result = test_string_matching(MATCH_KARP_RABIN);
    if (result == GREATEST_TEST_RES_FAIL) {
        return GREATEST_TEST_RES_FAIL;
    }

    GREATEST_PASS();
}

GREATEST_TEST test_morris_pratt() {
    int result = -1;

    result = test_string_matching(MATCH_MORRIS_PRATT);
    if (result == GREATEST_TEST_RES_FAIL) {
        return GREATEST_TEST_RES_FAIL;
    }

    GREATEST_PASS();
}

GREATEST_SUITE(string_matching_suite) {
    GREATEST_SET_SETUP_CB(test_setup_cb, NULL);
    GREATEST_SET_TEARDOWN_CB(test_teardown_cb, NULL);

    GREATEST_RUN_TEST(test_brute_force);
    GREATEST_RUN_TEST(test_karp_rabin);
    GREATEST_RUN_TEST(test_morris_pratt);
}

/* Add definitions that need to be in the test runner's main file. */
GREATEST_MAIN_DEFS();

int main(int argc, char **argv) {
    ctx.argc = argc;
    ctx.argv = argv;

    GREATEST_MAIN_BEGIN(); /* command-line arguments, initialization. */
    GREATEST_RUN_SUITE(string_matching_suite);
    GREATEST_MAIN_END(); /* display results */
}
