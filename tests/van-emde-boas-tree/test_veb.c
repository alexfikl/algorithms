// SPDX-FileCopyrightText: 2014-2025 Alexandru Fikl <alexfikl@gmail.com>
// SPDX-License-Identifier: MIT

#include <greatest.h>
#include <veb.h>

static int g_argc;
static char **g_argv;

static void test_setup_cb(void *arg) {
    ALG_UNUSED(arg);
    algorithms_init(g_argc, g_argv);
}

static void test_teardown_cb(void *arg) {
    ALG_UNUSED(arg);
    algorithms_finalize();
}

static bool test_veb_preallocation(const veb_t *veb) {
    if (veb->universe_size == 2) {
        return veb->summary == NULL && veb->cluster == NULL;
    }

    if (veb->summary == NULL) {
        return false;
    }

    bool is_allocated = test_veb_preallocation(veb->summary);
    for (int64_t i = 0; i < veb->cluster_size; ++i) {
        if (veb->cluster[i] == NULL) {
            is_allocated = false;
            break;
        }

        is_allocated |= test_veb_preallocation(veb->cluster[i]);
    }

    return is_allocated;
}

GREATEST_TEST test_veb_new() {
    const int64_t universe_size = 16;
    veb_t *veb = NULL;

    /* test min universe size */
    veb = veb_new(2);

    GREATEST_ASSERT(veb->min < 0);
    GREATEST_ASSERT(veb->max < 0);
    GREATEST_ASSERT(veb->summary == NULL);
    GREATEST_ASSERT(veb->cluster == NULL);

    veb_free(veb);

    /* test universe size */
    veb = veb_new(universe_size);

    GREATEST_ASSERT(veb->min < 0);
    GREATEST_ASSERT(veb->max < 0);
    GREATEST_ASSERT(veb->summary == NULL);
    GREATEST_ASSERT(veb->cluster != NULL);
    GREATEST_ASSERT(veb->cluster[0] == NULL);

    veb_free(veb);

    /* test preallocate */
    veb = veb_new(universe_size);
    veb_preallocate(veb);

    GREATEST_ASSERT(veb->min < 0);
    GREATEST_ASSERT(veb->max < 0);
    GREATEST_ASSERT(test_veb_preallocation(veb));

    veb_free(veb);

    GREATEST_PASS();
}

GREATEST_TEST test_veb_insert() {
    const int64_t universe_size = 16;
    veb_t *veb = NULL;

    /* test universe size */
    veb = veb_new(universe_size);

    veb_insert(veb, 3);
    GREATEST_ASSERT(veb->min == 3);
    GREATEST_ASSERT(veb->max == 3);

    veb_insert(veb, 8);
    GREATEST_ASSERT(veb->min == 3);
    GREATEST_ASSERT(veb->max == 8);

    veb_insert(veb, 13);
    GREATEST_ASSERT(veb->min == 3);
    GREATEST_ASSERT(veb->max == 13);

    veb_insert(veb, 2);
    GREATEST_ASSERT(veb->min == 2);
    GREATEST_ASSERT(veb->max == 13);

    veb_insert(veb, -10);
    GREATEST_ASSERT(veb->min == 2);
    GREATEST_ASSERT(veb->max == 13);

    veb_insert(veb, universe_size + 10);
    GREATEST_ASSERT(veb->min == 2);
    GREATEST_ASSERT(veb->max == 13);

    veb_free(veb);

    GREATEST_PASS();
}

GREATEST_SUITE(bloom_filter_suite) {
    GREATEST_SET_SETUP_CB(test_setup_cb, NULL);
    GREATEST_SET_TEARDOWN_CB(test_teardown_cb, NULL);

    GREATEST_RUN_TEST(test_veb_new);
    GREATEST_RUN_TEST(test_veb_insert);
    //     GREATEST_RUN_TEST(veb_contains);
}

/* Add definitions that need to be in the test runner's main file. */
GREATEST_MAIN_DEFS();

int main(int argc, char **argv) {
    g_argc = argc;
    g_argv = argv;

    GREATEST_MAIN_BEGIN(); /* command-line arguments, initialization. */
    GREATEST_RUN_SUITE(bloom_filter_suite);
    GREATEST_MAIN_END(); /* display results */
}
